REM Set up the docker shell environment:
REM docker-machine env --shell cmd default
FOR /f "tokens=*" %%i IN ('docker-machine env --shell cmd default') DO %%i

REM Build the image:
docker run -it -p=32123:8000 registry.synthesislabs.net/finsurv-dashboard