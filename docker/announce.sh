#!/bin/bash

MY_IP=$(ip addr show eth0 | grep "inet " | awk '{print $2}' | sed 's/\/.*//')


while true
do
   echo ${SERVICE_NAME} - Announcing myself as $MY_IP:${SERVICE_PORT} to etcd [$ETCD_SERVERS]
   /opt/etcd/etcdctl -C $ETCD_SERVERS set --ttl 60 /synthesislabs/routing/${SERVICE_NAME}/endpoints/${MY_IP}:${SERVICE_PORT} {}
   sleep 45
done
