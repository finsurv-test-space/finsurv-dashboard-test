#!/bin/bash

# Clone the code repo (||true at the end to continue even if it fails):
echo Cloning the source repository:
git clone https://synthesisbuilduser:PMt5VePOXMf4@bitbucket.org/synthesis_admin/finsurv-dashboard.git . || true

# Install the application explicitly so that we can work around the permission issue:
# http://stackoverflow.com/questions/18136746/npm-install-failed-with-cannot-run-in-wd
npm install --unsafe-perm

# Start the server:
npm start