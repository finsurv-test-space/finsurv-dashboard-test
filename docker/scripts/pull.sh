#!/bin/bash

# Wait for clone.
sleep 300

while true
do
   echo Pulling repo changes
   git pull
   sleep 300
done
