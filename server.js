var express = require('express');
var app = express();

var port = process.env.PORT || 8000;

app.use(express.static(__dirname + '/web'));

require('./app/routes')(app);


app.listen(port);


exports = module.exports = app;
