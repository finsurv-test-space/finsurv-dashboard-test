When running devmode ...

  open terminal and run "npm start"


When deploying with tomcat ... 

  Make the following changes: 
    -> finsurv-dashboard/web/index.html 
      Change the base path to <base href="/finsurv-dashboard/">
    -> finsurv-dashboard/web/main.js
      Add the following property in the require.config: baseUrl: '/finsurv-dashboard/'
    -> web/app/dashboards/dynamic-dashboard-states/dynamic-dashboard-states.factory.js
      Change the rasCodeUrl from "var rawCodeURL = '/finsurv-dashboard/dashboards' + codePath + '/dashboard.js'" to "var rawCodeURL = '../../../../dashboards' + codePath + '/dashboard.js'"

