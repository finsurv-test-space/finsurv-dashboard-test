#!/bin/bash

ROOT=$PWD

if [ $# -gt 1 ]
    then
          version=$1
          country=$2
    else
          echo "Using default values"
          version="3.3"
          country="South Africa"
fi

echo "Version: $version"
echo "Country: $country"

write_metadata(){
   echo "{
        	\"artefact\":
                [{
                	\"name\":\"${ROOT}/build/libs/*.war\",
                	\"destination\":\"TOMCAT_FRONT\"
                }]
        }" >> ${ROOT}/build/metadata.json
}

sed -i "s|^\s*<h1 class=\"welcome\">.*</h1>|<h1 class=\"welcome\">Welcome to TXstream $version $country</h1>|g" ./src/main/webapp/login.jsp

rm -rf build

npm i --unsafe-perm
./gradlew clean build

write_metadata

# if [ ! -d "BuiltFiles" ]
# then
# 	mkdir "BuiltFiles"
# fi

# rm -f "BuiltFiles/*"
# cp "build/libs/"*.war "BuiltFiles/"