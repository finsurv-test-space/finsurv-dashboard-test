// This defines all the data sources that exist for dashboards.
// This list is used to load all the factories that handle the data.
// Various widgets will get their data from these.
// This file is processed before RequireJS has booted up. Therefore it is not in a define block.
// This file drives the RequireJS config as well as the Angular config.
var dataSourceList = [

  { 'name': 'script' }, 
  { 'name': 'clock' }, 
  { 'name': 'text' }, 
  { 'name': 'pivotModel'}, 
  { 'name': 'workflows' }
];
