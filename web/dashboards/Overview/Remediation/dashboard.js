function createDashBoard(parameters) {

  getDate = function(days) {
    d = new Date();
    d.setDate(d.getDate() - days);
    return d.toISOString().substr(0, 10);
  };

  return {

    widgets: [
      //This widget Gives information regarding the last refresh date and time,
      //The dashboard version,
      //and the database the dashboard is getting its data from
      {
        class: "col-xs-12",
        name: "Information",
        type: "information",
        config: {
          interval: 5000 //time in milliseconds //5 seconds
        },
        maximisedMode: false
      },
     
      //This widget represents the Count of Rejected Bounces from the SARB
      //Data is represented on a Bar Graph
      //Shows data for past week with a bucket for everything older than a week
      {
        //This widget represents the Error Pareto
        //Data is represented matrix
        class: "col-xs-12",
        name: "Error Pareto (80/20)",
        type: "pareto",
        config: {
          interval: 5000,
          dataSourceName: "Error Pareto"
        },
        maximisedMode: false
      },
       {
        //This widget represents the Invalids Pareto
        //Data is represented matrix
        class: "col-xs-12",
        name: "Incompletes Pareto (80/20)",
        type: "pareto",
        config: {
          interval: 5000,
          dataSourceName: "Invalids Pareto"
        },
        maximisedMode: false
      },
      {
        class: "col-sm-12 col-md-6",
        name: "Count of Rejected Bounces from Regulator",
        type: "barGraph",
        config: {
          interval: 5000,
          dataSourceName: "Bounce Counts"
        },
        maximisedMode: false
      },  
      //This widget represents the Count of replaced transactions
      //Data is represented on a Bar Graph
      //Shows data for past week with a bucket for everything older than a week
      {
        class: "col-sm-12 col-md-6",
        name: "Count of Replacement Bounces",
        type: "barGraph",
        config: {
          interval: 5000,
          dataSourceName: "Replacement Bouncing"
        },
        maximisedMode: false
      },
    ],


    dataSources: [{
      name: "Bounce Counts",
      type: "pivotModel",
      config: {
        query: {
          "model": "dModelSARBRejectedCount",
          "startAt": 1,
          "maxRecords": 1000000,
          "select": {
            "bucket": "field('RejectBucket')"
          },
          "filter": [],
          "aggregate": {
            "count": "count()"
          },
          "orderBy": ["asc('bucket')"]
        },
        useSampleData: false,
        sampleData: [{
          bucket: '>7',
          count: 1
        }, {
          bucket: '6',
          count: 4
        }, {
          bucket: '5',
          count: 16
        }, {
          bucket: '4',
          count: 30
        }, {
          bucket: '2',
          count: 43
        }]
      }
    }, {
      name: "Error Pareto",
      type: "pivotModel",
      config: {
        query: {
          "model": "ModelSARBError",
          "startAt": 1,
          "maxRecords": 1000000,
          "select": {
            "errorCode": "field('Code')"
          },
          "filter": [],
          "aggregate": {
            "count": "count()"
          },
          "orderBy": ["asc('errorCode')"]
        },
        useSampleData: false,
        sampleData: 
        [
          {
            "errorGroup": "217",
            "count": "3"
          },
          {
            "errorGroup": "224",
            "count": "3"
          },
          {
            "errorGroup": "283",
            "count": "3"
          },
          {
            "errorGroup": "322",
            "count": "1"
          },
          {
            "errorGroup": "S09",
            "count": "1"
          },
          {
            "errorGroup": "217",
            "count": "156"
          },
          {
            "errorGroup": "224",
            "count": "156"
          },
          {
            "errorGroup": "225",
            "count": "156"
          },
          {
            "errorGroup": "238",
            "count": "8"
          },
          {
            "errorGroup": "241",
            "count": "4"
          },
          {
            "errorGroup": "366",
            "count": "3"
          },
          {
            "errorGroup": "378",
            "count": "1"
          },
          {
            "errorGroup": "381",
            "count": "3"
          },
          {
            "errorGroup": "405",
            "count": "4"
          },
          {
            "errorGroup": "410",
            "count": "3"
          },
          {
            "errorGroup": "215",
            "count": "1"
          },
          {
            "errorGroup": "217",
            "count": "86"
          },
          {
            "errorGroup": "224",
            "count": "86"
          },
          {
            "errorGroup": "272",
            "count": "4"
          },
          {
            "errorGroup": "283",
            "count": "86"
          },
          {
            "errorGroup": "297",
            "count": "1"
          },
          {
            "errorGroup": "320",
            "count": "1"
          },
          {
            "errorGroup": "322",
            "count": "12"
          },
          {
            "errorGroup": "374",
            "count": "7"
          },
          {
            "errorGroup": "410",
            "count": "3"
          },
          {
            "errorGroup": "416",
            "count": "2"
          },
          {
            "errorGroup": "490",
            "count": "7"
          },
          {
            "errorGroup": "514",
            "count": "5"
          },
          {
            "errorGroup": "S09",
            "count": "3"
          },
          {
            "errorGroup": "S11",
            "count": "3"
          },
          {
            "errorGroup": "00",
            "count": "1"
          },
          {
            "errorGroup": "215",
            "count": "1"
          },
          {
            "errorGroup": "217",
            "count": "6428"
          },
          {
            "errorGroup": "224",
            "count": "8690"
          },
          {
            "errorGroup": "225",
            "count": "8022"
          },
          {
            "errorGroup": "231",
            "count": "5"
          },
          {
            "errorGroup": "233",
            "count": "1"
          },
          {
            "errorGroup": "234",
            "count": "5"
          },
          {
            "errorGroup": "238",
            "count": "8"
          },
          {
            "errorGroup": "244",
            "count": "3"
          },
          {
            "errorGroup": "254",
            "count": "5"
          },
          {
            "errorGroup": "256",
            "count": "1"
          },
          {
            "errorGroup": "257",
            "count": "6"
          },
          {
            "errorGroup": "259",
            "count": "1"
          },
          {
            "errorGroup": "260",
            "count": "5"
          },
          {
            "errorGroup": "267",
            "count": "2"
          },
          {
            "errorGroup": "268",
            "count": "2"
          },
          {
            "errorGroup": "269",
            "count": "1"
          },
          {
            "errorGroup": "272",
            "count": "64"
          },
          {
            "errorGroup": "279",
            "count": "1"
          },
          {
            "errorGroup": "283",
            "count": "668"
          },
          {
            "errorGroup": "289",
            "count": "5"
          },
          {
            "errorGroup": "290",
            "count": "11"
          },
          {
            "errorGroup": "294",
            "count": "15"
          },
          {
            "errorGroup": "296",
            "count": "5"
          },
          {
            "errorGroup": "297",
            "count": "4"
          },
          {
            "errorGroup": "298",
            "count": "2"
          },
          {
            "errorGroup": "299",
            "count": "3"
          },
          {
            "errorGroup": "304",
            "count": "4"
          },
          {
            "errorGroup": "306",
            "count": "6"
          },
          {
            "errorGroup": "308",
            "count": "5"
          },
          {
            "errorGroup": "310",
            "count": "1"
          },
          {
            "errorGroup": "311",
            "count": "6"
          },
          {
            "errorGroup": "315",
            "count": "5"
          },
          {
            "errorGroup": "316",
            "count": "5"
          },
          {
            "errorGroup": "319",
            "count": "9"
          },
          {
            "errorGroup": "320",
            "count": "5"
          },
          {
            "errorGroup": "322",
            "count": "364"
          },
          {
            "errorGroup": "326",
            "count": "7"
          },
          {
            "errorGroup": "331",
            "count": "2"
          },
          {
            "errorGroup": "332",
            "count": "97"
          },
          {
            "errorGroup": "333",
            "count": "97"
          },
          {
            "errorGroup": "334",
            "count": "97"
          },
          {
            "errorGroup": "336",
            "count": "94"
          },
          {
            "errorGroup": "337",
            "count": "48"
          },
          {
            "errorGroup": "339",
            "count": "12"
          },
          {
            "errorGroup": "340",
            "count": "48"
          },
          {
            "errorGroup": "341",
            "count": "1"
          },
          {
            "errorGroup": "346",
            "count": "3"
          },
          {
            "errorGroup": "349",
            "count": "2"
          },
          {
            "errorGroup": "355",
            "count": "1"
          },
          {
            "errorGroup": "366",
            "count": "1"
          },
          {
            "errorGroup": "371",
            "count": "1"
          },
          {
            "errorGroup": "374",
            "count": "63"
          },
          {
            "errorGroup": "378",
            "count": "12"
          },
          {
            "errorGroup": "379",
            "count": "1"
          },
          {
            "errorGroup": "380",
            "count": "4"
          },
          {
            "errorGroup": "381",
            "count": "6"
          },
          {
            "errorGroup": "385",
            "count": "1"
          },
          {
            "errorGroup": "393",
            "count": "9"
          },
          {
            "errorGroup": "405",
            "count": "13"
          },
          {
            "errorGroup": "408",
            "count": "1"
          },
          {
            "errorGroup": "410",
            "count": "47"
          },
          {
            "errorGroup": "416",
            "count": "9"
          },
          {
            "errorGroup": "419",
            "count": "1"
          },
          {
            "errorGroup": "421",
            "count": "1"
          },
          {
            "errorGroup": "425",
            "count": "4"
          },
          {
            "errorGroup": "428",
            "count": "3829"
          },
          {
            "errorGroup": "431",
            "count": "2"
          },
          {
            "errorGroup": "439",
            "count": "4"
          },
          {
            "errorGroup": "456",
            "count": "4"
          },
          {
            "errorGroup": "485",
            "count": "2"
          },
          {
            "errorGroup": "490",
            "count": "120"
          },
          {
            "errorGroup": "491",
            "count": "1"
          },
          {
            "errorGroup": "496",
            "count": "1"
          },
          {
            "errorGroup": "503",
            "count": "1"
          },
          {
            "errorGroup": "504",
            "count": "2"
          },
          {
            "errorGroup": "512",
            "count": "4"
          },
          {
            "errorGroup": "514",
            "count": "104"
          },
          {
            "errorGroup": "533",
            "count": "20"
          },
          {
            "errorGroup": "565",
            "count": "3"
          },
          {
            "errorGroup": "E01",
            "count": "71"
          },
          {
            "errorGroup": "L01",
            "count": "2"
          },
          {
            "errorGroup": "L02",
            "count": "1163"
          },
          {
            "errorGroup": "S09",
            "count": "26"
          },
          {
            "errorGroup": "S11",
            "count": "1393"
          },
          {
            "errorGroup": "217",
            "count": "4934"
          },
          {
            "errorGroup": "224",
            "count": "4934"
          },
          {
            "errorGroup": "225",
            "count": "4934"
          },
          {
            "errorGroup": "333",
            "count": "1"
          },
          {
            "errorGroup": "371",
            "count": "199"
          },
          {
            "errorGroup": "381",
            "count": "9"
          },
          {
            "errorGroup": "217",
            "count": "4"
          },
          {
            "errorGroup": "224",
            "count": "4"
          },
          {
            "errorGroup": "225",
            "count": "1"
          },
          {
            "errorGroup": "283",
            "count": "3"
          },
          {
            "errorGroup": "217",
            "count": "551"
          },
          {
            "errorGroup": "224",
            "count": "551"
          },
          {
            "errorGroup": "230",
            "count": "1"
          },
          {
            "errorGroup": "238",
            "count": "9"
          },
          {
            "errorGroup": "244",
            "count": "1"
          },
          {
            "errorGroup": "254",
            "count": "1"
          },
          {
            "errorGroup": "256",
            "count": "1"
          },
          {
            "errorGroup": "267",
            "count": "3"
          },
          {
            "errorGroup": "283",
            "count": "551"
          },
          {
            "errorGroup": "290",
            "count": "3"
          },
          {
            "errorGroup": "292",
            "count": "3"
          },
          {
            "errorGroup": "297",
            "count": "1"
          },
          {
            "errorGroup": "300",
            "count": "6"
          },
          {
            "errorGroup": "310",
            "count": "3"
          },
          {
            "errorGroup": "311",
            "count": "1"
          },
          {
            "errorGroup": "320",
            "count": "1"
          },
          {
            "errorGroup": "322",
            "count": "38"
          },
          {
            "errorGroup": "324",
            "count": "2"
          },
          {
            "errorGroup": "333",
            "count": "10"
          },
          {
            "errorGroup": "334",
            "count": "10"
          },
          {
            "errorGroup": "337",
            "count": "10"
          },
          {
            "errorGroup": "340",
            "count": "16"
          },
          {
            "errorGroup": "346",
            "count": "6"
          },
          {
            "errorGroup": "349",
            "count": "2"
          },
          {
            "errorGroup": "371",
            "count": "4"
          },
          {
            "errorGroup": "374",
            "count": "10"
          },
          {
            "errorGroup": "376",
            "count": "1"
          },
          {
            "errorGroup": "379",
            "count": "4"
          },
          {
            "errorGroup": "380",
            "count": "3"
          },
          {
            "errorGroup": "385",
            "count": "7"
          },
          {
            "errorGroup": "388",
            "count": "4"
          },
          {
            "errorGroup": "393",
            "count": "3"
          },
          {
            "errorGroup": "405",
            "count": "1"
          },
          {
            "errorGroup": "410",
            "count": "40"
          },
          {
            "errorGroup": "416",
            "count": "30"
          },
          {
            "errorGroup": "419",
            "count": "21"
          },
          {
            "errorGroup": "421",
            "count": "19"
          },
          {
            "errorGroup": "425",
            "count": "3"
          },
          {
            "errorGroup": "426",
            "count": "6"
          },
          {
            "errorGroup": "428",
            "count": "60"
          },
          {
            "errorGroup": "431",
            "count": "15"
          },
          {
            "errorGroup": "433",
            "count": "1"
          },
          {
            "errorGroup": "464",
            "count": "24"
          },
          {
            "errorGroup": "485",
            "count": "2"
          },
          {
            "errorGroup": "490",
            "count": "13"
          },
          {
            "errorGroup": "499",
            "count": "2"
          },
          {
            "errorGroup": "514",
            "count": "3"
          },
          {
            "errorGroup": "565",
            "count": "2"
          },
          {
            "errorGroup": "E01",
            "count": "2"
          },
          {
            "errorGroup": "L02",
            "count": "7"
          },
          {
            "errorGroup": "S09",
            "count": "3"
          },
          {
            "errorGroup": "S11",
            "count": "31"
          },
          {
            "errorGroup": "217",
            "count": "7"
          },
          {
            "errorGroup": "224",
            "count": "7"
          },
          {
            "errorGroup": "283",
            "count": "7"
          },
          {
            "errorGroup": "217",
            "count": "24"
          },
          {
            "errorGroup": "224",
            "count": "24"
          },
          {
            "errorGroup": "283",
            "count": "24"
          },
          {
            "errorGroup": "217",
            "count": "119"
          },
          {
            "errorGroup": "224",
            "count": "132"
          },
          {
            "errorGroup": "225",
            "count": "39"
          },
          {
            "errorGroup": "231",
            "count": "7"
          },
          {
            "errorGroup": "242",
            "count": "7"
          },
          {
            "errorGroup": "269",
            "count": "5"
          },
          {
            "errorGroup": "272",
            "count": "12"
          },
          {
            "errorGroup": "279",
            "count": "5"
          },
          {
            "errorGroup": "283",
            "count": "93"
          },
          {
            "errorGroup": "304",
            "count": "5"
          },
          {
            "errorGroup": "315",
            "count": "2"
          },
          {
            "errorGroup": "355",
            "count": "14"
          },
          {
            "errorGroup": "217",
            "count": "36"
          },
          {
            "errorGroup": "224",
            "count": "36"
          },
          {
            "errorGroup": "283",
            "count": "36"
          },
          {
            "errorGroup": "290",
            "count": "1"
          },
          {
            "errorGroup": "304",
            "count": "3"
          },
          {
            "errorGroup": "382",
            "count": "8"
          },
          {
            "errorGroup": "418",
            "count": "2"
          },
          {
            "errorGroup": "420",
            "count": "2"
          },
          {
            "errorGroup": "423",
            "count": "5"
          },
          {
            "errorGroup": "427",
            "count": "2"
          },
          {
            "errorGroup": "435",
            "count": "1"
          },
          {
            "errorGroup": "436",
            "count": "1"
          },
          {
            "errorGroup": "438",
            "count": "3"
          },
          {
            "errorGroup": "440",
            "count": "3"
          },
          {
            "errorGroup": "441",
            "count": "2"
          },
          {
            "errorGroup": "442",
            "count": "3"
          },
          {
            "errorGroup": "443",
            "count": "6"
          },
          {
            "errorGroup": "444",
            "count": "6"
          },
          {
            "errorGroup": "445",
            "count": "6"
          },
          {
            "errorGroup": "449",
            "count": "8"
          },
          {
            "errorGroup": "461",
            "count": "4"
          },
          {
            "errorGroup": "463",
            "count": "4"
          },
          {
            "errorGroup": "465",
            "count": "3"
          }
        ]

      }
    }, {
      name: "Invalids Pareto",
      type: "pivotModel",
      config: {
        query: {

          "model": "ModelInvalidError",
          "startAt": 1,
          "maxRecords": 1000000,
          "select": {
            "errorCode": "field('ErrorCode')"
          },
          "filter": [],
          "aggregate": {
            "count": "count()"
          },
          "orderBy": ["asc('errorCode')"]

        },
        useSampleData: false,
        sampleData: 

        [
          {
            "errorGroup": "217",
            "count": "3"
          },
          {
            "errorGroup": "224",
            "count": "3"
          },
          {
            "errorGroup": "283",
            "count": "3"
          },
          {
            "errorGroup": "322",
            "count": "1"
          },
          {
            "errorGroup": "S09",
            "count": "1"
          },
          {
            "errorGroup": "217",
            "count": "156"
          },
          {
            "errorGroup": "224",
            "count": "156"
          },
          {
            "errorGroup": "225",
            "count": "156"
          },
          {
            "errorGroup": "238",
            "count": "8"
          },
          {
            "errorGroup": "241",
            "count": "4"
          },
          {
            "errorGroup": "366",
            "count": "3"
          },
          {
            "errorGroup": "378",
            "count": "1"
          },
          {
            "errorGroup": "381",
            "count": "3"
          },
          {
            "errorGroup": "405",
            "count": "4"
          },
          {
            "errorGroup": "410",
            "count": "3"
          },
          {
            "errorGroup": "215",
            "count": "1"
          },
          {
            "errorGroup": "217",
            "count": "86"
          },
          {
            "errorGroup": "224",
            "count": "86"
          },
          {
            "errorGroup": "272",
            "count": "4"
          },
          {
            "errorGroup": "283",
            "count": "86"
          },
          {
            "errorGroup": "297",
            "count": "1"
          },
          {
            "errorGroup": "320",
            "count": "1"
          },
          {
            "errorGroup": "322",
            "count": "12"
          },
          {
            "errorGroup": "374",
            "count": "7"
          },
          {
            "errorGroup": "410",
            "count": "3"
          },
          {
            "errorGroup": "416",
            "count": "2"
          },
          {
            "errorGroup": "490",
            "count": "7"
          },
          {
            "errorGroup": "514",
            "count": "5"
          },
          {
            "errorGroup": "S09",
            "count": "3"
          },
          {
            "errorGroup": "S11",
            "count": "3"
          },
          {
            "errorGroup": "00",
            "count": "1"
          },
          {
            "errorGroup": "215",
            "count": "1"
          },
          {
            "errorGroup": "217",
            "count": "6428"
          },
          {
            "errorGroup": "224",
            "count": "8690"
          },
          {
            "errorGroup": "225",
            "count": "8022"
          },
          {
            "errorGroup": "231",
            "count": "5"
          },
          {
            "errorGroup": "233",
            "count": "1"
          },
          {
            "errorGroup": "234",
            "count": "5"
          },
          {
            "errorGroup": "238",
            "count": "8"
          },
          {
            "errorGroup": "244",
            "count": "3"
          },
          {
            "errorGroup": "254",
            "count": "5"
          },
          {
            "errorGroup": "256",
            "count": "1"
          },
          {
            "errorGroup": "257",
            "count": "6"
          },
          {
            "errorGroup": "259",
            "count": "1"
          },
          {
            "errorGroup": "260",
            "count": "5"
          },
          {
            "errorGroup": "267",
            "count": "2"
          },
          {
            "errorGroup": "268",
            "count": "2"
          },
          {
            "errorGroup": "269",
            "count": "1"
          },
          {
            "errorGroup": "272",
            "count": "64"
          },
          {
            "errorGroup": "279",
            "count": "1"
          },
          {
            "errorGroup": "283",
            "count": "668"
          },
          {
            "errorGroup": "289",
            "count": "5"
          },
          {
            "errorGroup": "290",
            "count": "11"
          },
          {
            "errorGroup": "294",
            "count": "15"
          },
          {
            "errorGroup": "296",
            "count": "5"
          },
          {
            "errorGroup": "297",
            "count": "4"
          },
          {
            "errorGroup": "298",
            "count": "2"
          },
          {
            "errorGroup": "299",
            "count": "3"
          },
          {
            "errorGroup": "304",
            "count": "4"
          },
          {
            "errorGroup": "306",
            "count": "6"
          },
          {
            "errorGroup": "308",
            "count": "5"
          },
          {
            "errorGroup": "310",
            "count": "1"
          },
          {
            "errorGroup": "311",
            "count": "6"
          },
          {
            "errorGroup": "315",
            "count": "5"
          },
          {
            "errorGroup": "316",
            "count": "5"
          },
          {
            "errorGroup": "319",
            "count": "9"
          },
          {
            "errorGroup": "320",
            "count": "5"
          },
          {
            "errorGroup": "322",
            "count": "364"
          },
          {
            "errorGroup": "326",
            "count": "7"
          },
          {
            "errorGroup": "331",
            "count": "2"
          },
          {
            "errorGroup": "332",
            "count": "97"
          },
          {
            "errorGroup": "333",
            "count": "97"
          },
          {
            "errorGroup": "334",
            "count": "97"
          },
          {
            "errorGroup": "336",
            "count": "94"
          },
          {
            "errorGroup": "337",
            "count": "48"
          },
          {
            "errorGroup": "339",
            "count": "12"
          },
          {
            "errorGroup": "340",
            "count": "48"
          },
          {
            "errorGroup": "341",
            "count": "1"
          },
          {
            "errorGroup": "346",
            "count": "3"
          },
          {
            "errorGroup": "349",
            "count": "2"
          },
          {
            "errorGroup": "355",
            "count": "1"
          },
          {
            "errorGroup": "366",
            "count": "1"
          },
          {
            "errorGroup": "371",
            "count": "1"
          },
          {
            "errorGroup": "374",
            "count": "63"
          },
          {
            "errorGroup": "378",
            "count": "12"
          },
          {
            "errorGroup": "379",
            "count": "1"
          },
          {
            "errorGroup": "380",
            "count": "4"
          },
          {
            "errorGroup": "381",
            "count": "6"
          },
          {
            "errorGroup": "385",
            "count": "1"
          },
          {
            "errorGroup": "393",
            "count": "9"
          },
          {
            "errorGroup": "405",
            "count": "13"
          },
          {
            "errorGroup": "408",
            "count": "1"
          },
          {
            "errorGroup": "410",
            "count": "47"
          },
          {
            "errorGroup": "416",
            "count": "9"
          },
          {
            "errorGroup": "419",
            "count": "1"
          },
          {
            "errorGroup": "421",
            "count": "1"
          },
          {
            "errorGroup": "425",
            "count": "4"
          },
          {
            "errorGroup": "428",
            "count": "3829"
          },
          {
            "errorGroup": "431",
            "count": "2"
          },
          {
            "errorGroup": "439",
            "count": "4"
          },
          {
            "errorGroup": "456",
            "count": "4"
          },
          {
            "errorGroup": "485",
            "count": "2"
          },
          {
            "errorGroup": "490",
            "count": "120"
          },
          {
            "errorGroup": "491",
            "count": "1"
          },
          {
            "errorGroup": "496",
            "count": "1"
          },
          {
            "errorGroup": "503",
            "count": "1"
          },
          {
            "errorGroup": "504",
            "count": "2"
          },
          {
            "errorGroup": "512",
            "count": "4"
          },
          {
            "errorGroup": "514",
            "count": "104"
          },
          {
            "errorGroup": "533",
            "count": "20"
          },
          {
            "errorGroup": "565",
            "count": "3"
          },
          {
            "errorGroup": "E01",
            "count": "71"
          },
          {
            "errorGroup": "L01",
            "count": "2"
          },
          {
            "errorGroup": "L02",
            "count": "1163"
          },
          {
            "errorGroup": "S09",
            "count": "26"
          },
          {
            "errorGroup": "S11",
            "count": "1393"
          },
          {
            "errorGroup": "217",
            "count": "4934"
          },
          {
            "errorGroup": "224",
            "count": "4934"
          },
          {
            "errorGroup": "225",
            "count": "4934"
          },
          {
            "errorGroup": "333",
            "count": "1"
          },
          {
            "errorGroup": "371",
            "count": "199"
          },
          {
            "errorGroup": "381",
            "count": "9"
          },
          {
            "errorGroup": "217",
            "count": "4"
          },
          {
            "errorGroup": "224",
            "count": "4"
          },
          {
            "errorGroup": "225",
            "count": "1"
          },
          {
            "errorGroup": "283",
            "count": "3"
          },
          {
            "errorGroup": "217",
            "count": "551"
          },
          {
            "errorGroup": "224",
            "count": "551"
          },
          {
            "errorGroup": "230",
            "count": "1"
          },
          {
            "errorGroup": "238",
            "count": "9"
          },
          {
            "errorGroup": "244",
            "count": "1"
          },
          {
            "errorGroup": "254",
            "count": "1"
          },
          {
            "errorGroup": "256",
            "count": "1"
          },
          {
            "errorGroup": "267",
            "count": "3"
          },
          {
            "errorGroup": "283",
            "count": "551"
          },
          {
            "errorGroup": "290",
            "count": "3"
          },
          {
            "errorGroup": "292",
            "count": "3"
          },
          {
            "errorGroup": "297",
            "count": "1"
          },
          {
            "errorGroup": "300",
            "count": "6"
          },
          {
            "errorGroup": "310",
            "count": "3"
          },
          {
            "errorGroup": "311",
            "count": "1"
          },
          {
            "errorGroup": "320",
            "count": "1"
          },
          {
            "errorGroup": "322",
            "count": "38"
          },
          {
            "errorGroup": "324",
            "count": "2"
          },
          {
            "errorGroup": "333",
            "count": "10"
          },
          {
            "errorGroup": "334",
            "count": "10"
          },
          {
            "errorGroup": "337",
            "count": "10"
          },
          {
            "errorGroup": "340",
            "count": "16"
          },
          {
            "errorGroup": "346",
            "count": "6"
          },
          {
            "errorGroup": "349",
            "count": "2"
          },
          {
            "errorGroup": "371",
            "count": "4"
          },
          {
            "errorGroup": "374",
            "count": "10"
          },
          {
            "errorGroup": "376",
            "count": "1"
          },
          {
            "errorGroup": "379",
            "count": "4"
          },
          {
            "errorGroup": "380",
            "count": "3"
          },
          {
            "errorGroup": "385",
            "count": "7"
          },
          {
            "errorGroup": "388",
            "count": "4"
          },
          {
            "errorGroup": "393",
            "count": "3"
          },
          {
            "errorGroup": "405",
            "count": "1"
          },
          {
            "errorGroup": "410",
            "count": "40"
          },
          {
            "errorGroup": "416",
            "count": "30"
          },
          {
            "errorGroup": "419",
            "count": "21"
          },
          {
            "errorGroup": "421",
            "count": "19"
          },
          {
            "errorGroup": "425",
            "count": "3"
          },
          {
            "errorGroup": "426",
            "count": "6"
          },
          {
            "errorGroup": "428",
            "count": "60"
          },
          {
            "errorGroup": "431",
            "count": "15"
          },
          {
            "errorGroup": "433",
            "count": "1"
          },
          {
            "errorGroup": "464",
            "count": "24"
          },
          {
            "errorGroup": "485",
            "count": "2"
          },
          {
            "errorGroup": "490",
            "count": "13"
          },
          {
            "errorGroup": "499",
            "count": "2"
          },
          {
            "errorGroup": "514",
            "count": "3"
          },
          {
            "errorGroup": "565",
            "count": "2"
          },
          {
            "errorGroup": "E01",
            "count": "2"
          },
          {
            "errorGroup": "L02",
            "count": "7"
          },
          {
            "errorGroup": "S09",
            "count": "3"
          },
          {
            "errorGroup": "S11",
            "count": "31"
          },
          {
            "errorGroup": "217",
            "count": "7"
          },
          {
            "errorGroup": "224",
            "count": "7"
          },
          {
            "errorGroup": "283",
            "count": "7"
          },
          {
            "errorGroup": "217",
            "count": "24"
          },
          {
            "errorGroup": "224",
            "count": "24"
          },
          {
            "errorGroup": "283",
            "count": "24"
          },
          {
            "errorGroup": "217",
            "count": "119"
          },
          {
            "errorGroup": "224",
            "count": "132"
          },
          {
            "errorGroup": "225",
            "count": "39"
          },
          {
            "errorGroup": "231",
            "count": "7"
          },
          {
            "errorGroup": "242",
            "count": "7"
          },
          {
            "errorGroup": "269",
            "count": "5"
          },
          {
            "errorGroup": "272",
            "count": "12"
          },
          {
            "errorGroup": "279",
            "count": "5"
          },
          {
            "errorGroup": "283",
            "count": "93"
          },
          {
            "errorGroup": "304",
            "count": "5"
          },
          {
            "errorGroup": "315",
            "count": "2"
          },
          {
            "errorGroup": "355",
            "count": "14"
          },
          {
            "errorGroup": "217",
            "count": "36"
          },
          {
            "errorGroup": "224",
            "count": "36"
          },
          {
            "errorGroup": "283",
            "count": "36"
          },
          {
            "errorGroup": "290",
            "count": "1"
          },
          {
            "errorGroup": "304",
            "count": "3"
          },
          {
            "errorGroup": "382",
            "count": "8"
          },
          {
            "errorGroup": "418",
            "count": "2"
          },
          {
            "errorGroup": "420",
            "count": "2"
          },
          {
            "errorGroup": "423",
            "count": "5"
          },
          {
            "errorGroup": "427",
            "count": "2"
          },
          {
            "errorGroup": "435",
            "count": "1"
          },
          {
            "errorGroup": "436",
            "count": "1"
          },
          {
            "errorGroup": "438",
            "count": "3"
          },
          {
            "errorGroup": "440",
            "count": "3"
          },
          {
            "errorGroup": "441",
            "count": "2"
          },
          {
            "errorGroup": "442",
            "count": "3"
          },
          {
            "errorGroup": "443",
            "count": "6"
          },
          {
            "errorGroup": "444",
            "count": "6"
          },
          {
            "errorGroup": "445",
            "count": "6"
          },
          {
            "errorGroup": "449",
            "count": "8"
          },
          {
            "errorGroup": "461",
            "count": "4"
          },
          {
            "errorGroup": "463",
            "count": "4"
          },
          {
            "errorGroup": "465",
            "count": "3"
          }
        ]


      }
    }, {
      //The delay will not produce a matrix view.
      name: "Waiting for Authorisation",
      type: "pivotModel",
      config: {
        query: {
          "model": "ModelInvalidAuthorisation",
          "startAt": 1,
          "maxRecords": 1000000,
          "select": {
            "delay": "daydiff('NowDate','InAuthStateFrom')"
          },
          "filter": [],
          "aggregate": {
            "count": "count()"
          },
          "orderBy": [
            "asc('delay')"
          ]
        },
        useSampleData: false,
        sampleData: [{
          delay: 10,
          count: 0
        }, {
          delay: 9,
          count: 2
        }, {
          delay: 8,
          count: 4
        }, {
          delay: 7,
          count: 6
        }, {
          delay: 6,
          count: 8
        }, {
          delay: 5,
          count: 10
        }, {
          delay: 4,
          count: 12
        }, {
          delay: 3,
          count: 14
        }, {
          delay: 2,
          count: 16
        }, {
          delay: 1,
          count: 18
        }, {
          delay: 0,
          count: 20
        }, ]
      }
    }, {
      name: "Replacement Bouncing",
      type: "pivotModel",
      config: {
        query: {
          "model": "dModelRepeatEdit",
          "startAt": 1,
          "maxRecords": 1000000,
          "select": {
            "bucket": "field('EditCount')" //Need to check the count
          },
          "filter": [],
          "aggregate": {
            "count": "count()"
          },
          "orderBy": ["asc('bucket')"]
        },
        useSampleData: false,
        sampleData: [{
          bucket: '>7',
          count: 17
        }, {
          bucket: '7',
          count: 1
        }, {
          bucket: '6',
          count: 4
        }, {
          bucket: '5',
          count: 3
        }, {
          bucket: '4',
          count: 0
        }, {
          bucket: '3',
          count: 6
        }, {
          bucket: '2',
          count: 13
        }]
      }
    }]

  };
}
