function createDashBoard(parameters) {

    getDate = function (days) {
        d = new Date();
        d.setDate(d.getDate() - days);
        return d.toISOString().substr(0, 10);
    };

    return {

        widgets: [
            //This widget Gives information regarding the last refresh date and time,
            //The dashboard version,
            //and the database the dashboard is getting its data from
            {
                layout: {
                    "sizeX": 30,
                    "sizeY": 2,
                    "row": 0,
                    "col": 0
                },
                name: "Information",
                type: "information",
                config: {
                    interval: 5000 //time in milliseconds //5 seconds
                },
                maximisedMode: false
            },

            //This widget represents the counts of the rules used for matching
            //Full system
            //Tabular visualisation
            {
                layout: {
                    "sizeX": 30,
                    "sizeY": 18,
                    "row": 2,
                    "col": 0
                },
                name: "Rules Used for Matching",
                type: "matchingTable",
                config: {
                    interval: 5000, //time in milliseconds //5 seconds
                    dataSourceName: "Matching Table"
                },
                maximisedMode: false
            }

        ],


        dataSources: [{
            name: "Matching Table",
            type: "pivotModel",
            config: {
                query: {
                    "model": "dMatchingCounts",
                    "startAt": 1,
                    "maxRecords": 1000000,
                    "select": {
                        "matchingRule": "field('MatchingRule')",
                        "confidence": "field('Confidence')",
                        "yesterdayCount": "field('YesterdayCount')",
                        "lastMonthCount": "field('LastMonthCount')",
                        "lastWeekCount": "field('LastWeekCount')",
                        "beforeCount": "field('BeforeCount')"
                    },
                    "filter": [
                    ],
                    "aggregate": {
                        "count": "count()"
                    },
                    "orderBy": [
                        "desc('Confidence')"
                    ]
                },
                useSampleData: false,
                sampleData: [{
                    matchingRule: "Exact match (rounded amount)",
                    confidence: 1.0,
                    count: 1,
                    yesterdayCount: 0,
                    lastWeekCount: 0,
                    lastMonthCount: 2233,
                    beforeCount: 0
                }, {
                    matchingRule: "Ref match, dates similar, amounts equal (with cent)",
                    confidence: 0.9,
                    count: 2,
                    yesterdayCount: 1,
                    lastWeekCount: 0,
                    lastMonthCount: 0,
                    beforeCount: 1
                }, {
                    matchingRule: "Exact match (with cents)",
                    confidence: 0.8,
                    count: 1,
                    yesterdayCount: 20,
                    lastWeekCount: 0,
                    lastMonthCount: 291,
                    beforeCount: 0
                }, {
                    matchingRule: "Ref match, dates similar, amounts equal",
                    confidence: 0.7,
                    count: 1,
                    yesterdayCount: 0,
                    lastWeekCount: 3,
                    lastMonthCount: 0,
                    beforeCount: 8
                }, {
                    matchingRule: "Manual Match",
                    confidence: 0.6,
                    count: 1,
                    yesterdayCount: 0,
                    lastWeekCount: 0,
                    lastMonthCount: 1,
                    beforeCount: 87
                }, {
                    matchingRule: "Ref match, amounts similar, dates equal",
                    confidence: 0.5,
                    count: 1,
                    yesterdayCount: 0,
                    lastWeekCount: 0,
                    lastMonthCount: 1,
                    beforeCount: 0
                }]

            }
        }]
    };
}
