function createDashBoard(parameters) {

  getDate = function(days) {
    d = new Date();
    d.setDate(d.getDate() - days);
    return d.toISOString().substr(0, 10);
  };

  return {

    widgets: [
      //This widget Gives information regarding the last refresh date and time,
      //The dashboard version,
      //and the database the dashboard is getting its data from
      {
        layout: {
          "sizeX": 30,
          "sizeY": 2,
          "row": 0,
          "col": 0
        },
        name: "BAU Recon",
        type: "bauReconciliations",
        config: {
          interval: 5000 //time in milliseconds //5 seconds
        },
        maximisedMode: true
      }
    ],


    dataSources: [{
      name: "bauRecon",
      type: "pivotModel",
      config: {
        query: {
          "model": "dModelSARBRejectedCount",
          "startAt": 1,
          "maxRecords": 1000000,
          "select": {
            "bucket": "field('RejectBucket')"
          },
          "filter": [],
          "aggregate": {
            "count": "count()"
          },
          "orderBy": ["asc('bucket')"]
        },
        useSampleData: false,
        sampleData: []
      }
    }]

  };
}
