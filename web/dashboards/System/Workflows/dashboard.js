function createDashBoard(parameters) {

  return {

    widgets: [
      //This widget Gives information regarding the last refresh date and time,
      //The dashboard version,
      //and the database the dashboard is getting its data from
      {
        layout: {
          "sizeX": 30,
          "sizeY": 2,
          "row": 0,
          "col": 0
        },
        name: "Information",
        type: "information",
        config: {
          interval: 5000 //time in milliseconds //5 seconds
        },
        maximisedMode: false
      },

      //This widget represents the counts of the rules used for matching
      //Full system
      //Tabular visualisation
      {
        layout: {
          "sizeX": 30,
          "sizeY": 18,
          "row": 2,
          "col": 0
        },
        name: "Workflows",
        type: "workFlows",
        config: {
          interval: 5000, //time in milliseconds //5 seconds
          dataSourceName: "workFlows"
        },
        maximisedMode: false
      }

    ],

   dataSources: [
      {           
      name: "workFlows",
      type: "workflows",
        config: {
          useSampleData: false,
          sampleData: [] 
        }
      }
    ]

  };
}
