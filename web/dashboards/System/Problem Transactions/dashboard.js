function createDashBoard(parameters) {

  return {

    widgets: [
      //This widget Gives information regarding the last refresh date and time,
      //The dashboard version,
      //and the database the dashboard is getting its data from
      {
        layout: {
          "sizeX": 30,
          "sizeY": 2,
          "row": 0,
          "col": 0
        },
        name: "Information",
        type: "information",
        config: {
          interval: 5000 //time in milliseconds //5 seconds
        },
        maximisedMode: false
      },

      //This widget represents the counts of the rules used for matching
      //Full system
      //Tabular visualisation
      {
        layout: {
          "sizeX": 30,
          "sizeY": 18,
          "row": 2,
          "col": 0
        },
        name: "Problem Transactions",
        type: "problemTransactions",
        config: {
          interval: 5000, //time in milliseconds //5 seconds
          dataSourceName: "Problem Transactions"
        },
        maximisedMode: false
      }

    ],

   dataSources: [
      {
        name: "Problem Transactions",
        type: "pivotModel",
        config: {
          query: 
          {
            "model": "dProblemTransactions",
            "startAt": 1,
            "maxRecords": 1000000,
            "select": {
              "reason": "field('Reason')"
            },
            "filter": [
            ],
            "aggregate": {
              "count": "count()"
            },
            "orderBy": [
              "desc('reason')"
            ]
          },
          useSampleData: false,
          sampleData: [{
            reason: "Reason 1",
            count: 1
          }, {
            reason: "Reason 2",
            count: 2
          }, {
            reason: "Reason 3",
            count: 4
          }, {
            reason: "Reason 4",
            count: 2
          }, {
            reason: "Reason 5",
            count: 9
          }, {
            reason: "Reason 6",
            count: 23
          }]
        }
      }
    ]

  };
}
