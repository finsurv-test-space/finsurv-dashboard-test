// This defines all the widgets that exist in the toolbox.
// This list is used to load all the factories that handle the creation of widgets on the dashboard.
// This file is processed before RequireJS has booted up. Therefore it is not in a define block.
// This file drives the RequireJS config as well as the Angular config.
var widgetList = [

    {
        'name': 'delayMatrix',
        'image': 'glyphicon glyphicon-adjust',
        'category': ''
    },
    {
        'name': 'bau',
        'image': 'glyphicon glyphicon-adjust',
        'category': ''
    },
    {
        'name': 'pareto',
        'image': 'glyphicon glyphicon-adjust',
        'category': ''
    },
    {
        'name': 'barGraph',
        'image': 'glyphicon glyphicon-adjust',
        'category': ''
    },
    {
        'name': 'pieChart',
        'image': 'glyphicon glyphicon-adjust',
        'category': ''
    },
    {
        'name': 'information',
        'image': 'glyphicon glyphicon-adjust',
        'category': ''
    },
    {
        'name': 'matchingTable',
        'image': 'glyphicon glyphicon-adjust',
        'category': ''
    },
    {
        'name': 'workFlows',
        'image': 'glyphicon glyphicon-adjust',
        'category': ''
    },
    {
        'name': 'problemTransactions',
        'image': 'glyphicon glyphicon-adjust',
        'category': ''
    },
    {
        'name': 'bauReconciliations',
        'image': 'glyphicon glyphicon-adjust',
        'category': ''
    }

];
