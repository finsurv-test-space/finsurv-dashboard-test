define([], function () {

    // Define the navigation tree:
    return [

        {
            name: 'Overview',
            displayName: 'Overview',
            cssClass: 'i_system',
            children: [
                {
                    name: 'Maintaining Control',
                    displayName: 'Maintaining Control',
                    description: 'View and control feeds',
                    cssClass: 'i_dashboard'
                },
                {
                    name: 'Remediation',
                    displayName: 'Remediation',
                    description: 'View and control feeds',
                    cssClass: 'i_dashboard'
                },
                {
                    name: 'Reconciliation',
                    displayName: 'Reconciliation',
                    description: 'View and control feeds',
                    cssClass: 'i_dashboard'
                }
            ]
        },
        {
            name: 'System',
            displayName: 'System',
            cssClass: 'i_system',
            children: [
                {
                    name: 'Workflows',
                    displayName: 'Workflows',
                    description: 'View and control feeds',
                    cssClass: 'i_dashboard'
                },
                {
                    name: 'Problem Transactions',
                    displayName: 'Problem Transactions',
                    description: 'View and control feeds',
                    cssClass: 'i_dashboard'
                }
            ]
        }

    ];

});
