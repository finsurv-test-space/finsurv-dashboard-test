// Get the app dependencies so we can transform the data into the necessary config structures that we need for RequireJS:
// NOTE: this is defined in app.dependencies.js.

// The code below is working to generate the structure below from the app.dependencies.js data:
/*
 require.config({
   shim       : {
     app                          : ['angular', 'less'],
     "angular"                    : {exports: "angular"},
     'angular-ui-router'          : ['angular'],
     'angular-resource'           : ['angular'],
     'angular-bootstrap'          : ['angular'],
     'angular-chosen'             : ['angular'],
     'angular-ui-sortable'        : ['angular'],
     'ng-sortable'                 :['angular'],
     'localytics.directives'      : ['angular'],
     'angular-bootstrap-templates': ['angular-bootstrap'],
     // graphing
     d3                           : { exports: 'd3' },
     //nvd3                         : ['d3'],
     'angularjs-nvd3-directives'  : ['angular', 'nvd3'],
     // --------
     underscore                   : { exports: "_"},
     less                         : { exports: 'less'},
     'angular-ui-grid'            : { exports: 'ngGrid' },
     'angular-aside'              : { exports: 'ngAside' },
     'angular-gridster'           : ['angular']
   },
   packages: [
     // This is a Promise Library called 'When'.
     // https://github.com/cujojs/when/blob/master/docs/installation.md
     { name: 'when', location: '/bower_components/when', main: 'when' }
   ],

   paths      : {
     app                          : ['/js/app'],
     require						 : ['/bower_components/requirejs/require'],
     angular                      : ['/bower_components/angular/angular'],
     'angular-resource'           : ['/bower_components/angular-resource/angular-resource'],
     underscore                   : ['/bower_components/underscore/underscore'],
     'angular-ui-router'          : ['/bower_components/angular-ui-router/release/angular-ui-router'],
     'angular-ui-grid'            : ['/bower_components/angular-ui-grid/ui-grid'],
     'angular-ui-sortable'        : ['/bower_components/angular-ui-sortable/sortable'],
     'angular-bootstrap'          : ['/bower_components/angular-bootstrap/ui-bootstrap'],
     'ng-sortable'                 :['/bower_components/ng-sortable/dist/ng-sortable'],
     'angular-bootstrap-templates': ['/bower_components/angular-bootstrap/ui-bootstrap-tpls.min'],
     'd3'                         : ['/bower_components/d3/d3.min'],
     //'nvd3'                       : ['/bower_components/nvd3/nv.d3'],
     'angularjs-nvd3-directives'  : ['/bower_components/angularjs-nvd3-directives/dist/angularjs-nvd3-directives'],
     'angular-chosen'             : ['/bower_components/angular-chosen-localytics/chosen'],
     'bootstrap-tour'             : ['/bower_components/bootstrap-tour/build/js/bootstrap-tour.min'],
     'localytics.directives'      : ['/bower_components/angular-chosen-localytics/chosen'],
     'angular-aside'              : ['/bower_components/angular-aside/dist/js/angular-aside.min'],
     d3Module                     : ['/js/d3Module'],
     less                         : ['/resources/less-1.7.5'], // TODO: dev only,
     'angular-gridster'            :['/bower_components/angular-gridster/dist/angular-gridster.min']
   },
 */

// Create the shim configuration object:
var shimConfig = {};
appDependencies.filter(function(appDependency)
  {
    // only select dependencies that have the shim specified:
    return appDependency.shim;
  })
    .map(function (shimDependency)
  {
    // Write the shim config onto the config object:
    shimConfig[shimDependency.require] = shimDependency.shim;
  });

// Create the package configuration object:
var packageConfig = [];
appDependencies.filter(function(appDependency)
  {
    // only select dependencies that are packages:
    return appDependency.type == 'package';
  })
  .forEach(function (packageDependency)
  {
    // Write the package config onto the config object:
    packageConfig.push({
      name: packageDependency.require,
      location: packageDependency.path,
      main: packageDependency.main || packageDependency.require
    });
  });

// Now add a package for each widget that we will load:
widgetList.forEach(function (widget)
{
  // Write the package config onto the config object:
  packageConfig.push({
    name: 'widgets.' + widget.name,
    location: './app/widgets/' + widget.name,
    main: widget.name + '.js'
  });
});

// Now add a package for each navigation provider that we will load:
navigationProviderList.forEach(function (navigationProvider)
{
  // Write the package config onto the config object:
  packageConfig.push({
    name: 'navigation-providers.' + navigationProvider.name,
    location: './app/navigation-providers/' + navigationProvider.name,
    main: navigationProvider.name + '.js'
  });
});

// Now add a package for each data source that we will load for dashboards:
dataSourceList.forEach(function (dataSource)
{
  // Write the package config onto the config object:
  packageConfig.push({
    name: 'data-sources.' + dataSource.name,
    location: './app/data-sources/' + dataSource.name,
    main: dataSource.name + '.js'
  });
});

// Create the path configuration object:
var pathConfig = {};
appDependencies.filter(function(appDependency)
  {
    // only select dependencies that are files:
    return appDependency.type == 'file';
  })
  .forEach(function (fileDependency)
  {
    // Write the path config onto the config object:
    pathConfig[fileDependency.require] = fileDependency.path;
  });

require.config({
  baseUrl: '/finsurv-dashboard/', // The base URL defaults to the data-main folder, which is this file 'main.js' in the 'js' folder. Files can be referenced without a leading slash and without a trailing '.js' when they are under the baseURL.
  // The shim section is only for non-require libraries that need to have their dependencies registered explicitly. For require-friendly libraries you don't need the shim.
  shim        : shimConfig,
  packages    : packageConfig,
  paths       : pathConfig,
  waitSeconds : 30
});

// Load in the Promise shim for When:
// https://github.com/cujojs/when/blob/master/docs/es6-promise-shim.md
require(['require', 'when/es6-shim/Promise']);

// Create the list of require dependencies for the app:
var requireDependencies = [];
appDependencies.filter(function(appDependency)
  {
    // only select dependencies that are not globals:
    return appDependency.type != 'global';
  })
  .forEach(function (fileDependency)
  {
    // Define the dependency:
    requireDependencies.push(fileDependency.require);
  });

// Create the list of angular dependencies for the app:
var angularDependencies = [];
appDependencies.filter(function(appDependency)
{
  // only select dependencies that have the angular definition:
  return appDependency.angular;
})
  .forEach(function (angularDependency)
  {
    // Define the dependency:
    angularDependencies.push(angularDependency.angular);
  });


// This is where we start loading up code:
define(['require',                                                      // The require framework
    'angular', 'less',                                                  // UI Frameworks
    'app/widgets/widget-loader',                                        // Loader Framework for Dashboard UI (This is what loads the instance specific configs below)
    'app/navigation-providers/navigation-provider-loader',              // Loader Framework for navigation providers (This is what loads the instance specific configs below)
    'app/data-sources/data-source-loader'                               // Loader Framework for data sources (This is what loads the instance specific configs below)
  ].concat(requireDependencies),
  function (require, angular, less, widgetLoaderPromise, navigationProviderLoaderPromise, dataSourceLoaderPromise) {
    // If we get here then all the static dependencies have been resolved.
    // We need to wait for the dynamic dependencies to complete:
    Promise.all([widgetLoaderPromise, navigationProviderLoaderPromise, dataSourceLoaderPromise])
      .then(function()
      {
        // We managed to resolve all static and dynamic dependencies.

        //less.watch();

        // Define the actual application module to depend on the static and dynamic modules:
        angular.module('FinsurvDashboardClient', angularDependencies);

        // Bootstrap the application:
        angular.bootstrap(document, ['FinsurvDashboardClient'], { strictDi: true });

      })
      .catch(function(err)
      {
        console.error('Could not load all the dynamic dependencies (widgets, data sources and dashboards).')
        console.log(err);
      });
  });
