/**
 * Created by daniel on 15/09/02.
 */
define(['angular', 'login/login.module','config.list'], function (angular, loginModule,configList) {
  'use strict';
  //Solution is from http://jasonwatmore.com/post/2015/03/10/AngularJS-User-Registration-and-Login-Example.aspx
  loginModule.factory('authenticationService', authenticationService);

  //Have to instantiate this variable before the return statement
  // Base64 encoding service used by AuthenticationService
  var Base64 = {

    keyStr: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=',

    encode: function (input) {
      var output = "";
      var chr1, chr2, chr3 = "";
      var enc1, enc2, enc3, enc4 = "";
      var i = 0;

      do {
        chr1 = input.charCodeAt(i++);
        chr2 = input.charCodeAt(i++);
        chr3 = input.charCodeAt(i++);

        enc1 = chr1 >> 2;
        enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
        enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
        enc4 = chr3 & 63;

        if (isNaN(chr2)) {
          enc3 = enc4 = 64;
        } else if (isNaN(chr3)) {
          enc4 = 64;
        }

        output = output +
            this.keyStr.charAt(enc1) +
            this.keyStr.charAt(enc2) +
            this.keyStr.charAt(enc3) +
            this.keyStr.charAt(enc4);
        chr1 = chr2 = chr3 = "";
        enc1 = enc2 = enc3 = enc4 = "";
      } while (i < input.length);

      return output;
    },

    decode: function (input) {
      var output = "";
      var chr1, chr2, chr3 = "";
      var enc1, enc2, enc3, enc4 = "";
      var i = 0;

      // remove all characters that are not A-Z, a-z, 0-9, +, /, or =
      var base64test = /[^A-Za-z0-9\+\/\=]/g;
      if (base64test.exec(input)) {
        window.alert("There were invalid base64 characters in the input text.\n" +
            "Valid base64 characters are A-Z, a-z, 0-9, '+', '/',and '='\n" +
            "Expect errors in decoding.");
      }
      input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

      do {
        enc1 = this.keyStr.indexOf(input.charAt(i++));
        enc2 = this.keyStr.indexOf(input.charAt(i++));
        enc3 = this.keyStr.indexOf(input.charAt(i++));
        enc4 = this.keyStr.indexOf(input.charAt(i++));

        chr1 = (enc1 << 2) | (enc2 >> 4);
        chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
        chr3 = ((enc3 & 3) << 6) | enc4;

        output = output + String.fromCharCode(chr1);

        if (enc3 != 64) {
          output = output + String.fromCharCode(chr2);
        }
        if (enc4 != 64) {
          output = output + String.fromCharCode(chr3);
        }

        chr1 = chr2 = chr3 = "";
        enc1 = enc2 = enc3 = enc4 = "";

      } while (i < input.length);

      return output;
    }
  };

  //Todo Dummy authentication implemented here, follow comments in the code to setup real authentication
  /*The authentication service contains methods for authenticating a user, setting credentials and
   * clearing credentials from the HTTP "Authorization" headers used by the AngularJS $http service, so effectively logging in and out.*/
  authenticationService.$inject = ['$http', '$cookieStore', '$rootScope', '$timeout'];
  function authenticationService($http, $cookieStore, $rootScope, $timeout) {
    var service = {};

    service.login = login;
    service.setCredentials = setCredentials;
    service.clearCredentials = clearCredentials;
    service.setUserCookies = setUserCookies;

    return service;

    function login(username, password, callback) {
      $http.post(configList.serverUrl+'/resource/login/', {username: username, password: password}, {
        headers: {
          "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
          "X-Requested-With": "XMLHttpRequest"
        },
        transformRequest: function (obj) {
          var str = [];
          for (var p in obj)
            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
          return str.join("&");
        }
      })
          .then(function (data) {
            //$http.get("http://localhost:9090/resource/secure/whoamI").then(function (response) {
            //  var user = UserService.GetUser();
            //  console.log('User object',user);
            //  callback({success: true, data: response});
            //}, function (error) {
            //  var err = {};
            //  if (response.status = 401) {
            //    err.message = "Invalid Login";
            //  }
            //  else {
            //    err.message = "Server down"
            //  }
            //
            //  callback({success: false, error: err})
            //})

            //User is authenticated
            callback({success: true, data: data});

          }, function (err) {
            //User is not authenticated
            callback({success: false, error: err});
          })
    }

    //Take user credentials and store them in the cookie
    function setCredentials(username, password, user) {
      /*todo Create some auth data. This is currently not used
       Deciding if it is necessary to implement this on server side.*/
      var authdata = Base64.encode(username + ':' + password);
      var cookies = {
        currentUser: user,
        authdata: authdata
      };
      $http.defaults.headers.common['Authorization'] = 'Basic ' + authdata; // jshint ignore:line
      $cookieStore.put('cookies', cookies);
    }

    function setUserCookies(user) {
      if (user.loggedIn) {
        var cookies = $cookieStore.get('cookies');
        cookies.currentUser = user;
        $http.defaults.headers.common['Authorization'] = 'Basic ' + cookies.authdata;
        $cookieStore.put('cookies', cookies);
      }
    }

    function clearCredentials() {
      $cookieStore.remove('cookies');
      $http.defaults.headers.common.Authorization = 'Basic ';
    }
  }

  return loginModule;
})