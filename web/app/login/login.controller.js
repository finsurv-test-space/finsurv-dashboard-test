/**
 * Created by daniel on 15/09/02.
 */
define(['angular', 'login/login.module'], function (angular, loginModule) {
  loginModule.controller('LoginController', LoginController);
  //Todo in the example i got this from the guy has a flash service he has created seems like some type of messaging service
  //Todo for now commenting this out and will see what to do about it
  //Code taken from https://github.com/cornflourblue/angular-registration-login-example/blob/master/login/login.controller.js
  LoginController.$inject = ['$state','$stateParams','$scope','user', 'loginService','$window','$injector'/*'FlashService'*/];
  function LoginController($state,$stateParams,$scope , user, loginService,$window,$injector/*,FlashService*/) {

    $scope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
      if((fromState.name!="login"&&fromState.url!="^")||toParams.userJustLoggedOut == true){
        toParams.userJustLoggedOut=false;

        // Need to cause a site reload when logging out so that the when a new user logs in they are given fresh data.
        $window.location.href = '';
      }
    })

    var vm = this;
    vm.login = login;
    vm.logout =logout;
    vm.currentUser=user;
    vm.dataLoading = false;

    function login() {
      vm.dataLoading = true;
      loginService.login(vm.username, vm.password).then(function (response) {
        vm.dataLoading= false;
        vm.error="";
        if (response) {
          user.setUser(response);
          $state.go('dashboard');
        }
        //else{
        //  //FlashService.Error(response.message);
        //  vm.dataLoading = false;
        //}
      },function(error){
        vm.notLoggedIn=true;
        vm.error=error.data.message;
        vm.dataLoading = false;
      })
    };

    function logout(){
      vm.dataLoading= false;
      loginService.logout().then(function(){
        // User has clicked the logout button now need to set the flag to let the system know
        $stateParams.userJustLoggedOut = true
        $state.go('login', $stateParams, {reload: true}); //second parameter is for $stateParams
      });

    }

  }

  return loginModule;
})