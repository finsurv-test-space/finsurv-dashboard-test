/**
 * Created by daniel on 15/09/03.
 */

define(['angular', 'login/login.module'], function (angular, loginModule) {
  loginModule.factory('loginRunService', loginRunService);
  loginRunService.$inject = ['$rootScope', '$state', '$injector', 'loginModal', 'authenticationService', 'user', 'mainMenuDropdownService', 'loginService'];
  function loginRunService($rootScope, $state, $injector, loginModal, authenticationService, user, mainMenuDropdownService, loginService) {
    var logoutDropdownItem = {name: 'Logout', icon: 'fa-power-off', click: loginService.logout};
    mainMenuDropdownService.registerDropdownItem(logoutDropdownItem);
    var service = {};
    //We are going to keep track if the state has been rerouted by the code below
    //in order to avoid an infinite loop;
    var shouldAllowStateTransition = false;
    service.loginInitialisation = loginInitialisation;
    //Star the polling service as soon as login module is defined.
    user.poll();
    function loginInitialisation() {
      registeringOnStateChange();

    }

    function registeringOnStateChange() {
      $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {

            if (shouldAllowStateTransition) {
              // We always want to let the transition occur, no matter what case it is.
              // We are either going to an unsecured state, or we have completed authentication for the user and we now want to complete the state transition.

              // Flag that the next state change needs to be checked:
              shouldAllowStateTransition = false;
              //Clicking on the logo on the login screen takes the user to the
              //dashboard.
              //This if is to make sure the user is only taken to the dashboard if they are signed in
              if(fromState.name=='login'&&toState.name=='dashboard'&&!user.loggedIn){
                shouldAllowStateTransition = true;
                event.preventDefault();
                $state.go('login')
              }
              return;
            }

            if (toState.data.requireLogin) {

              //We always want to query the server for a state change that requires a login(even if they are already logged in)
              // This ensures the server session is still alive

              // transitionTo() promise will be rejected with
              // a 'transition prevented' error
              event.preventDefault();
              user.queryUser().then(function (response) {
                //Replace our user with the data form server in case it has been modified.
                user.setUser(response);
                // We got a response from the server and need to complete the state transition
                shouldAllowStateTransition = true;
                $state.go(toState.name, toParams);
              }, function (error) {
                authenticationService.clearCredentials();
                user.clearUser();
                //Only popup the modal if the user tries to access a restricted page from within the site
                if (fromState.url != '/login' && fromState.name != "" && fromState.url != "^") {
                  // We must show the modal.
                  loginModal()
                      .then(function (response) {
                        //Go to intended state with successful login
                        if (response.data) {
                          shouldAllowStateTransition = true;
                          return $state.go(toState.name, toParams);
                        } else {
                          shouldAllowStateTransition = true;
                          console.error("Something went wrong: Expecting login to be successful but was not");
                          return $state.go(fromState.name, fromParams);
                        }
                      })
                      .catch(function () {
                        // Pull out the navigation tree:
                        // NOTE: We use the injector here because we only want to boot up dependencies once we have the initial user object.
                        //       We originally had the injector as a normal function dependency but that meant that the navigation tree service (and all navigation provider dependencies)
                        //       boot up before we have a session with the server, causing lots of web server calls to fail.
                        var navigationTree = ($injector.get('navigationTree'));
                        if(fromState.data && fromState.data.requireLogin ==true){
                          return $state.go('login')
                        }
                        else{
                          return navigationTree.selectTabAndDashboardFromStateUrl(fromState.url)
                        }
                      });
                }
                else {
                  // We are coming in from outside of the site.
                  shouldAllowStateTransition = true;
                  console.log("error logging in go to login")
                  return $state.go('login');

                }
              });
            }
          }



      )
    }

    return service;
  }

  return loginModule;
})
