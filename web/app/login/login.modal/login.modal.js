/**
 * Created by daniel on 15/09/02.
 */
define(['login/login.module'],function(loginModule){
  loginModule.service('loginModal',['$uibModal', 'user', function($modal,user){
    function assignCurrentUser(response){
      if(response.data) {
        user.setUser(response.data);
      }
      else{
        //We got back the user is signed in but the response.data was empty, sign user out.
        user.clearUser()
      }
    };
    return function(){
      var instance = $modal.open({
        templateUrl:'./app/login/login.modal/login.modal.html',
        controller:'LoginModalController',
        controllerAs:'LoginModalCtrl'
      });
      instance.result.then(assignCurrentUser);
      return instance.result;
    };
  }]);
  return loginModule;
})
