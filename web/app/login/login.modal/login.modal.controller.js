/**
 * Created by daniel on 15/09/02.
 */
define(['login/login.module'], function (loginModule) {
  loginModule.controller('LoginModalController', LoginModalCtrl);
  LoginModalCtrl.$inject = ['$scope', 'loginService'];
  function LoginModalCtrl($scope, loginService) {
    this.cancel = function () {
      $scope.$dismiss();
    }
    this.submit = function (username, password) {
      loginService.login(username, password).then(function (response) {
        $scope.$close(response);
      }, function (response) {
        if (response.data) {
          //Error message to be displayed in the login modal
          $scope.errorMessage = response.data.message;
        }
      })
    }
  }

  return loginModule;
})