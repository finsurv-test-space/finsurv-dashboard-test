/**
 * Created by daniel on 15/09/01.
 */
define(['require', 'login/login.module'], function (require, loginModule) {
  loginModule.config(['$stateProvider', function ($stateProvider) {
    $stateProvider.state(
        'login',
        {
          url: '/login',
          data: {
            requireLogin: false
          },
          params:{
            //This is set to true as soon as the user clicks the logout button on the login screen
            userJustLoggedOut:false
          },
          views: {
            '': {
              templateUrl: require.toUrl('login/login.html'),
              controller: 'LoginController',
              controllerAs: 'vm'
            }
          }
        }
    )
  }]);
  return loginModule;
})