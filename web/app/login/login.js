define(['./login.module',
  './login.authentication.service',
  'login/login.controller',
  'login/login.logo.directive',
  'login/login.run.service',
  'login/login.state',
  'login/login.service',
  'login/login.modal/login.modal.controller',
  'login/login.modal/login.modal'
], function (loginModule) {
  return loginModule;
})