/**
 * Created by daniel on 15/09/02.
 */
define(['angular',
      'angular-cookies',
      'angular-bootstrap',
      'angular-bootstrap-templates',
      'angular-ui-router',
      'user',
      'root'
    ],
    function (angular) {
      var loginModuleDependencies = [
        'ngCookies',
        'ui.bootstrap',
        'ui.bootstrap.tpls',
        'ui.router',
        'user'
      ];
      var loginModule = angular.module('login', loginModuleDependencies);
      //loginModule.$inject = ['$rootScope', '$state','$cookieStore', '$http', 'loginModal','AuthenticationService'];
      loginModule.run(runBlock);
      runBlock.$inject = ['loginRunService'];
      function runBlock(loginRunService) {
        loginRunService.loginInitialisation();
      }

      return loginModule;
    })