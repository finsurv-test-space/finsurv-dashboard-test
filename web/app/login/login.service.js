/**
 * Created by daniel on 15/09/03.
 */
define(['angular', 'login/login.module', 'config.list'], function (angular, loginModule, configList) {
  loginModule.factory('loginService', loginService);
  loginService.$inject = ['authenticationService','$q', 'user', '$http', '$state'];
  function loginService(authenticationService, $q, user, $http, $state) {
    var service = {};
    service.login = login;
    service.logout = logout;
    return service;
    function login(username, password) {
      var loginDefer = $q.defer();
      //This checks if the user is authenticated and if the user is then sets the user data
      authenticationService.login(username, password, function (response) {
        if (response.success) {
          //User is authenticated get user data
          user.queryUser().then(function (user) {
            if (user) {
              //Setup the cookie with the user data.
              authenticationService.setCredentials(username, password, user);
              loginDefer.resolve(user);
            }
            else {
              //User was able to login however unable to obtain user information
              var error = {}
              //The error is setup in this way as this is the general format returned from the server
              //and the format should be kept consisitent.
              error.data = {message: "Could not login"}
              loginDefer.reject(error);
            }
          }, function (error) {
            loginDefer.reject(error);
          })
        } else {
          //Resolve the promise with the error object if the user is not authenticated
          loginDefer.reject(response.error);
        }
      });
      return loginDefer.promise;

    };

    function logout() {
      var logOutDefer = $q.defer();
      $http.get(configList.serverUrl + '/resource/logout')
          .then(function (data) {
            user.clearUser();
            logOutDefer.resolve(data);
            $state.go('login');
            //This is the code previously called on logout completed
            //notifyPopup('Successfully logged Off!');
            //window.location = '/';
          },
          function () {
            logOutDefer.reject('Could not log out. The server might be down...');
            //todo Do I need to add a state.go('login') over here
            //This is the code previously called on logout completed
            //notifyPopup('Could not log out. The server might be down...');
          })

      authenticationService.clearCredentials();
      return logOutDefer.promise;
    }
  }

  return loginModule;
})
