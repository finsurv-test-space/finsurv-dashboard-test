/**
 * Created by daniel on 15/09/04.
 */
define(['login/login.module'],function(loginModule){

  loginModule.directive('synthesisLogo', function () {
        return {
          restrict: 'E',
          templateUrl: require.toUrl('login/login.synthesis.logo.html')
        }
      })
  return loginModule;
})