'use strict';

define(['angular', 'angular-ui-router'], function (angular, uiRouter) {

  // Define what other module this one depends on:
  var moduleDependencies = [
    'ui.router',
    'navigation',
    //'notifications',
    'widgetRegistry',
    'dataSourceRegistry',
    //'settings',
    //'user'

  ];

  // Define the module:
  var module = angular.module('dashboards', moduleDependencies).run(runBlock);
  runBlock.$inject = ['$state','$rootScope','$injector']
  function runBlock($state,$rootScope,$injector){
    $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
      //This check is to accomodate the case when the user is on a selected dashboard and then clicks the
      //home/marketStream logo button that it will take the user to the default selected tab menu.
      if(toState.name=="dashboard"){
        // Pull out the navigation tree:
        // NOTE: We use the injector here because we only want to boot up dependencies once we have the initial user object.
        //       We originally had the injector as a normal function dependency but that meant that the navigation tree service (and all navigation provider dependencies)
        //       boot up before we have a session with the server, causing lots of web server calls to fail.
        var navigationTree = $injector.get('navigationTree');
        navigationTree.selectedTabIndex = undefined;
        navigationTree.selectDefaultTab();
      }
      //Only set the selected tab index to undefined if the from state is not /dashboard
      //The reason for including the parent property is that the url for the different dashboards are dynamic
      //so they won't have dashboard as part of their url.
      //Also check that there is no pending dashboard state to be changed to.
      else if(toState.name != 'login' && fromState.url!='/dashboard'&&fromState.parent!="dashboard"&&!toParams.pendingState){

        // Pull out the navigation tree:
        // NOTE: We use the injector here because we only want to boot up dependencies once we have the initial user object.
        //       We originally had the injector as a normal function dependency but that meant that the navigation tree service (and all navigation provider dependencies)
        //       boot up before we have a session with the server, causing lots of web server calls to fail.
        var navigationTree = $injector.get('navigationTree');
        navigationTree.selectedTabIndex = undefined;
      };
    });
  }
  return module;
});


