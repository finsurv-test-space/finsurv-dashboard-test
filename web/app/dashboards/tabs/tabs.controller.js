//Had to replace . with dashboard here otherwise the files were not found
define(['dashboards/dashboards.module', 'angular', 'dashboards/dashboards.constants'], function (module, angular) {

  // Configure a controller:
  module.controller('dashboardTabsController', controller);

  // Define the dashboard controller:
  controller.$inject = ['$scope', '$state', '$stateParams', 'navigationTree', 'dynamicDashboardStates'];
  function controller($scope, $state, $stateParams, navigationTree, dynamicDashboardStates) {
    //$scope.currentUser = user;
    //$scope.checkSettingsPermissions = settingsService.checkSettingsPermissions;
    //$scope.goToSettings = function () {
    //  $state.go('settings')
    //}

    // tradeFeedData.subscribe(updateTabStatusColour);
    // function updateTabStatusColour() {
    //   var feeds = tradeFeedData.feeds;
    //   if ($scope.navTree && feeds) {
    //     feeds.forEach(function (feed) {
    //       switch (feed.state) {
    //         case tradeFeedFeedState.RUNNING.value:
    //           return changeTabColour(feed.name, 'status-green')
    //         case tradeFeedFeedState.STOPPED.value:
    //           return changeTabColour(feed.name, 'status-red')
    //         case tradeFeedFeedState.ERROR.value:
    //           return changeTabColour(feed.name, 'status-purple')
    //         case tradeFeedFeedState.QUERYING.value:
    //           return changeTabColour(feed.name, 'status-blue')
    //         default:
    //           return changeTabColour(feed.name, 'status-yellow')
    //       }
    //     })

    //   }
    // }
    // function changeTabColour(feedName, statusColour) {
    //   $scope.navTree.forEach(function (treeItem) {
    //       if (feedName == treeItem.displayName) {
    //         treeItem.statusColour = statusColour;
    //       }
    //   })
    // }
    
    //navigationTree.subscribe(updateTabStatusColour);
    navigationTree.subscribe(getTree);
    $scope.selectTab = navigationTree.selectTab;
    function getTree(tree) {
      $scope.navTree = tree;
      //Have to call this first as this function calls both the select tab select dashboard which in turn populated main and side menu.
      if($state.current.name=="dashboard"){
        navigationTree.selectDefaultTab();
      }

      if ($stateParams.pendingState) {
        var pendingState=$stateParams.pendingState;
        $stateParams.pendingState = null ;
        navigationTree.selectTabAndDashboardFromStateUrl(pendingState);
      }
    };
  }
});


