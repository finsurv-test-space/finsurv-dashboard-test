'use strict';

define([
  'require',
  './tabs/tabs.controller',
  './menu/menu.controller',
  './layout/layout.controller',
  './toolbox/toolbox.controller',
  './widgets/widgets.controller',
  './data-sources/data-sources.controller',
  //'./notifications/notifications.menu.controller'

], function (require)
{

  return {
    name: 'dashboard',
    parent:'root',
    url: '/dashboard',
    controllerAs:'dashboardController',
    //This is set when the user refreshes the page or is trying to be directed to one of the dynamic dashboard states.
    //As the state do not exist at config to when the url is validated when need to store the dynamic state here until we can succesfully call it.
    params:{pendingState:null},
    // abstract: true, // This needs to be a real state (not abstract) because we need to be able to navigate to it so all the dynamic states can be created.
    //resolve: {
    //},
    data: {
      requireLogin: true
    },
    views: {
      // See here why we need the '@dashboard' after tabs.
      // Otherwise it would be the tabs view on the root state.
      // https://github.com/angular-ui/ui-router/wiki/Multiple-Named-Views#view-names---relative-vs-absolute-names
      // 'view@absolute.state'

      // This is where the top level items in the navTree will be shown as tabs:
      'main-menu-tabs@root': {
        templateUrl: require.toUrl('./tabs/tabs.html'),
        controller: 'dashboardTabsController'
      },
      // This is where the children of the selected tab will be rendered as a menu tree structure:
      'side-menu@root': {
        templateUrl: require.toUrl('./menu/menu.html'),
        controller: 'dashboardMenuController'
      },
      // This is where the actual widgets will be placed for the dashboard:
      'layout@root': {
        templateUrl: require.toUrl('./layout/layout.html'),
        controller: 'dashboardLayoutController'
      },
      // This is where the toolbox for the dashboard will be placed for the dashboard:
      'toolbox@dashboard': {
        templateUrl: require.toUrl('./toolbox/toolbox.html'),
        controller: 'dashboardToolboxController'
      }
      // 'notifications-menu@dashboard': {
      //   templateUrl: require.toUrl('./notifications/notifications.menu.html'),
      //   controller: 'notificationsMenuController'
      // }
    }

  }

});


