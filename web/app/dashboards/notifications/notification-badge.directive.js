//Had to replace . with dashboard here otherwise the files were not found
define(['dashboards/dashboards.module', 'require'], function (module, require) {

  // Define the directive:
  module.directive('notificationBadge', notificationBadge);
  notificationBadge.$inject = [];
  function notificationBadge()
  {
    return {
      restrict: 'E',
      templateUrl: require.toUrl('./notification-badge.html'),
      scope: {
        value: '=value',
        inlineStyle: '@inlineStyle'
      }
    }
  }

});


