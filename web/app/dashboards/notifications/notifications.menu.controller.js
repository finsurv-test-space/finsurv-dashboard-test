//Had to replace . with dashboard here otherwise the files were not found
define(['dashboards/dashboards.module', 'angular'], function (module, angular) {

  // Configure a controller:
  module.controller('notificationsMenuController', controller);

  // Define the dashboard controller:
  controller.$inject = ['$scope','navigationTree', 'notificationsData','$state']; // notificationsData comes from notifications-data.service in the notifications module.
  function controller($scope, navigationTree,notificationsData,$state)
  {
    // Get the notifications data:
    notificationsData.subscribe(updateData);

    /*
     This gets called whenever trade feed data is available or updated.
     */
    function updateData(notificationsData)
    {
      // Only get unread notifications:
      var unreadNotifications = notificationsData.notifications.filter(function (notification)
      {
        // Only show notifications without a read time:
        return !notification.readTime;
      });

      // Set the notifications on the scope for the menu to bind to:
      $scope.notifications = unreadNotifications;
    }

    // Add the function calls from the notifications data service:
    $scope.markRead = notificationsData.markRead;
    $scope.updateDiffs = notificationsData.updateDiffs;
    $scope.viewAllNotifications = function(stateName){
      // When on the notifications page, trying to redirect to the notifications page
      // again was taking the user to the main dashboard. The if below stops this from occurring.
      if($state.current.url!=stateName){
      navigationTree.selectTabAndDashboardFromStateUrl(stateName);
      }
    }


    // Define the function that determines what notification type we have:
    $scope.notificationType = function (notification)
    {
      switch (notification.threshold)
      {
        case 'ERROR':
          return 'fa fa-exclamation-circle fa-fw notification-error';
        case 'WARN':
          return 'fa fa-warning fa-fw notification-warn';
        default:
          return 'fa fa-info-circle fa-fw notification-info';
      }
    };

  }

});


