//Had to replace . with dashboard here otherwise the files were not found
define(['dashboards/dashboards.module'], function (module) {

  // Configure a controller:
  module.controller('dashboardDataSourcesController', controller);

  // Define the dashboard controller:
  controller.$inject = ['$scope', '$state', 'dashboardDefinition', 'dataSourceRegistry']; // dashboardDefinition comes from the resolve in the dynamic-dashboard-states factory. The dataSourceRegistry is from the data-source-registry module.
  function controller($scope, $state, dashboardDefinition, dataSourceRegistry) {

    // Save the current state name:
    $scope.stateName = $state.current.name;

    // Define the controller for missing widget types:
    $scope.missingDataSourceController = missingDataSourceController;
    function missingDataSourceController()
    {
      // Do nothing.
    };

    // Create the function to get a controller:
    $scope.getControllerConstructorFunction = getControllerConstructorFunction;
    function getControllerConstructorFunction(dataSource)
    {
      if (dataSourceRegistry.hasDataSource(dataSource.type))
      {
        // We have this data source type.
        // Get the controller:
        // https://docs.angularjs.org/api/ng/service/$compile
        // http://stackoverflow.com/questions/27866620/use-case-for-controller-service-in-angularjs
        // NOTE:  The ng-controller directive needs either the name or the constructor function of the controller. We can't use $controller because that doesn't give us the constructor function but rather the result of calling the constructor function.
        //        Therefore we use require to get the constructor function.
        var controllerConstructorFunction = dataSourceRegistry.getDataSourceControllerConstructor(dataSource.type);
        return controllerConstructorFunction;
      }
      else
      {
        // We did not find the data source type.
        // Return a dummy controller:
        return missingDataSourceController;
      }
    }

  }

});