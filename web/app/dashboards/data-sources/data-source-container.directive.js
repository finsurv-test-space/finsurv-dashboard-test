//Had to replace . with dashboard here otherwise the files were not found
define(['dashboards/dashboards.module', 'require'], function (module, require) {

  // Define the directive:
  module.directive('datasourceContainer', dataSourceContainer);
  dataSourceContainer.$inject = ['dataSourceRegistry'];
  function dataSourceContainer(dataSourceRegistry)
  {
    return {
      restrict: 'E',
      templateUrl: require.toUrl('./data-source-container.html'),

      // Note: We choose to use isolated scopes so that each data source is unaffected by the root scope.
      // The implication is that each data source must access it's parent scope explicitly if it wants something outside of the scope.
      // '$parent' will not have root scope data trickling down because there might be other isolated scopes in the chain up to the root.
      // You can use '$root' in the angular expressions to access the root scope.
      // https://github.com/angular/angular.js/wiki/Understanding-Scopes
      scope: {
        dataSource: '=datasourceconfig', // The '=' is used to set up 2 way binding between this isolate scope and the parent scope. https://docs.angularjs.org/api/ng/service/$compile
        showHeader:'=showheader',
        dashboardScope:'=dashboardscope', // The attribute should point at the scope where the dashboard is assigned. The attribute should normally be set to findDashboardScope().
        dataSourceController: '=datasourcecontroller' // We need to do this here because only the scope gets run before the link method gets called below.
      },


      /*
          NOTE: The following idea was trying to get the directives controller to be driven by an attribute in the directive.
                However, we couldn't get it to be driven off the {{dataSource.type}} so we had to scrap this idea.
                I am keeping the code because it is interesting, maybe for another feature.
                Instead, we put an explicit ng-controller within the template directive.
      // Define a controller that is provided by the widget:
      // https://docs.angularjs.org/api/ng/service/$compile
      // http://stackoverflow.com/questions/19444414/how-to-set-the-dynamic-controller-for-directives
      // http://stackoverflow.com/a/23647720/231860
      // This seems like an undocumented feature but it's there in the code.
      // From: Angular.js: function setupControllers($element, attrs, transcludeFn, controllerDirectives, isolateScope, scope) {
      //    var controller = directive.controller;
      //    if (controller == '@') {
      //      controller = attrs[directive.name];
      //    }
      controller : "@", // Magic value!
      name: "controllerName", // this is the normalised name of the attribute on the directive.
      */

      link: function (scope, element, attrs) {

        // Get the data source we are working on:
        var dataSource = scope.dataSource;

        // Get the type of data source:
        var dataSourceType = dataSource.type;
        scope.dataSourceType = dataSourceType;

        // Get the normalised name for the data source type (ie: trade-feed -> tradeFeed)
        // http://stackoverflow.com/questions/20100031/which-service-converts-element-and-attr-names-in-angularjs
        // http://stackoverflow.com/a/20100606/231860
        var normalizedDataSourceType = attrs.$normalize(dataSourceType);
        scope.normalizedDataSourceType = normalizedDataSourceType;

        // Check whether we know about this data source type:
        var isDataSourceTypeKnown = dataSourceRegistry.hasDataSource(dataSourceType);

        // Store the scope on the data source:
        dataSource.scope = scope;

        // We have successfully pre loaded the widget.
        // Set the UI template so that Angular can start processing the UI.
        scope.getContentUrl = function () {
          // Check whether we actually have a widget of this type:
          if (isDataSourceTypeKnown)
          {
            // We have a data source of this type.
            return dataSourceRegistry.getDataSourceHtmlUrl(dataSourceType);
          }
          else
          {
            // We don't have a data source of this type.
            return require.toUrl('./missing-data-source.html');
          }
        };

        //http://stackoverflow.com/questions/23848127/how-can-i-overcome-race-conditions-within-directives-without-a-timeout-function
        // We use the name postLoad because onLoad is used for some other angular mechanism and it causes clashes.
        scope.postLoad = function () {
        }

      }
    };
  }

});


