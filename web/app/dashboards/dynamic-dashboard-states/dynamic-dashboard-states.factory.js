//Had to replace . with dashboard here otherwise the files were not found
define(['dashboards/dashboards.module', 'require'], function (module, require)
{

  // This is a factory for injecting and going to new dashboard states.
  // The expanded navigation tree is used to generate new states for each dashboard.

  // This was inspired by:
  // http://stackoverflow.com/questions/25866387/angular-ui-router-programmatically-add-states

  // Save the state provider from the config phase:
  var stateProvider = null;
  module.config(config);
  config.$inject = ['$stateProvider'];
  function config($stateProvider)
  {
    // Save the state provider for use at runtime:
    stateProvider = $stateProvider;
  }

  // Define the factory:
  module.factory('dynamicDashboardStates', factory);
  factory.$inject = ['$state'];
  function factory($state)
  {

    return {
      /*
       This adds a new state for the given dashboard.
       If the state already exist then it skips over this item.
       returns: The name of the state.
       */
      addState: function (navItem)
      {
        // Get the dashboard path for this item:
        var dashboardPath = navItem.getPath('/', true);

        // Get the code path (with unexpanded names) where the dashboard definition is:
        var codePath = navItem.getPath('/', false);

        // Get the state name:
        var stateName = dashboardPath;

        // Get all the states that have been defined:
        // http://stackoverflow.com/questions/21324303/how-to-enumerate-registered-states-in-ui-router
        var allStates = $state.get();

        // Check whether we have already defined states for this item:
        // http://stackoverflow.com/questions/21590466/see-what-states-are-configured-in-ui-router
        if (allStates.filter(function (state){return state.name == stateName;}).length == 0)
        {
          // We don't have this state yet.

          // Define the new state using the state provider we captured at config time:
          stateProvider.state(stateName, {
            url: dashboardPath,
            parent:'dashboard',
            data: {
              // Save the navigation item context on the state:
              navItem: navItem,
              // Save the parameters from the navigation tree:
              parameters: navItem.parameters,
              requireLogin: true // Make sure we are logged in.
            },
            resolve: {
              // This downloads the dashboard code before we transition to the dashboard.
              dashboardDefinition: dashboardDefinition
            },
            views: {
              'widgets@dashboard': {
                templateUrl: require.toUrl('dashboards/widgets/widgets.html'),
                controller: 'dashboardWidgetsController'
              },
              // This is where the data sources for the dashboard will be placed for the dashboard:
              'data-sources@dashboard': {
                templateUrl: require.toUrl('dashboards/data-sources/data-sources.html'),
                controller: 'dashboardDataSourcesController'
              }
            }


          });

          //The current grid implementation requires that the widgets
          //are ordered first by row asc then by col asc;
          // function reOrderWidgets(dashboard){
          //   //Order first by row asc
          //   dashboard.widgets.sort(function(a,b){
          //     return a.layout.row-b.layout.row
          //   })
          //   //Then order by col asc
          //   dashboard.widgets.sort(function(a,b){
          //     if(a.layout.row == b.layout.row){
          //       return a.layout.col- b.layout.col;
          //     }
          //   })
          // }

          /*
            This is the function to run when resolving the code for the dashboard.
           */
          dashboardDefinition.$inject = ['$http', '$state'];
          function dashboardDefinition($http, $state)
          {
            /*
             NOTE TO FUTURE SELF:
             YES, we definitely want to download the code for the dashboard as a string because we need it for our UI to manipulate (when the user edits it).
             We will eval it ourselves each time because we don't want any in-memory caching.
             Don't use require because that would cache the instance and we would loose the string that we can use to modify the UI from the front end.
             Don't use Angular because it would also cache the instance.
             This is a valid use-case for using eval in this context.
             */
            // Get the URL for the code:
            var rawCodeURL = '/finsurv-dashboard/dashboards' + codePath + '/dashboard.js';
            // var rawCodeURL = '../../../../dashboards' + codePath + '/dashboard.js';

            var codeURL = require.toUrl(rawCodeURL);

            // Make sure to append a query string to bust the cache:
            // https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest/Using_XMLHttpRequest
            var codeURLWithDate = codeURL + ((/\?/).test(codeURL) ? "&" : "?") + (new Date()).getTime();

            // Download the code:
            return $http.get(codeURLWithDate)
              .then(function(response)
              {
                // Now we have the code downloaded.
                // Get the code:
                var code = response.data;

                // Wrap the code in so that we get an Immediately-invoked function expression (so we don't polute the global namespace):
                // https://en.wikipedia.org/wiki/Immediately-invoked_function_expression
                var wrappedCode = '(' + code + ')(parameters);';

                // Get the parameters for this context:
                var parameters = navItem.parameters || [];

                // Evaluate the code to get the dashboard definition:
                var dashboard = eval(wrappedCode);
                //Make sure that the widgets attached to the dashboard are in the right order, which is first row asc and then col asc.
                // if(dashboard.widgets && dashboard.widgets.length>1){
                //   reOrderWidgets(dashboard);
                // }
                // Return the structure:
                return {
                  dashboardPath: dashboardPath,
                  codePath: codePath,
                  codeURL: codeURL,
                  code: code,
                  wrappedCode: wrappedCode,
                  dashboard: dashboard
                };
              });
          }
        }

        // Return the state name:
        return stateName;
      },

      /*
       This navigates to the given dashboard for the navigation item from the navigation tree.
       */
      go: function (navItem)
      {
        // Get the dashboard path that was selected:
        // The '/' delimited path of the dashboard as defined by the navigation tree.
        // The path root of the navigation tree is '/'.
        // It needs to start with a leading '/'.
        // This is not the absolute URL.
        // The state will determine that for you.
        var dashboardPath="";
        if(typeof navItem == "object"){
         dashboardPath = navItem.getPath('/');
        }
        else{
          dashboardPath = navItem;
        }
        //Check if the state we want to go to exists
        if($state.get(dashboardPath)) {
          // Go to the state:
          $state.go(dashboardPath);
        }
        else{
          //If it does not stay at your current state.
          //Currently because of the way we are handling the dynamic states the user will be taken to the dashboard state
          //which will then direct the user to first item in the state.
          $state.go($state.current.name)
        }
      }
    }
  }

});
