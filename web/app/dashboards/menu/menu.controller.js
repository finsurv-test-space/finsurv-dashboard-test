//Had to replace . with dashboard here otherwise the files were not found
define(['dashboards/dashboards.module', 'require', 'dashboards/dashboards.constants'], function (module, require) {

  // Configure a controller:
  module.controller('dashboardMenuController', controller);

  // Define the dashboard controller:
  controller.$inject = ['$scope', 'navigationTree', 'dynamicDashboardStates'];
  function controller($scope, navigationTree, dynamicDashboardStates)
  {
    // We expect the navigationTree to propagate down from the parent scope as the navTree property and the selected menu as the menu property (from dashboard.controller.js)
    //We used to expect the navigation tree to propogate down from the parent scope however with changing to the use of a root shell, the navigation tree is now obtained
    //through the navigation tree service.
    navigationTree.subscribe(getTree);
    function getTree(tree,menu){
      $scope.navTree =tree;
      $scope.menu = menu;
    };
    // Define the function for selecting a dashboard:
    $scope.selectDashboard = navigationTree.selectDashboard;
    // Get the URL for the menu item:
    $scope.menuItemURL = require.toUrl('./menu-item.html');
  }

});


