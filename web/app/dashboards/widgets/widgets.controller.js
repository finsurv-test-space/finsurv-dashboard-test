//Had to replace . with dashboard here otherwise the files were not found
define(['dashboards/dashboards.module', 'angular'], function (module, angular) {

  // Configure a controller:
  module.controller('dashboardWidgetsController', controller);

  // Define the dashboard controller:
  controller.$inject = ['$scope', 'dashboardDefinition', 'widgetRegistry']; // dashboardDefinition comes from the resolve in the dynamic-dashboard-states factory. The widgetRegistry is from the widget-registry module.
  function controller($scope, dashboardDefinition, widgetRegistry) {

    //Reset these values here for loading of new dashboards.
    $scope.maximisedMode = false;
    $scope.maximisedWidgetName = '';

    //Sets the maximise widget mode.
    //If another widget tries to set maximise mode again return false with a logged error message.
    var setMaximisedMode = function (dashboard) {
      dashboard.widgets.forEach(function (widget) {
        if (widget.maximisedMode) {
          if ($scope.maximisedMode == false || $scope.maximisedWidgetName == widget.name) {
            $scope.maximisedWidgetName = widget.name;
            $scope.maximisedMode = widget.maximisedMode;
            return true;
          }
          else {
            console.log("You cannot maximise " + widget.name + " as " + $scope.maximisedWidgetName + " is already maximised");
            return false
          }
        }
      })
    };

    // Attach the dashboard on the dashboard scope so we can resolve common data:
    $scope.dashboardScope.attachDashboardDefinition(dashboardDefinition.dashboard);

    $scope.gridsterOptions = {
      draggable: {
        handle: '.drag-handle',
        enabled: false,
        stop: function (event, $element, layout) {
          storeGridsterLayoutInLocalStorage(layout);
        } // optional callback fired when item is finished dragging
      },
      resizable: {
        enabled: false,
        resize: function () {
          //Although in the documentation for gridster there is a gridster-item-resized event call
          //this is not functioning as expected and is only being called on window resize so for now
          //we need to explicitly broadcast our own event when gridster resize Stop callback occurs.
          //$scope.$broadcast("gridsterWidgetResizing");
        }
      },
      colWidth: 'auto',
      rowHeight: 50,
      margins: [10, 10],
      mobileBreakPoint: 800,
      columns: 30,
      minRows: 2,
      maxRows: 100
      // This defines the usable width of the gridster container. We take the total screen width and subtract the side menu with its padding
      // to get the usable width.
    }

    //Only allowing dragging and resizing if edit mode enabled
    $scope.$watch(function (scope) {
      return scope.inEditMode
    }, function () {
      $scope.gridsterOptions.draggable.enabled = $scope.inEditMode;
      $scope.gridsterOptions.resizable.enabled = $scope.inEditMode;
    })

    // Define the controller for missing widget types:
    $scope.missingWidgetController = missingWidgetController;
    function missingWidgetController() {
      // Do nothing.
    };

    // Create the function to get a controller:
    $scope.getControllerConstructorFunction = getControllerConstructorFunction;
    function getControllerConstructorFunction(widget) {
      if (widgetRegistry.hasWidget(widget.type)) {
        // We have this widget type.
        // Get the controller:
        // https://docs.angularjs.org/api/ng/service/$compile
        // http://stackoverflow.com/questions/27866620/use-case-for-controller-service-in-angularjs
        // NOTE:  The ng-controller directive needs either the name or the constructor function of the controller. We can't use $controller because that doesn't give us the constructor function but rather the result of calling the constructor function.
        //        Therefore we use require to get the constructor function.
        var controllerConstructorFunction = widgetRegistry.getWidgetControllerConstructor(widget.type);
        return controllerConstructorFunction;
      }
      else {
        // We did not find the widget type.
        // Return a dummy controller:
        return missingWidgetController;
      }
    }
  }

});
