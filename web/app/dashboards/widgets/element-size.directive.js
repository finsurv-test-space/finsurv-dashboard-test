//Had to replace . with dashboard here otherwise the files were not found
define(['dashboards/dashboards.module', 'require'], function (module, require) {
  //Reference for solution
  //http://stackoverflow.com/questions/19048985/angularjs-better-way-to-watch-for-height-change
  /**
   * This attribute directive watches the change in height and width of a specific directive and then updates the size of the widget container.
   * This directive is attached to the div in the widget-container where the widget will be displayed.
   */
  module.directive('elSize', elSize);
  //elSize.$inject = ['$parse'];
  function elSize() {
    return {
      restrict: 'A',
      scope: {
        widget: "=widget"
      },
      link: function (scope, elem, attrs) {
        //This is our own event called from broadcasted from the widget.cotnroller class.
        scope.$on("gridsterWidgetResizing",function(){
            scope.oldSize.parentWidth = elem.parent().width();
            scope.oldSize.parentHeight = elem.parent().height();

            var heightOfWidgetHeader = elem.parent().siblings().height();

            //The widgets new height is the total height of the container minus the height of the header.
            scope.oldSize.height =scope.oldSize.parentHeight - heightOfWidgetHeader;
            //Just to note the width of the inner element and the container is the same thing.
            scope.oldSize.width = elem.width();

            // Because the girdster library uses pixels per unit setting, in our case fifty, if the widget contents
            // new height/width goes over this amount the container will grid by another unit.
            // Therefor the buffer is to ensure the widget content remains within the limits of current widget container.r
            var buffer = 5;
            scope.widget.config.width = scope.oldSize.width-buffer;
            scope.widget.config.height =   scope.oldSize.height-buffer;

            scope.$emit("widgetResized",scope.widget);
        })
        scope.$watch(function () {
          //Not watching for parent width as element and parent width are the same in this case.
          return {width: elem.width(), height: elem.height(),parentHeight:elem.parent().height(),parentWidth:elem.parent().width()};

        }, function (size) {

          //Using the the width and height which are contained within the size object
          //we can calculate the width and height of the gridster container.

          //We need to ceiling these values are the layout for the gridster object
          //only take in whole numbers.
          if (!scope.oldSize) {
            scope.oldSize = size;
          }

          if(scope.oldSize.parentWidth!==size.parentWidth||scope.oldSize.height!=size.parentHeight){
            scope.oldSize.parentWidth = size.parentWidth;
            scope.oldSize.parentHeight = size.parentHeight;
          }

          //Only set the new heights if the difference is greater a gridster row height unit
          if (Math.abs(scope.oldSize.height - size.height)>scope.widget.gridsterRowHeight) {
            scope.oldSize.height = size.height;
            var layoutY = Math.ceil(size.height / scope.widget.gridsterRowHeight);
            //Additional one here is used so that there is sufficient space at the bottom of the widget container to display the widget.
            scope.widget.layout.sizeY = layoutY + 1;
          }

          //Only set the new width if the difference is greater a gridster col width unit
          if (Math.abs(scope.oldSize.width - size.width)>scope.widget.gridsterColWidth) {
            scope.oldSize.width = size.width;
            var layoutX = Math.ceil(size.width / scope.widget.gridsterColWidth);
            scope.widget.layout.sizeX = layoutX;
          }
        }, true);

      }
    }
  }
});