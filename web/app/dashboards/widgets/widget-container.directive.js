//Had to replace . with dashboard here otherwise the files were not found
define(['dashboards/dashboards.module', 'require'], function (module, require) {

  // Define the directive:
  module.directive('widgetContainer', widgetContainer);

  widgetContainer.$inject = ['widgetRegistry', '$q','$window'];
  function widgetContainer(widgetRegistry, $q, $window)
  {
    return {
      restrict: 'E',
      templateUrl: require.toUrl('./widget-container.html'),

      // Note: We choose to use isolated scopes so that each widget is unaffected by the root scope.
      // The implication is that each widget must access it's parent scope explicitly if it wants something outside of the scope.
      // '$parent' will not have root scope data trickling down because there might be other isolated scopes in the chain up to the root.
      // You can use '$root' in the angular expressions to access the root scope.
      // https://github.com/angular/angular.js/wiki/Understanding-Scopes
      scope: {
        widget: '=widgetconfig', // The '=' is used to set up 2 way binding between this isolate scope and the parent scope. https://docs.angularjs.org/api/ng/service/$compile
        showHeader:'=showheader',//This is determins whether the header is displayed by default
        showHeaderInEditMode:'=showHeaderInEditMode',//This sets the header and the buttons to display in edit mode.
        dashboardScope:'=dashboardscope', // The attribute should point at the scope where the dashboard is assigned. The attribute should normally be set to findDashboardScope().
        widgetController: '=widgetcontroller' // We need to do this here because only the scope gets run before the link method gets called below.
      },


      /*
          NOTE: The following idea was trying to get the directives controller to be driven by an attribute in the directive.
                However, we couldn't get it to be driven off the {{widget.type}} so we had to scrap this idea.
                I am keeping the code because it is interesting, maybe for another feature.
                Instead, we put an explicit ng-controller within the template directive.
      // Define a controller that is provided by the widget:
      // https://docs.angularjs.org/api/ng/service/$compile
      // http://stackoverflow.com/questions/19444414/how-to-set-the-dynamic-controller-for-directives
      // http://stackoverflow.com/a/23647720/231860
      // This seems like an undocumented feature but it's there in the code.
      // From: Angular.js: function setupControllers($element, attrs, transcludeFn, controllerDirectives, isolateScope, scope) {
      //    var controller = directive.controller;
      //    if (controller == '@') {
      //      controller = attrs[directive.name];
      //    }
      controller : "@", // Magic value!
      name: "controllerName", // this is the normalised name of the attribute on the directive.
      */

      link: function (scope, element, attrs) {

        // Get the widget we are working on:
        var widget = scope.widget;

        // Get the type of widget:
        var widgetType = widget.type;
        scope.widgetType = widgetType;

        // Save the element for the widget so it knows where it is being drawn (useful for widgets that extend the dom):
        widget.element = element;

        // Get the normalised name for the widget type (ie: pie-chart -> pieChart)
        // http://stackoverflow.com/questions/20100031/which-service-converts-element-and-attr-names-in-angularjs
        // http://stackoverflow.com/a/20100606/231860
        var normalizedWidgetType = attrs.$normalize(widgetType);
        scope.normalizedWidgetType = normalizedWidgetType;

        // Check whether we know about this widget type:
        var isWidgetTypeKnown = widgetRegistry.hasWidget(widgetType);

        // Store the scope on the widget:
        widget.scope = scope;

        // We have successfully pre loaded the widget.
        // Set the UI template so that Angular can start processing the UI.
        scope.getContentUrl = function () {
          // Check whether we actually have a widget of this type:
          if (isWidgetTypeKnown)
          {
            // We have a widget of this type.
            return widgetRegistry.getWidgetHtmlUrl(widgetType);
          }
          else
          {
            // We don't have a widget of this type.
            return require.toUrl('./missing-widget.html');
          }
        };

        //http://stackoverflow.com/questions/23848127/how-can-i-overcome-race-conditions-within-directives-without-a-timeout-function
        // We use the name postLoad because onLoad is used for some other angular mechanism and it causes clashes.
        scope.postLoad = function ()
        {
          // Resolve the deferred promise for the widget:
          widget.postLoadDeferred.resolve(widget);
        }

      }
    };
  }

});


