/**
 * Created by daniel on 15/09/16.
 */
define(['dashboards/dashboards.module', 'angular', 'dashboards/dashboards.constants'], function (module, angular) {

  // Configure a controller:
  module.controller('dashboardToolboxController', controller);

  // Define the dashboard controller:
  controller.$inject = ['$scope'];
  function controller($scope)
  {
    // We expect the navigationTree to propagate down from the parent scope as the navTree property (from dashboard.controller.js)
  }

});


