// Load all the related files for the module:
define([
  './dashboards.module',
  './dashboards.states',
  './dynamic-dashboard-states/dynamic-dashboard-states.factory',
  './widgets/widget-container.directive',
  './widgets/element-size.directive',
  './data-sources/data-source-container.directive',
  //'./notifications/notification-badge.directive'
], function (module) {
  return module;
});