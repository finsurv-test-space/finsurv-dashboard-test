//Had to replace . with dashboard here otherwise the files were not found
define(['dashboards/dashboards.module'], function (module) {

  // Configure a controller:
  module.controller('dashboardLayoutController', controller);

  // Define the dashboard controller:
  controller.$inject = ['$scope', '$state', '$q', '$timeout']; // We removed user to break the dependency. user is from the User module.
  function controller($scope, $state, $q, $timeout) {

    // Define the common storage location for data sources.
    // NOTE: we keep this off the scope for performance reasons.
    // https://synthesis-software.atlassian.net/browse/MKT-402
    var data = {};
    // Define the callbacks for the data:
    var dataCallbacks = {};

    // Save the current state name:
    $scope.stateName = $state.current.name;

    // Define the list of callbacks to call during the post load phase (usually because widgets need their UI loaded before they update the UI):
    var postLoadCallbacks = [];

    // Define a variable that keeps track of whether we have finished post loading all the widgets. This is useful for keeping track of
    var postLoadFinished = false;

    $scope.inEditMode = false;
    $scope.toolBoxOpen = false;
    $scope.dataSourcesOpen = false;


    //This functions turns on or off the ability to edit the grid
    $scope.gridEditMode = function () {
      //$scope.setDefaultWidgetLayout = false;
      $scope.inEditMode = !$scope.inEditMode;
      //Close toolbox if we quit edit mode
      if ($scope.toolBoxOpen) {
        $scope.toolBoxOpen = !$scope.toolBoxOpen;
      }
      if ($scope.dataSourcesOpen) {
        $scope.dataSourcesOpen = !$scope.dataSourcesOpen;
      }
    }
    
    /*
    * This toggles the toolbox panel.
    */
    $scope.openToolBox = function () {
      $scope.toolBoxOpen = !$scope.toolBoxOpen;
    }
    
    /*
    * This toggles the data sources panel.
    */
    $scope.openDataSources = function () {
      $scope.dataSourcesOpen = !$scope.dataSourcesOpen;
    }


    //In order to have as-sortable lists where the items are draggable between the lists
    //all lists must reference the same options therefor they are being set here.
    $scope.toolboxSortableOptions = {
      accept: function (sourceItemHandleScope, destSortableScope) {
        return sourceItemHandleScope.itemScope.sortableScope.$id != destSortableScope.$id;
      },
      dragEnd: function (eventObj) {
        // console.log(eventObj);
        if (eventObj.source.sortableScope.$id != eventObj.dest.sortableScope.$id) {
          console.log("to be placed");
          //For the below if the moved object contains a widget object
          //then add this widget object to the currently displayed dashboard
          if (eventObj.source.itemScope.modelValue.widget) {
            var newWidget = {};
            //Have to use this copy metho here otherwise we get a ng-repeat duplicate error
            angular.copy(eventObj.source.itemScope.modelValue.widget, newWidget);
            $rootScope.dashboard.widgets.push(newWidget);
          }
        }
        else {
          console.log("not to be placed");
        }
      },
      itemMoved: function (eventObj) {
        var moveSuccess, moveFailure;
        /**
         * remove the item from destination Column.
         * insert the item again in original Column.
         * This is to give the effect that the user is dropping the widget on the page
         * and that it appears again in the toolbox
         */
          //https://github.com/a5hik/ng-sortable
        eventObj.dest.sortableScope.removeItem(eventObj.dest.index);
        //On the authors page the second argument in the insertItem function below ends with .task however
        //no .task exist and therefore .modelValue is used.
        eventObj.source.itemScope.sortableScope.insertItem(eventObj.source.index, eventObj.source.itemScope.modelValue)
      }
    }

    $scope.setDefaultLayout = function(){
      $scope.$broadcast('setDefaultWidgetLayout');
    }

    // Save the dashboard scope so that widget and data source controllers can access it easily:
    $scope.dashboardScope = $scope; // We are at the dashboard scope.

    /**
     * This initialises the dashboard.
     * It must be called whenever a dynamic dashboard state is loaded.
     * This is important because switching child dashboard states does not create a new parent dashboard layout controller.
     * In order to make sure that datasources and post-loads are called correctly, we need all dynamic dashboard states to call this.
     */
    function initDashboard()
    {
      // Clear any dashboard data:
      data = {};

      // Clear the callbacks for the data:
      dataCallbacks = {};

      // Save the current state name:
      $scope.stateName = $state.current.name;

      // Clear the list of callbacks to call during the post load phase (usually because widgets need their UI loaded before they update the UI):
      postLoadCallbacks = [];

      // Clear the variable that keeps track of whether we have finished post loading all the widgets. This is useful for keeping track of
      postLoadFinished = false;
    }

    // Define a function to attach the dashboard definition to this scope.
    // This must be called by the dynamic dashboard state once it has downloaded the actual dashboard definition.
    // It is defined at this level so that a common implementation can be used by the widget and data source controller.
    $scope.attachDashboardDefinition = attachDashboardDefinition;
    function attachDashboardDefinition(dashboardDefinition) {
      // Save the dashboard definition on the scope:
      $scope.dashboard = dashboardDefinition;

      // Create the array of promises for each widgets post load phase:
      var allPostLoadPromises = [];

      // Initialise the dashboard each time we transition to a dynamic dashboard state.
      // NOTE: This is required because switching between dashboard sub states does not create a new dashboard layout controller.
      //       We only get a new dashboard layout controller when we move out of the dashboard state completely (like login for example).
      initDashboard();

      // Create deferred promises for the post load of the widget:
      dashboardDefinition.widgets.forEach(function (widget) {
        // Create a deferred promise:
        var deferred = $q.defer();

        // Save the deferred promise on the widget:
        widget.postLoadDeferred = deferred;
        widget.postLoadPromise = deferred.promise;

        // Add the promise to the list of all promises:
        allPostLoadPromises.push(widget.postLoadPromise);
      });

      // Create the promise for when all widget post loads have run:
      var allWidgetsLoadedPromise = $q.all(allPostLoadPromises);
      $scope.allWidgetsLoadedPromise = allWidgetsLoadedPromise;

      // Define what happens once all widgets have loaded:
      allWidgetsLoadedPromise.then(function (widgets) {
        // Now all widgets have loaded.
        // Call any post load update callbacks that need to be fired:
        postLoadCallbacks.forEach(function (callback) {
          callback()
        });

        // Flag that we have finished post loading all the widgets:
        postLoadFinished = true;

        // Clear the post load array of callbacks so we don't keep references longer than we have to:
        // http://stackoverflow.com/questions/1232040/how-to-empty-an-array-in-javascript
        postLoadCallbacks.length = 0;
      });

      // Now that the dashboard definition has loaded, notify any callbacks that already have data:
      // http://stackoverflow.com/questions/1200562/difference-in-json-objects-using-javascript-jquery/1200713#1200713
      for (var dataName in data)
      {
        if (data.hasOwnProperty(dataName))
        {
          // Publish the data:
          publish(dataName);
        }
      }
    }

    // Define functions for getting data (from data sources) (this is defined in the dashboard layout controller because we want the common implementation available to the widgets controller and the data sources controller):
    $scope.getData = getData;
    function getData(dataName) {
      // Get the given named data:
      return data[dataName];
    }

    // Define functions for watching data (from data sources) (this is defined in the dashboard layout controller because we want the common implementation available to the widgets controller and the data sources controller):
    $scope.watchData = watchData;

    /**
     * This watches for changes to the given data sources.
     * You don't need to unsubscribe from the data because the they are unwatched whenever the controller is destroyed.
     * @param dataName
     * @param callback
     * @param waitForPostLoad
     */
    function watchData(dataName, callback, waitForPostLoad) {
      // Make sure we have a data source to watch:
      if (!dataName) return;

      // Check whether we need to wait for the post load event before firing the call back:
      if (waitForPostLoad === undefined) {
        // Default to false:
        waitForPostLoad = false;
      }

      // Register the callback for the data source:
      if (!dataCallbacks[dataName])
      {
        // We have not created any callbacks here before.
        // Initialise the array of callbacks:
        dataCallbacks[dataName] = [];
      }
      dataCallbacks[dataName].push({ callback: callback, waitForPostLoad: waitForPostLoad });

      // Check if we already have data there so we can fire the callback immediately (also make sure that the dashboard definition exists):
      if ($scope.dashboard && data[dataName] !== undefined) {
        // We have some data already.

        // Check whether we must queue up the callback or call it immediately:
        if (!waitForPostLoad || postLoadFinished) {
          // We should fire the callback immediately.
          // Notify the callback:
          callback(data[dataName]);
        }
        else {
          // We must queue up the call back until the post load phase:
          postLoadCallbacks.push(function () {
            callback(data[dataName]);
          });
        }
      }
    }

    // Define functions for setting data (usually from data sources):
    $scope.setData = setData;
    function setData(dataName, value) {
      // Set the given named data:
      data[dataName] = value;

      // Make sure we have already loaded the dashboard definition:
      //if ($scope.dashboard)
      {
        // Publish the change:
        publish(dataName);
      }
    }

    /**
     * This publishes the callbacks for the given data source.
     * All callbacks for that data source are notified.
     * @param dataName The name of the data source to publish.
     */
    function publish(dataName)
    {
      // Get the current data value:
      var value = data[dataName];

      // Get all the callbacks for the data:
      var callbacks = dataCallbacks[dataName];
      if (callbacks)
      {
        // There are callbacks.

        // Make sure to copy the array in case one of the callbacks adds or removes more callbacks:
        var oldCallbacks = callbacks.slice(); // shallow copy of the array: http://stackoverflow.com/questions/7486085/copying-array-by-value-in-javascript
        oldCallbacks.forEach(function(info)
        {
          // Check whether we must queue up the callback or call it immediately:
          if (!info.waitForPostLoad || postLoadFinished)
          {
            // We should fire the callback immediately.
            // Notify the callback:
            info.callback(value);
          }
          else
          {
            // We must queue up the call back until the post load phase:
            postLoadCallbacks.push(function ()
            {
              info.callback(value);
            });
          }
        });
      }
    }

    // Define the logic for checking permissions:
    //$scope.checkPermissions = user.checkUserPermissions;
  }

});
