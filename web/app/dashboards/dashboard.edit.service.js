/**
 * Created by daniel on 15/09/20.
 */
define(['dashboards/dashbords.module'], function (module) {
  module.service("EditService", EditService);
  //EditService.$inject=[];
  function EditService() {
    var service = {}
    var editModeState = false;
    service.setEditMode = setEditMode;
    service.getEditMode = getEditMode;

    function setEditMode(changedEditMode) {
      editModeState = changedEditMode;
    }

    function getEditMode() {
      return editModeState;
    }

    return service;
  }
})