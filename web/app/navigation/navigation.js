// Load all the related files for the module:
define([
  './navigation.module',
  './navigation.constants',
  './navigation-tree.service'
], function (module) {
  return module;
});