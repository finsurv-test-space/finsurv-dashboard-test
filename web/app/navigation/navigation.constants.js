define(['./navigation.module', 'navigation-tree'], function (module, originalNavigationTree) { // navigation-tree is at the root of the webapp

  // Define any constants:

  // Save the original navigation tree structure within angular:
  module.constant('originalNavigationTree', originalNavigationTree);

});