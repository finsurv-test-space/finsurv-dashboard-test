'use strict';

define(['angular', 'navigation-provider-list'], function (angular, navigationProviderList) {

  // Define what other module this one depends on:
  // We want this module to depend on all the navigation provider implementations:
  var moduleDependencies = navigationProviderList.map(function(np){return 'navigation-providers.' + np.name;});

  // Add additional dependencies:
  moduleDependencies.push('cascadingParameters')

  // Define the module:
  var module = angular.module('navigation', moduleDependencies);

  return module;
});


