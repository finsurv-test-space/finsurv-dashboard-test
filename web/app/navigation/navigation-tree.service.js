define(['./navigation.module', 'angular'], function (module, angular) {

  /*
   This data structure gives us a consolidated view of the navigation tree.
   This is useful for cases in the UI where we want to display something while the navigation providers are still loading.
   */

  module.factory('navigationTree', factory);

  // Define the navigation tree service:
  factory.$inject = ['originalNavigationTree', 'navigationProviders', '$injector', '$q', 'CascadingParameters', 'dynamicDashboardStates']; // originalNavigationTree is from constants. CascadingParameters is from the cascading-parameters module. navigationProviders is from navigation-provider-loader
  function factory(originalNavigationTree, navigationProviders, $injector, $q, CascadingParameters, dynamicDashboardStates) {
    // The originalNavigationTree is the un-expanded definition of the navigation structure.
    // The promise will return the expanded tree structure once all navigation providers have expanded the tree.

    // Keep track of all the navigation providers that we instantiate:
    var actualNavigationProviders = [];

    // Clone the initial structure of the navigation tree:
    var initialTree = angular.copy(originalNavigationTree);

    // Define the root node of the tree:
    var rootNode = {
      isRoot: true,
      parameters: new CascadingParameters(null),
      children: initialTree
    };

    // Keep track of invalidation's that are pending:
    var isInvalidatePending = false;

    // Recursively remove nodes that have providers so that we have an initial tree:
    removeDynamicProviderNodes(initialTree);

    // Get the navigation tree promise:
    var navigationTreePromise = getNavigationTreePromise();

    // Wait for the full navigation tree to resolve:
    navigationTreePromise = navigationTreePromise.then(handleNavigationTreePromiseResolved);

    // Connect the nodes to the root node:
    connectToRootNode(initialTree);

    // Adorn the nodes recursively with additional properties:
    initialTree.forEach(adornTree);

    var selectedTabIndex;
    var menu = {};
    //These variables are used for keeping track of which was active dashboard
    var currentSelectedTab = undefined;
    var currentSelectedDashBoard = {};
    // Create the return structure:
    var result = {
      initial: initialTree,
      promise: navigationTreePromise,
      combinedTree: initialTree,
      rootNode: rootNode,
      loaded: false,
      selectTab: selectTab,
      selectedTabIndex: selectedTabIndex,
      selectDefaultTab: selectDefaultTab,
      selectDashboard: selectDashboard,
      subscribe: subscribe,
      unsubscribe: unsubscribe,
      addBadgesToNavTree: addBadgesToNavTree,
      removeBadgesFromNavTree: removeBadgesFromNavTree,
      addBadgeOnly: addBadgeOnly,
      removeBadgeOnly: removeBadgeOnly,
      selectTabAndDashboardFromStateUrl: selectTabAndDashboardFromStateUrl,
      actualNavigationProviders: actualNavigationProviders
    }

    // Define the callbacks:
    var callbacks = [];

    return result;


    /**
     * This is the logic that gets run once the navigation tree has resolved.
     */
    function handleNavigationTreePromiseResolved(newRootNode) {
      // Set the new root node:
      result.rootNode = newRootNode;

      // Update the combined tree on the return structure to be the combined tree:
      result.combinedTree = newRootNode.children;

      // Number each tab with its index:
      result.combinedTree.forEach(function (navItem, i) {
        navItem.tabIndex = i;
      });

      // Perform parameter replacement for each node in the navigation tree:
      result.combinedTree.forEach(replaceParameters);

      // Define dynamic states for each dashboard in the navigation tree:
      // http://stackoverflow.com/questions/25866387/angular-ui-router-programmatically-add-states
      result.combinedTree.forEach(defineStates);

      // Flag that the full tree is loaded:
      result.loaded = true;

      // Publish changes:
      publish();
    }


    /**
     * This replaces parameters in each node recursively.
     * @param node The node to process.
     */
    function replaceParameters(node) {
      // Check whether the node has parameters:
      if (node.parameters) {
        // The node has parameters.

        // Perform parameter replacement in the node:
        var displayName = node.displayName;

        // Check whether there are any parameters:
        if ((displayName.indexOf('{{') != -1) && (displayName.indexOf('}}') != -1)) {
          // The display name has parameters that need to be replaced.

          // Go through each parameter and replace it:
          for (var property in node.parameters.all) {
            if (node.parameters.all.hasOwnProperty(property)) {
              // Get the parameter value:
              var parameterValue = node.parameters.get(property);

              // Perform replacements in the display name:
              displayName = displayName.replace('{{' + property + '}}', parameterValue);
            }
          }
          // Now the display name has had all parameters replaced.

          // Save the updated display name:
          node.displayName = displayName;
        }
      }

      // Check whether the node has children:
      if (node.children) {
        // The node has children.
        // Recursively replace parameters in each child:
        node.children.forEach(replaceParameters);
      }
    }


    /*
     Multi-Pass Navigation Tree Algorithm
     https://synthesis-software.atlassian.net/browse/MKT-388
     This section describes how the navigation tree gets expanded.
     This was required because nested navigation providers need to be dealt with correctly.
     A multi pass approach handles the intricacies of this algorithm,
     albeit at the expense of time where one navigation provider gets to work on the tree at once.
     */


    /**
     * This resolves the navigation tree in multiple passes.
     * Each navigation tree provider gets to run on the tree independently.
     * https://synthesis-software.atlassian.net/browse/MKT-388
     */
    function resolveNavigationTreeRecursively(originalNavigationTree) {
      return $q(function (resolve, reject) {
        // The originalNavigationTree is the un-expanded definition of the navigation structure.
        // We need to process each node and expand it based on the nav tree provider defined on it:

        // Clone the initial structure of the navigation tree:
        var navigationTree = angular.copy(originalNavigationTree);

        // Define a temporary root node:
        var temporaryRootNode = {
          isRoot: true,
          parameters: new CascadingParameters(null),
          children: []
        };

        // Connect each item to the root node:
        navigationTree.forEach(function (node) {
          temporaryRootNode.children.push(node)
          node.parent = temporaryRootNode;
        });

        // Adorn the nodes recursively with additional properties:
        navigationTree.forEach(adornTree);

        // Kick off the recursive chain of passes for the navigation tree resolution:
        resolveNextPass(temporaryRootNode).then(function (finalRootNode) {
          // Now we have finished all passes of the navigation tree resolution.
          //console.log(finalRootNode);
          resolve(finalRootNode);
        }).
            catch(function (err) {
              console.error(err);
              reject(err);
            });
      });
    }


    /**
     * This finds the first navigation provider that needs to be run on the navigation tree.
     * @param childNodes The collection of child nodes to search.
     */
    function findNextNodeWithUnresolvedNavigationProvider(childNodes) {
      // Go through each node in the navigation tree:
      for (var i = 0; i < childNodes.length; i++) {
        // Get the node to iterate:
        var node = childNodes[i];

        // Check whether there is a navigation provider configuration and it hasn't been resolved yet:
        if (node.navTreeProvider && node.provider === undefined) {
          // There is an unresolved navigation tree provider definition on this node.
          return node;
        }

        // Walk each child recursively:
        if (node.children && node.children.length > 0) {
          // This node has children.
          // Get the result of the child:
          var childResult = findNextNodeWithUnresolvedNavigationProvider(node.children);

          // Check if there was a child node that was unresolved so we can return it:
          if (childResult) return childResult;
        }
      }

      // If we get here then none of the nodes had an unresolved navigation provider.
      return undefined;
    }


    /**
     * Gets the provider factory for the given node.
     * This function does not attach the provider to the node. You should do this outside of the function.
     * @param node The node to create the provider instance for.
     */
    function getProviderFactory(node) {
      // Find the provider for this node:
      var providerEntry = navigationProviders.filter(function (p) {
        return p.name == node.navTreeProvider.type
      })[0];

      // Get the angular service from the injector (resolving any dependencies that the navigation provider declared):
      var provider = $injector.get(providerEntry.angularModuleName);

      return provider;
    }


    /**
     * This resolves the next pass of the navigation tree.
     * It automatically chains subsequent passes until there are no more unresolved navigation providers.
     * @param rootNode The root node of the navigation tree.
     * @return {*}
     */
    function resolveNextPass(rootNode) {
      return $q(function (resolve, reject) {
        // Get the next navigation tree provider that we want to run:
        var nextNodeWithUnresolvedNavigationProvider = findNextNodeWithUnresolvedNavigationProvider(rootNode.children);

        // Check whether we are done or whether there is still something to resolve:
        if (nextNodeWithUnresolvedNavigationProvider) {
          // There is still a node that needs to have a provider resolved.
          // Create the provider instance for this node:
          var providerFactory = getProviderFactory(nextNodeWithUnresolvedNavigationProvider);

          // Check if the provider factory has the require method to create a new provider instance for this node:
          if (providerFactory.createProvider) {
            // Create an instance of the specific provider for this node (each node will have its own provider instance):
            var provider = new providerFactory.createProvider(rootNode, nextNodeWithUnresolvedNavigationProvider, nextNodeWithUnresolvedNavigationProvider.navTreeProvider.settings);

            // Save context on the provider:
            provider.rootNode = rootNode;
            provider.node = nextNodeWithUnresolvedNavigationProvider;
            provider.settings = nextNodeWithUnresolvedNavigationProvider.navTreeProvider.settings;

            // Save the factory on the provider (we need it later if we clone this node, so we can create a new provider instance for the clones):
            provider.factory = providerFactory;

            // Save the provider on the node:
            nextNodeWithUnresolvedNavigationProvider.provider = provider;

            // Check whether the provider knows how to return a promise for the changes:
            if (provider.promiseChanges) {
              // Define the tree helper that can be used for working with the navigation tree:
              var navigationTreeHelper = {
                cloneNode: cloneNode,
                cloneNodeTree: cloneNodeTree,
                addNodeToParent: addNodeToParent,
                disconnectNode: disconnectNode,
                setParameters: setParameters
              };

              // Define the logic to invalidate the navigation tree:

              /**
               * This invalidates the navigation tree and rebuilds it.
               * Providers can call this whenever their structure changes and the navigation tree needs to be rebuilt.
               * The entire navigation tree is invalidated because there might be complex interactions between navigation providers that have changed.
               */
              function invalidate() {
                // Check whether there is an invalidation pending:
                if (!isInvalidatePending) {
                  // Flag that an invalidation is pending:
                  isInvalidatePending = true;

                  // Get the navigation tree promise:
                  var navigationTreePromise = getNavigationTreePromise();

                  // Wait for the full navigation tree to resolve:
                  navigationTreePromise.then(handleNavigationTreePromiseResolved)
                      .then(function () {
                        // Flag that the invalidation is finished:
                        isInvalidatePending = false;
                      });
                }
              }

              // Get the promise for data on the provider:
              var promise = $q.when(provider.promiseChanges(nextNodeWithUnresolvedNavigationProvider, nextNodeWithUnresolvedNavigationProvider.navTreeProvider.settings, rootNode, navigationTreeHelper, invalidate));

              // Save the promise on the provider:
              provider.promise = promise;

              // Define what happens when the promise for the changes to the node resolve:
              promise.then(function (nextNavigationTree) {
                // The provider made the changes to the node.

                // Cascade all the parameters down the tree:
                nextNavigationTree.parameters.cascade();

                // Kick off another pass:
                var nextPassPromise = resolveNextPass(nextNavigationTree);

                // Wait for the next pass to finish:
                nextPassPromise.then(function (nextRootNode) {
                  // Now the next pass has resolved.

                  // Cascade all the parameters down the tree:
                  nextNavigationTree.parameters.cascade();

                  // Resolve this pass:
                  resolve(nextRootNode);
                });
              }).catch(function (err) {
                reject(err);
              });
            }
            else {
              // The provider for this node did not know how to promise changes.
              reject("A navigation provider was not able to return a promise for its changes.")
            }
          }
          else {
            // The provider factory did not have the require function to create a new provider instance for this node.

            // Flag that the provider does not exist but was handled:
            nextNodeWithUnresolvedNavigationProvider.provider = null;
            reject("A navigation provider factory was not able to create a new provider for a navigation tree node.")
          }
        }
        else {
          // There are no more nodes that have unresolved providers.
          // Flag that we are done:
          resolve(rootNode);
        }
      });
    }

    /**
     * This clones the given node.
     * It will clone the navigation provider if one exists on this node.
     * It is NOT added to the parent nodes collection if there is a parent. The resulting clone does not have a parent property.
     * It does NOT clone child nodes. It just leaves off that property.
     * It shares the reference to the navTreeProvider data of the original node.
     * @param node The node to clone.
     */
    function cloneNode(node) {
      // Create the new node:
      var newNode = {};

      // Copy all the data across:
      // Angular adds a hash to objects so that it can keep track of them.
      // The angular copy method handles incrementing of the hash therefor using this function to allow angular to handle their own hashing.
      // NOTE: We use extend so that we can control what parts of the tree are walked into.
      angular.extend(newNode, node);

      // Create a clone of parameters (but keep them disconnected for now):
      newNode.parameters = new CascadingParameters(null);

      // Copy local parameters over:
      newNode.parameters.setMany(node.parameters.locals);

      // Clone the provider if there was one on the node:
      if (node.provider) {
        // This node has an existing provider instance.
        // Get the original provider:
        var originalProvider = node.provider;

        // We need to clone it.

        // Get the factory that was used to create this provider:
        var providerFactory = originalProvider.factory;

        // Create another instance of the provider from the factory:
        var newProvider = new providerFactory.createProvider(originalProvider.rootNode, node, node.navTreeProvider.settings);

        // Save context on the provider:
        newProvider.rootNode = originalProvider.rootNode;
        newProvider.node = node;
        newProvider.settings = node.navTreeProvider.settings;

        // Save the factory on the provider (we need it later if we clone this node, so we can create a new provider instance for the clones):
        newProvider.factory = providerFactory;

        // Save a reference to the original provider:
        newProvider.originalProvider = originalProvider;

        // Update the node with the given provider:
        newNode.provider = newProvider;
      }

      // Delete the reference to the parent because it has not been added to the parent:
      if (newNode.parent) delete newNode.parent;

      // Delete the children collection because it would be a reference to the original:
      if (newNode.children) delete newNode.children;

      return newNode;
    }

    /**
     * This clones the given node and its entire sub tree.
     * It is NOT added to the parent nodes collection if there is a parent. The resulting clone does not have a parent property.
     * It will clone all child nodes recursively.
     * It automatically updates navigation tree provider cloning if there are any nodes that already have an instance of the provider.
     * @param node The node to clone.
     */
    function cloneNodeTree(node) {
      // Clone the node:
      var newNode = cloneNode(node);
      // NOTE: the children of the node will not be cloned.
      // The parent property will also not be cloned.

      // Clone all the children:
      if (node.children) {
        // The node has children.

        // Create a new collection of children for the cloned node:
        newNode.children = [];

        // Go through each child and clone it:
        node.children.forEach(function (childNode) {
          // Clone the node:
          var newChildNode = cloneNodeTree(childNode);

          // Add the new child to the new node:
          newNode.children.push(newChildNode);

          // Set the parent on the new child node:
          newChildNode.parent = newNode;

          // Connect the cascading parameters:
          newChildNode.parameters.setParent(newNode.parameters);
        });
      }

      return newNode;
    }

    /**
     * This adds the given node to the parent node.
     * @param parent The parent node to add to.
     * @param node The node to add.
     */
    function addNodeToParent(parent, node) {
      node.parent = parent;
      if (parent) {
        // We have a parent node.
        // Make sure that the parent node has a children collection:
        if (!parent.children) parent.children = [];

        // Add the node to the parent:
        parent.children.push(node);
      }
    }


    /**
     * This disconnects the given node from its parent in the navigation tree.
     * This is useful if the node is a template used for cloning.
     * After the cloning you need to disconnect the node from the tree so it doesn't appear.
     * @param node
     */
    function disconnectNode(node) {
      // Remove the node from the parent collection:
      if (node.parent) {
        // Disconnect cascading parameters:
        node.parameters.setParent(null);

        // Remove the node from the parent's children collection:
        if (node.parent.children) {
          // Remove the node from the parent node:
          node.parent.children.splice(node.parent.children.indexOf(node), 1);
        }

        // Clear the reference to the parent:
        node.parent = null;
      }
    }

    /**
     * This extends the given parameters onto the node.
     * @param node The node to update with parameters.
     * @param parametersToMerge The parameters to merge into the node.
     */
    function setParameters(node, parametersToMerge) {
      // Make sure there is a parameters object:
      if (!node.parameters && node.parent && node.parent.parameters) {
        // Create a default parameter object:
        node.parameters = new CascadingParameters(node.parent.parameters);
      }
      // Now we have a parameters object.

      // Set all the parameters at once:
      node.parameters.setMany(parametersToMerge);
    }


    /**
     * This gets a promise for the resolved navigation tree.
     * @returns {Promise} A promise for the resolved and update navigation tree, after navigation providers have run.
     */
    function getNavigationTreePromise() {
      // Resolve the navigation tree recursively:
      var allNavTreeProvidersAreDone = resolveNavigationTreeRecursively(originalNavigationTree);

      // Handle the new root node:
      return allNavTreeProvidersAreDone.then(function (newRootNode) {
        // Adorn the tree that we have so far with useful properties and functions:
        newRootNode.children.forEach(adornTree);

        // Make sure that all parameters have cascaded down:
        newRootNode.parameters.cascade();

        // Get all the nodes that have providers on them:
        newRootNode.children.forEach(function (node) {
          getNavTreeProviderNodes(node, actualNavigationProviders);
        });
        // Now we know which nodes have providers and need to get expanded.

        return newRootNode;
      });

      //This function returns an array which will contains a reference to the nodes
      //which have navTreeProviders attached to them
      function getNavTreeProviderNodes(node, collectionToAddTo) {
        if (node.provider) {
          collectionToAddTo.push(node)
        }
        if (node.children) {
          for (var i = 0; i < node.children.length; i++) {
            getNavTreeProviderNodes(node.children[i], collectionToAddTo);
          }
        }
      }
    }

    /**
     * This allows navigation providers to identify any badges that need to be updated for the notification.
     * @param notification The notification to update with badge information.
     */
    function addBadgesToNavTree(type, data) {

      // Add helper data structures onto the data so that we have clean code for identifying badges and a clean api for where the badges must go:
      // Create the badge array on the notification:
      if (!data.badges) data.badges = [];

      // Attach the function to add badges:
      data.addBadge = addBadgeToData;
      data.removeBadge = removeBadgeFromData;

      // Go through each navigation provider and give it a chance to update badges on this notification.
      actualNavigationProviders.forEach(function (node) {
        // Check whether this navigation provider knows how to identify badges for notifications:
        if (node.provider.identifyBadges) {
          // Get the path for the navigation tree provider:
          var path = node.getPath();

          // Get the parameters for the node:
          var parameters = node.parameters;

          // Allow the navigation provider to identify badges:
          node.provider.identifyBadges(type, data, path, parameters, node, node.provider.settings, node.provider);
        }
      });

      // Attach the notification as the parent of all the badges:
      attachParent(data);

      // Attach the badges to the navigation tree:
      attachBadgeToNavTree(data);
    }

    //This attaches the parent data (notification or similar) object to each
    //badge object contained within the badges array.
    function attachParent(data) {
      if (data.badges) {
        data.badges.forEach(function (badge) {
          badge.parent = data;
        })
      }
    }

    /**
     * This adds a badge at the given path in the navigation tree.
     * You can call this directly to set badges on the tree.
     * @param badge We expect an object with a path and a count field.
     */
    function addBadgeOnly(badge) {
      // Get the path as an array:
      var pathArray = badge.path.split("/");

      // Strip off the leading slash:
      if (pathArray[0] == '') {
        // There was a leading slash.
        // Strip it off:
        pathArray.shift();
      }

      _.forEach(result.combinedTree, function (node) {
        traverseTree(node, pathArray, addBadge(badge.count, badge));
      })
    }

    /**
     * This removes a badge at the given path in the navigation tree.
     * You can call this directly to set badges on the tree even without having notifications.
     * @param badge We expect an object with a path and a count field.
     */
    function removeBadgeOnly(badge) {
      // Get the path as an array:
      var pathArray = badge.path.split("/");

      // Strip off the leading slash:
      if (pathArray[0] == '') {
        // There was a leading slash.
        // Strip it off:
        pathArray.shift();
      }

      _.forEach(result.combinedTree, function (node) {
        traverseTree(node, pathArray, removeBadge(badge.count, badge));
      })
    }

    //Wrapper function for modifyBadges function
    //with removeBadges function set to false
    function attachBadgeToNavTree(data) {
      data.badges.forEach(function (badge) {
        addBadgeOnly(badge);
      });
    }

    //Wrapper function for calling the modifyBadges function
    //with removeBadges set to true
    function removeBadgesFromNavTree(data) {
      data.badges.forEach(function (badge) {
        removeBadgeOnly(badge);
      });
    }

    //This function takes in the badge count and badge object and returns
    //a function that will take in a node from the navigation tree and attach the badgeObjectsArray
    //and the badgeCount to the navigation tree node.
    function addBadge(badgeCount, badgeObject) {
      return function (node) {
        if (node.badgeObjectsArray) {
          node.badgeObjectsArray.push(badgeObject);
        }
        else {
          node.badgeObjectsArray = [];
          node.badgeObjectsArray.push(badgeObject);
        }

        if (node.badgeCount) {
          node.badgeCount += badgeCount;
        }
        else {
          node.badgeCount = badgeCount;
        }
      };
    }

    //This mutator functions is called by modifyBadges function and removes
    //badges and the associated badge object from nav tree.
    function removeBadge(badgeCount, badgeObject) {
      return function (node) {
        if (node.badgeObjectsArray) {
          node.badgeObjectsArray.splice(node.badgeObjectsArray.indexOf(badgeObject), 1);
          node.badgeCount -= badgeCount;
        }
      };
    }

    //This function takes in a node of the navigation tree and an array which contains the
    //path to node in the tree the function should dig too.
    //When at the necessary node depth the function then calls
    //the mutatorFn to modify the node as needed.
    function traverseTree(node, pathArray, mutatorFn) {
      if (node.displayName == pathArray[0]) {

        if (pathArray.length > 1) {
          //Because we now have found this point in the nav tree
          //we no longer need the first element in the path array.
          pathArray.shift();
          _.forEach(node.children, function (child) {
            traverseTree(child, pathArray, mutatorFn);
          });
        }
        else {
          mutatorFn(node);

        }
      }
    };


    /**
     * Adds a badge to the data.
     * @param path The path where the badge should be added in the navigation tree.
     * @param count The badge count to increment by.
     */
    function addBadgeToData(path, count) {
      this.badges.push({path: path, count: count});
    }

    /**
     * Removes a badge from the data
     * @param badge This is the badge object to remove form the data
     */
    function removeBadgeFromData(badge){
      this.badges.splice(this.badges.indexOf(badge),1);
    }

    /**
     * This will select the first tab of the navigation structure.
     * It should be called once the data is published.
     */
    function selectDefaultTab() {
      // Make sure that we automatically select the first tab:
      if (result.combinedTree && result.combinedTree.length > 0) {
        // Check if we have previously selected a tab:
        if (result.selectedTabIndex === undefined) {
          result.selectedTabIndex = 0;

          // Make sure that the selected tab index is still valid:
          if (result.selectedTabIndex >= result.combinedTree.length)result.selectedTabIndex = 0;

          // Select the tab:
          selectTab(result.combinedTree[result.selectedTabIndex]);
        }
      }
    }

    /**
     * This connects the given nodes to the root node and uses the nodes collection as the children array for the root node.
     * @param nodes The new collection of root nodes to use.
     */
    function connectToRootNode(nodes) {
      // Disconnect any existing nodes:
      if (rootNode.children && rootNode.children.length > 0) {
        rootNode.children.forEach(function (node) {
          // Disconnect the node from the root node:
          delete node.parent;
        });
      }

      // Connect the new nodes:
      rootNode.children = nodes;
      rootNode.children.forEach(function (node) {
        // Connect the node to the root node:
        node.parent = rootNode;
      });
    }

    /*
     This adorns the tree with additional properties.
     This attaches a parent property to each node of the navigation tree.
     It also adds a method to get the full path of the tree.
     This lets us traverse the tree from leaf to root.
     */
    function adornTree(node) {
      // Adorn the node:
      node.getPath = getPath;

      // Get information from the parent:
      if (node.parent) {
        // We have a parent node.
        // Make sure that we have cascadint parameters:
        if (!node.parameters) {
          // The node doesn't have cascading parameters.
          // Create cascading parameters that are connected to the parent:
          node.parameters = new CascadingParameters(node.parent.parameters);
        }
        else {
          // The node has cascading parameters.
          // Set the new parent for the cascading parameters:
          node.parameters.setParent(node.parent.parameters);
        }
      }

      // Check if we have children:
      if (node.children) {
        // We have children.
        // Go through each child:
        for (var j = 0; j < node.children.length; j++) {
          // Get the child:
          var child = node.children[j];

          // Attach properties:
          child.parent = node;

          // Recursively adorn the child:
          adornTree(child);
        }
      }
      return node;
    }

    /*
     Gets the path for the current node.
     An optional delimiter can be specified.
     An optional flag to specify whether to use the display names or the names of the navigation tree. Default is display name.
     */
    function getPath(delimiter, useDisplayName) {
      // Make sure we have defaults:
      if (!delimiter) delimiter = "/";
      if (useDisplayName === undefined) useDisplayName = true;

      // Get the parent path:
      var parentPath = this.parent && this.parent.getPath ? this.parent.getPath(delimiter, useDisplayName) : '';

      // Return the current path:
      return parentPath + delimiter + (useDisplayName ? this.displayName : this.name);
    }

    /**
     * Finds the node in the navigation tree at the given path.
     * @param {string|Array} path The path to find in the navigation tree. eg: 'Overview/Reconciliation'
     */
    function findNode(path) {
      // Make sure that the path is an array:
      // http://stackoverflow.com/questions/5941706/javascript-detect-if-argument-is-array-instead-of-object-node-js
      if (!(path instanceof Array)) {
        // We assume it's a string.
        // Get the parts of the path:
        path = path.split("/");
      }
      // Now we have path as an array.

      // Strip off the leading slash:
      if (path[0] == '') {
        // There was a leading slash.
        // Strip it off:
        path.shift();
      }
      // Now have the clean path as an array.

      // Walk down the path:
      var pathNode = null;
      var currentCollection = result.combinedTree;
      for (var pathIndex = 0; pathIndex < path.length; pathIndex++) {
        // Make sure that we have a current collection:
        if (!currentCollection) {
          // We ran out of children while we were walking the path.
          // This means that we did not find the node we were looking for.
          return null;
        }
        // Now we know we have a collection of nodes to look through.

        // Check whether there is a node with the given name:
        var nodeResult = currentCollection.filter(function (node) {
          // Check whether the name matches the path:
          return node.name == path[pathIndex];
        });

        // Check whether we found the node:
        if (nodeResult.length == 0) {
          // We did not find a node with that name.
          // Break out because the node doesn't exist at that path.
          return null;
        }
        else {
          // We found a node at this point in the path.
          // Save this node so far:
          pathNode = nodeResult[0];

          // Move to the next collection:
          currentCollection = pathNode.children;
        }
      }
      // If we get here then it means we successfully found the node at the desired path.
      return pathNode;
    }

    /*
     This removes nodes that have navigation providers from the given collection.
     If a node has a navTreeProvider and no settings then it is considered to be dynamic and will be removed.
     If a dynamic node has a setting of isStatic: true then it is left in the list type.
     */
    function removeDynamicProviderNodes(nodeCollection) {
      // Go through each node and remove items with nav tree providers:
      for (var i = 0; i < nodeCollection.length; i++) {
        // Check whether there is a dynamic nav tree provider on this node so we can delete it:
        var isDynamic = nodeCollection[i].navTreeProvider && (nodeCollection[i].navTreeProvider.settings == undefined || nodeCollection[i].navTreeProvider.settings.isStatic == false);
        if (isDynamic) {
          // Remove this item from the array:
          nodeCollection.splice(i, 1);

          // Decrement the counter because we remove the current item:
          i--;
        }
      }
      // Go through each node and update recursively:
      nodeCollection.forEach(function (node) {
        // Apply the code recursively if there are children:
        if (node.children) {
          removeDynamicProviderNodes(node.children);
        }
      });
    };

    /*
     This dynamically defines the state for the given navigation tree item and all its children.
     */
    function defineStates(navItem) {
      // Add the state:
      var stateName = dynamicDashboardStates.addState(navItem);

      // Save the state name:
      navItem.stateName = stateName;

      // Check if there are children:
      if (navItem.children) {
        // Go through each child recursively:
        navItem.children.forEach(defineStates);
      }
    };

    /**
     * This function takes in the state url which is '/' delimitted string and converts this to an array
     * It then finds the necessary selected tab and dashboard using the elements from state array
     * @param stateUrlArray
     */
    function selectTabAndDashboardFromStateUrl(stateUrl) {
      var stateUrlArray = stateUrl.split('/');
      //Because of the leading slash there will be an empty element in the array, so remove it
      stateUrlArray.splice(0, 1);
      var tabToSelect = {};
      var dashboardToSelect = {};
      //The first element in the state url array will be the tab.
      var tabToSelectName = stateUrlArray[0];
      var tabToSelect = result.combinedTree.find(function (node) {
        if (node.displayName == tabToSelectName) {
          //We have found the tab we want to be taken too, therfore it does not need to be on the stateUrlArray
          stateUrlArray.splice(0, 1);
        }
        return node.displayName == tabToSelectName;
      })

      //If the state array has elements remaining these will define the point in the menu that we must navigate to
      if (stateUrlArray.length >= 1) {
        //By default set the selected dashboard to selected tab so we can start searching for dashbord to select
        dashboardToSelect = tabToSelect;
        //As we go through each state array item we are going deeper into the tree.
        stateUrlArray.forEach(function (state) {
          //Check if there are children elements
          if (dashboardToSelect.children) {
            //Set the selected dashboard to the child element which matches the state name
            //the dashbordToSelect needs to become equal to this child element as this allows us to dig into the tree to find the dashbaord we are looking for.
            dashboardToSelect = dashboardToSelect.children.find(function (dashboard) {
              return state == dashboard.displayName;
            })
          }

        })
      }
      else {
        //If only the tab was specified in the url then select the first child as the dashboard to go to
        dashboardToSelect = tabToSelect.children[0];
      }

      //Select the tab and dashboard.
      selectTab(tabToSelect);
      selectDashboard(dashboardToSelect)

    }

    /*
     This selects a tab for the root nav item in the tree.
     */
    function selectTab(rootNavItem) {
      if (currentSelectedTab) {
        currentSelectedTab.active = false;
      }
      // Save the selected tab index:
      result.selectedTabIndex = rootNavItem.tabIndex;
      rootNavItem.active = true;
      currentSelectedTab = rootNavItem;
      // Show the children of the selected tab in the menu:
      menu = rootNavItem.children;

      // Check whether there are any children so we can select the first dashboard automatically:
      if (rootNavItem.children && rootNavItem.children.length > 0) {
        // There are children.
        // Select the first menu item:
        selectDashboard(rootNavItem.children[0]);
      }
      publish();
    };

    /*
     This selects a tab for the root nav item in the tree.
     */
    function selectDashboard(navItem, $event) {
      if (currentSelectedDashBoard) {
        currentSelectedDashBoard.active = false;
      }
      navItem.active = true;
      currentSelectedDashBoard = navItem;
      // Because our nav tree is a hierarchical structure
      // we don't want any of our parents to handle the click event
      // therefor we stop the event from bubbling up.
      if ($event) {
        $event.stopPropagation();
      }

      if (navItem) {
        // Go to that state:
        dynamicDashboardStates.go(navItem);
      }
      publish();
    }

    /*
     This subscribes to navigation tree data updates.
     Whenever navigation tree data is available or changes, the callback will be called with the navigation tree data object as the argument.
     */
    function subscribe(callback) {
      // Save the callback:
      callbacks.push(callback);

      // Check whether the trade navigation tree is already available:
      if (result.loaded) {
        // Publish the data immediately to the callback:
        callback(result.combinedTree, menu);
      }
    }

    /*
     This un-subscribes from navigation tree data updates.
     */
    function unsubscribe(callback) {
      var index = callbacks.indexOf(callback);
      if (index > -1) {
        // We found this callback.
        // Remove it:
        callbacks.splice(index, 1);
      }
    }

    /*
     This publishes data to all subscribers.
     */
    function publish() {
      callbacks.forEach(function (callback) {
        // Pass the updated data to the callback:
        callback(result.combinedTree, menu);
      });
    }

  }

});