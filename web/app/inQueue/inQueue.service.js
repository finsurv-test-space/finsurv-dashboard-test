define(['./inQueue.module', 'angular'], function (module, angular) {

    /*
     This data service gives us access to ALL the inQueue data .
     */

    module.factory('inQueue', inQueue);

    // Define the inQueue service:
    inQueue.$inject = ['$http', '$timeout'];
    function inQueue($http, $timeout) {
   

    // Create the return structure:
    
    return{
            setDate: setDate,
            getData: getData
    }
    
    
    var date ;
    
    function setDate(newDate)
    {
        date = newDate;
    }
    
    function getDate()
    {
        return date;
    }
    
    function getData()
    {  
       var url = "http://localhost:8080/tXstreamServer";
       var requestString = url + "/statistics/uploads/date/" + getDate();
       return $http.get(requestString) ;
    }

  }


});