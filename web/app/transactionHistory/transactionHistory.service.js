define(['./transactionHistory.module', 'angular'], function (module, angular) {

    /*
     This data service gives us access to ALL the inQueue data .
     */

    module.factory('transactionHistory', transactionHistory);

    // Define the inQueue service:
    transactionHistory.$inject = ['$http', '$timeout'];
    function transactionHistory($http, $timeout) {
   
        // Create the return structure:
   

        var transactionHistory = [

            { "userName": "GMocbSfykHrT ", "DateTime": "2015-10-26", "Event": "#FF6600","EventRecord":"fsdfw" },
            { "userName": "MtywFXTRXQmE ", "DateTime": "2015-10-28", "Event": "#FF6600","EventRecord":"sxsx" },
            { "userName": "MtywFXTRXQmE ", "DateTime": "2016-01-16", "Event": "#FF6600","EventRecord":"sxsxs" },


        ];


        return transactionHistory
    }

});