// Load all the related files for the module:
define([
  './pnotify-angular.module',
  './pnotify-angular.service'
], function (module) {
  return module;
});