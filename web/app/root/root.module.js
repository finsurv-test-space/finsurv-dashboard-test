define(['jquery','angular',
      'angular-ui-router',
    ],
    function (jquery,angular) {
      var rootModuleModuleDependencies = [
        'ui.router'
      ];
      var rootModule = angular.module('root', rootModuleModuleDependencies)
      return rootModule;
    })