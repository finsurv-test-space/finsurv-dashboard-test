//Had to replace . with dashboard here otherwise the files were not found
define(['dashboards/dashboards.module', 'require', 'dashboards/dashboards.constants'], function (module, require) {

  // Configure a controller:
  module.controller('rootController', controller);

  // Define the root controller:
  controller.$inject = ['$scope', '$window'];
  function controller($scope, $window)
  {
    // Show the menu by default:
    $scope.sideMenuOpen = true;
   
    /*
    * This toggles the side menu panel.
    */
    $scope.toggleSideMenu = function ()
    {
      $scope.sideMenuOpen = !$scope.sideMenuOpen;
      
      setTimeout(function(){
        $window.dispatchEvent(new Event("resize"));
      },300);
    }

  }

});


