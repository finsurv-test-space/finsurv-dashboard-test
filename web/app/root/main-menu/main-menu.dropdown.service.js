/**
 * Created by daniel on 15/09/21.
 */
define(['root/root.module'], function (module) {
  module.factory('mainMenuDropdownService', mainMenuDropdownService);
  //mainMenuDropdownService.$inject=[];
  function mainMenuDropdownService() {
    var registeredDropdownItems = []
    var topDropDownItem = {};
    var service = {};
    service.registerTopDropDownItem = registerTopDropDownItem;
    service.registerDropdownItem = registerDropdownItem;
    service.getRegisteredItems = getRegisteredItems;
    service.getTopDropDownItem = getTopDropDownItem;

    function registerTopDropDownItem(item) {
      topDropDownItem = item;
    }

    //items is an array containing an object with the name of the menu item to be added
    //and the fontawesome icon to be displayed
    function registerDropdownItem(items) {
      //Check if items being sent in as an array or a single item
      if (items instanceof Array) {
        items.forEach(function (item) {
          //Some items may have a function for checking whether to show.
          //However an empty argument passed to ng-show function on the template is seen as false and item is hidden, therefor default value to true.
          if (!item.hasOwnProperty('show')) {
            item.show = true;
          }
          registeredDropdownItems.push(item);
        })
      }
      else {
        //Some items may have a function for checking whether to show.
        //However an empty argument passed to ng-show function on the template is seen as false and item is hidden, therefor default value to true.
        if (!items.hasOwnProperty('show')) {
          items.show = function () {
            return true;
          };
        }
        registeredDropdownItems.push(items);
      }
    };
    function getRegisteredItems() {
      return registeredDropdownItems;
    }

    function getTopDropDownItem() {
      return topDropDownItem;
    }

    return service;
  }

  return module;
})
