/**
 * Created by Mokgali on 15/12/10.
 */
define(['root/root.module','angular','jquery'], function (module,angular,jquery) {
  module.controller('MainMenuController', MainMenuController);
  MainMenuController.$inject = ['$scope','brand','$sce']
  function MainMenuController($scope,brand,$sce) {
    
    $scope.brand=function(value) {
    return $sce.trustAsHtml(brand.brandName);
      };

  }

  return module;
})