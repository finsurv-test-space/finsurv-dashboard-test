/**
 * Created by daniel on 15/09/21.
 */
define(['root/root.module'], function (module) {
  module.controller('MainMenuDropdownController', MainMenuDropdownController);
  MainMenuDropdownController.$inject = ['$scope', 'mainMenuDropdownService']
  function MainMenuDropdownController($scope, mainMenuDropdownService) {
    $scope.listItems = mainMenuDropdownService.getRegisteredItems();
    $scope.topItem = mainMenuDropdownService.getTopDropDownItem();
  }

  return module;
})