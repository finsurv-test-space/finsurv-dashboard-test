define([
  'root/root.module',
  'root/root.states',
  'root/root.state',
  'root/root.controller',
  'root/main-menu/main-menu.dropdown.service'
], function (module) {
  return module;
})