'use strict';
define([
  'require',
  'root/main-menu/main-menu.dropdown.controller',
  'root/main-menu/main-menu.brand.controller'
], function (require) {
  return {
    name: 'root',
    abstract: 'true',
    controllerAs: 'rootController',
    url: '',
    data: {
      requireLogin: true
    },
    views: {
      '': {
        templateUrl: require.toUrl('root/root.html'),
        controller: 'rootController'
      },
      'main-menu@root': {
        templateUrl: require.toUrl('root/main-menu/main-menu.html'),
        controller:'MainMenuController'
      },
      'main-menu-dropdown@root': {
        templateUrl: require.toUrl('root/main-menu/main-menu-dropdown.html'),
        controller: 'MainMenuDropdownController'
      }
    }
  }
});

