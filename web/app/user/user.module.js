/**
 * Created by daniel on 15/09/02.
 */

define(['angular',
      'angular-ui-router'
    ],
    function (angular) {
      var userModuleModuleDependencies = [
        'ui.router',
          'settings'
      ];
      var userModule = angular.module('user', userModuleModuleDependencies).run(runBlock);
      runBlock.$inject = ['settingsService','settingsConstants','user'];//The user injected here is the user service. settingsConstants is from the settings module.
      function runBlock(settingsService,settingsConstants,user) {
        //This is to register the  user settings menu item
        function registerUserSettings(){
          var userSettingsMenuItem = [
            {name: 'Users', cssClass: 'i_user', description: 'Manage system users', roles: ['admin', 'user'], state: 'usersSettings',permissions:settingsConstants.PermissionType.USER_MANAGEMENT},
            {name: 'Groups', cssClass: 'i_groups', description: 'Manage user groups',roles: ['admin', 'user'], state: 'groupsSettings',permissions:settingsConstants.PermissionType.USER_MANAGEMENT},
            {name: 'Roles', cssClass: 'i_roles', description: 'Manage roles',roles: ['admin', 'user'], state: 'rolesSettings',permissions:settingsConstants.PermissionType.USER_MANAGEMENT}];
          settingsService.registerMenuItem(userSettingsMenuItem);
          //Only want to register this once therefor unsubscribe this function once it has been run
        }
        //Only register the settings once the user has signed in

        //The reason for doing this in the runblock is so that the module can register its settings menu as it loads up.
        //One problem with doing this here is that the run block only gets run once at the app start up. This means if you logout
        //and log back in again as another user then the new settings items will not be loaded into the settings menu.
        //Subscribing the RegisterSettings function with the user module, means that as the user is updated we can then check to see if we need to register
        //these settings modules. This means that users will have the setting that are specific to them.
        function RegisterSettings(currentUser){
          if(currentUser.loggedIn){
            registerUserSettings();
          }
        }
        user.subscribe(RegisterSettings);
      }
      return userModule;
    })