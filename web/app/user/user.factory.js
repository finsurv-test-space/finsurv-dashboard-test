/**
 * Created by daniel on 15/09/02.
 */
define(['angular', 'user/user.module', 'config.list'], function (angular, userModule, configList) {
  'use strict';

  //Solution is from http://jasonwatmore.com/post/2015/03/10/AngularJS-User-Registration-and-Login-Example.aspx
  userModule.factory('user', userFactory);

  //This is the real user service must use once server has been setup.
  userFactory.$inject = ['$http', '$q', '$interval', 'mainMenuDropdownService'];
  function userFactory($http, $q, $interval, mainMenuDropdownService) {
    var callbacks = [];
    var user = {
      loaded: false,
      loggedIn: false,
      clearUser: clearUser,
      checkUserPermissions: checkUserPermissions,
      queryUser: function () {
        return $http.get(configList.serverUrl + "/resource/secure/whoamI");
      },
      permissions: [],
      poll: poll,
      setUser: setUser,
      subscribe: subscribe,
      unsubscribe: unsubscribe
    };

    user.queryUser().then(function (response) {
      //Setting the user object here so that the user object can then be obtained using the getUser function
      user.setUser(response);
    }, function (error) {
      user.clearUser(error);
    })
    function setUser(response) {
      //Setting the user object here so that the user object can then be obtained using the getUser function
      angular.extend(user, response.data);
      user.loggedIn = true;
      user.loaded = true;
      user.loginError = undefined;

      var dropdownSettingsItem = {name: user.name, icon: 'fa-user'}
      mainMenuDropdownService.registerTopDropDownItem(dropdownSettingsItem);

      publish();
    }

    //This function polls the server to check user is logged and thens sets the user accordingly.
    function poll() {
      $interval(function () {
        user.queryUser().then(function (response) {
          //Set the user if user is logged in on the server but not set on the app side
          if (!user.loaded || !user.loggedIn) {
            user.setUser(response);
          }
        }, function (error) {
          if (user.loggedIn || user.loaded) {
            user.clearUser(error);
          }
        })
      }, 30000)
    }

    function clearUser(error) {
      user.loggedIn = false;
      user.loaded = false;
      user.loginError = undefined;
      if (error) {
        console.error(error);
      }
      publish();
    }

    function checkUserPermissions(permissions, mustAllPermissionsExistInPermissionsArray) {
      if (user && user.loggedIn) {
        var flag = false;
        //Check if permissions is array;
        if (permissions instanceof Array) {
          if (mustAllPermissionsExistInPermissionsArray === undefined || mustAllPermissionsExistInPermissionsArray){
            //Make sure all set permissions exist in the permission list do exist and if one does not exist, exit with false
            for (var i = 0; i < permissions.length; i++) {
              if (user.permissions.indexOf(permissions[i].value) !== -1) {
                flag = true;
              }
              else {
                //If any permissions in the array do no exist in the user permission then return false.
                flag = false;
                break;
              }
            }
          }
          else{
            //Check that one of the permissions exist in the users permissions
            var anyPermissionsExist = false;
            for (var i = 0; i < permissions.length; i++) {
              if (user.permissions.indexOf(permissions[i].value) !== -1) {
                anyPermissionsExist = true;
                break;
              }
            }

            if (anyPermissionsExist) {
              flag = true;
            }
          }
        }
        //If permissions is not an array check the permission
        else if (user.permissions.indexOf(permissions.value) !== -1) {
          flag = true;
        }
        return flag;
      }
      else {
        return false;
      }
    }


    /*
     This subscribes to trade feed data updates.
     Whenever trade feed data is available or changes, the callback will be called with the trade feed data object as the argument.
     */
    function subscribe(callback) {
      // Save the callback:
      callbacks.push(callback);
      // Check whether the trade feed data is already available:
      if (user.loggedIn) {
        // Publish the data immediately to the callback:
        callback(user);
      }
    }

    /*
     This un-subscribes from trade feed data updates.
     */
    function unsubscribe(callback) {
      var index = callbacks.indexOf(callback);
      if (index > -1) {
        // We found this callback.
        // Remove it:
        callbacks.splice(index, 1);
      }
    }

    /*
     This publishes data to all subscribers.
     */
    function publish() {
      if (callbacks) {
        // Make sure to copy the array in case one of the callbacks adds or removes more callbacks:
        var oldCallbacks = callbacks.slice(); // shallow copy of the array: http://stackoverflow.com/questions/7486085/copying-array-by-value-in-javascript
        oldCallbacks.forEach(function (callback) {
          // Pass the updated data to the callback:
          callback(user);
        });
      }
    }


    return user;
  }

  return userModule;
})