/**
 * Created by daniel on 15/09/17.
 */
define(['user/user.module','require'],function(module,require){
  module.controller('usersSettingsController',usersSettingsController);
  usersSettingsController.$inject=['$scope','$uibModal','$http','users','groups','roles','notifyPopup'];
  //The users, groups and roles are defined in the resolve section of the user.state.js file
  function usersSettingsController($scope,$modal,$http,users,groups,roles,notifyPopup){
    $scope.users = users;
    $scope.groups = groups;
    $scope.roles = roles;

    //if($scope.users.length < 1){
    //  $('#no-data-block').show();
    //}

    // lists all group names for a user by finding
    // against the ID. This requires the group list
    // to be queried.
    $scope.listGroups = function(user) {
      return '' +
          _.map(user.groups,
              function(groupID) {
                return (_.findWhere(groups, { id : groupID }).name);
              }
          ).join(', ');
    };

    $scope.enableDisableUser = function (user) {
      notifyPopup('Confirmation', 'Are you sure you want to ' + (user.enabled ? 'disable' : 'enable') + ' the user: ' + user.username + '?', function () {
        $http.get('resource/secure/user/' + user.id + "/" + (!user.enabled ? "enable" : "disable"))
            .success(
            function (data, status, headers, config) {
              if (status == 200) {
                user.enabled = !user.enabled;

                notifyPopup("User successfully " + (user.enabled ? 'enabled' : 'disabled') + ".");
              }
            })
            .error(
            function (data, status, headers, config) {
              notifyPopup("Error " +  (!user.enabled ? 'enabling' : 'disabling') + " user: " + (data && data.message ? data.message : status));
            }
        );
      }, {}, true);
    };
    $scope.editUser = function(user) {
      var modalInstance =
          $modal.open({
            templateUrl : require.toUrl('user/settings/editUser.html'),
            controller : ['$scope','$modalInstance',function($scope, $modalInstance) {
              $scope.user = angular.copy(user);
              $scope.confirmPassword = $scope.user.passwordHash;
              $scope.groups = groups;
              $scope.authProviders = [ "INTERNAL", "ACTIVE_DIRECTORY" ];
              $scope.saving = false;

              $scope.passwordValid = function () {
                if ($scope.user.authProvider == 'ACTIVE_DIRECTORY') {
                  return true;
                } else if ((!$scope.confirmPassword && !$scope.user.passwordHash) || ($scope.confirmPassword == $scope.user.passwordHash)) {
                  return true;
                }

                return false;
              };

              $scope.ok = function() {
                var resource = null;
                if ($scope.user.id == undefined) {
                  resource = $http.post('resource/secure/users', $scope.user);
                } else {
                  resource = $http.put('resource/secure/user/' + $scope.user.id, $scope.user);
                }
                $scope.saving = true;

                resource.
                    success(
                    // this callback will be called asynchronously when the response is available
                    function(data, status, headers, config) {
                      if (status == 200) {
                        var index = -1;
                        if ((index = _.findIndex(users, {id: $scope.user.id})) == -1) {
                          users.push(data);

                          if(users.length > 0){
                            $('#no-data-block').hide();
                          }

                          notifyPopup("User successfully saved.");
                        } else {
                          users[index] = data;
                        }

                        $modalInstance.close();
                      }
                    }
                ).error(
                    // called asynchronously if an error occurs or server returns response with an error status.
                    function(data, status, headers, config) {
                      notifyPopup("Error saving user: " + (data && data.message ? data.message : status));

                      $scope.saving = false;
                    }
                );
              };

              $scope.cancel = function() {
                $modalInstance.dismiss('cancel');
              };

              $scope.open = function($event) {
                $event.preventDefault();
                $event.stopPropagation();
                $scope.opened = true;
              };
            }],
            size : 'lg'
          });
    }

  }
  return module;
})