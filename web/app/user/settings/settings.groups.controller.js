/**
 * Created by daniel on 15/09/22.
 */
define(['user/user.module','require'],function(module,require){
  module.controller('groupsSettingsController',groupsSettingsController);
  groupsSettingsController.$inject=['$scope','$http','$uibModal','groups','roles','notifyPopup'];
  //Groups and roles are defined in the resolve section the groups.state.js file
  function groupsSettingsController ($scope,$http,$modal,groups,roles,notifyPopup){
    $scope.groups = groups;
    $scope.roles = roles;
    $scope.listRoles = function(group) {
      return '' +
          _.map(group.roles,
              function(roleID) {
                return (_.findWhere(roles, { id : roleID }).name);
              }
          ).join(', ');
    };

    //if($scope.groups.length < 1){
    //  $('#no-data-block').show();
    //}

    $scope.enableDisableGroup = function (group) {
      notifyPopup('Confirmation', 'Are you sure you want to ' + (group.enabled ? 'disable' : 'enable') + ' the group: ' + group.name + '?', function () {
        $http.get('resource/secure/group/' + group.id + "/" + (!group.enabled ? "enable" : "disable"))
            .success(
            function (data, status, headers, config) {
              if (status == 200) {
                group.enabled = !group.enabled;

                notifyPopup("Group successfully " + (group.enabled ? 'enabled' : 'disabled') + ".");
              }
            })
            .error(
            function (data, status, headers, config) {
              notifyPopup("Error " +  (!group.enabled ? 'enabling' : 'disabling') + " group: " + (data && data.message ? data.message : status));
            }
        );
      }, {}, true);
    };
    $scope.editGroup = function (group) {
      var modalInstance = $modal.open({
        templateUrl:require.toUrl('user/settings/editGroup.html'),
        controller : ['user','$scope','$modalInstance',function (user, $scope, $modalInstance) {
          $scope.group = angular.copy(group);
          $scope.roles = roles;
          $scope.saving = false;

          $scope.ok = function () {
            var resource = null;
            if ($scope.group.id == undefined) {
              resource = $http.post('/resource/secure/groups', $scope.group);
            } else {
              resource = $http.put('/resource/secure/group/' + $scope.group.id, $scope.group);
            }
            $scope.saving = true;

            resource
                .success(function (data, status, headers, config) {
                  if (status == 200) {
                    var index = -1;
                    if ((index = _.findIndex(groups, {id: $scope.group.id})) == -1) {
                      groups.push(data);

                      if(groups.length > 0){
                        $('#no-data-block').hide();
                      }

                      notifyPopup("Group successfully saved.");
                    } else {
                      groups[index] = data;
                    }
                    var currentUser = user;
                    // Refresh roles, a similiar server operation is done to ensure whoAmI returns correctly
                    if (currentUser.groups.indexOf($scope.group.name) !== -1) {
                      currentUser.roles =
                          _.map($scope.group.roles,
                              function(roleID) {
                                return (_.findWhere(roles, { id : roleID }).name);
                              }
                          );
                    }

                    $modalInstance.close();
                  }
                })
                .error(function (data, status, headers, config) {
                  notifyPopup("Error saving group: " + (data && data.message ? data.message : status));

                  $scope.saving = false;
                });
          };

          $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
          };

          $scope.open = function ($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.opened = true;
          };
        }],
        size       : 'lg'
      });
    }

  }
  return  module;
})