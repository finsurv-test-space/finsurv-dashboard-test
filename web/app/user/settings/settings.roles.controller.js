/**
 * Created by daniel on 15/09/22.
 */
define(['user/user.module','require'],function(module,require){
  module.controller('rolesSettingsController',rolesSettingsController);
  rolesSettingsController.$inject=['$scope','$http','$uibModal','notifyPopup','roles','permissions','feedData', '$rootScope', 'userEvents'];
  //Roles, permissons and feedData are defined in the resolve section of the roles.state.js file
  // userEvents is from user.constants.
  function rolesSettingsController($scope,$http,$modal,notifyPopup,roles,permissions,feedData, $rootScope, userEvents){
    $scope.roles = roles;
    $scope.permissions = permissions;
    $scope.feeds = feedData;

    $scope.listPermissions = function(role) {
      return '' +
          _.map(role.permissions,
              function(permissionID) {
                return (_.findWhere(permissions, { id : permissionID }).name);
              }
          ).join(', ');
    };

    $scope.listFeeds = function(feeds) {
      return '' +
          _.map(feeds,
              function(feedId) {
                return (_.findWhere(feeds, { id : feedId }).name);
              }
          ).join(', ');
    };

    $scope.enableDisableRole = function (role) {
      notifyPopup('Confirmation', 'Are you sure you want to ' + (role.enabled ? 'disable' : 'enable') + ' the role: ' + role.name + '?', function () {
        $http.get('resource/secure/role/' + role.id + "/" + (!role.enabled ? "enable" : "disable"))
            .success(
            function (data, status, headers, config) {
              if (status == 200) {
                role.enabled = !role.enabled;

                notifyPopup("Role successfully " + (role.enabled ? 'enabled' : 'disabled') + ".");
              }
            })
            .error(
            function (data, status, headers, config) {
              notifyPopup("Error " +  (!role.enabled ? 'enabling' : 'disabling') + " role: " + (data && data.message ? data.message : status));
            }
        );
      }, {}, true);
    };
    $scope.editRole = function (role) {
      var modalInstance = $modal.open({
        templateUrl:require.toUrl ('user/settings/editRole.html'),
        controller :['$scope','$modalInstance', function ($scope, $modalInstance) {
          $scope.role = angular.copy(role);
          $scope.permissions = permissions;
          $scope.feedData = feedData;
          $scope.saving = false;

          $scope.ok = function () {
            var resource = null;
            if ($scope.role.id == undefined) {
              resource = $http.post('/resource/secure/roles', $scope.role);
            } else {
              resource = $http.put('/resource/secure/role/' + $scope.role.id, $scope.role);
            }
            $scope.saving = true;

            resource
                .success(function (data, status, headers, config) {
                  if (status == 200) {
                    var index = -1;
                    if ((index = _.findIndex(roles, {id: $scope.role.id})) == -1) {
                      roles.push(data);

                      if(roles.length > 0){
                        $('#no-data-block').hide();
                      }

                      notifyPopup("Role successfully saved!");
                    } else {
                      roles[index] = data;
                    }

                    // Broadcast that the roles have changed so other people can update appropriately:
                    $rootScope.$broadcast(userEvents.ROLES_CHANGED);

                    $modalInstance.close();
                  }
                })
                .error(function (data, status, headers, config) {
                  notifyPopup("Error saving role: " + (data && data.message ? data.message : status));

                  $scope.saving = false;
                });
          };

          $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
          };

          $scope.open = function ($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.opened = true;
          };
        }],
        size       : 'lg'
      });
    }

  }
})