define([
  'user/user.module',
  'user/user.state',
  'user/roles.state',
  'user/groups.state'
], function (module) {
  // Get the arguments as an array:
  // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/arguments
  var states = Array.prototype.slice.call(arguments);
  //Remove the module from the array list
  states.splice(0,1);
  // Configure our states at config time:
  module.config(configStates);

  // Define the states for dashboards:
  configStates.$inject = ['$stateProvider'];
  function configStates($stateProvider)
  {
    states.forEach(function(state){
      // Define all the states that we have:
      $stateProvider.state(state);
    })

  }

});