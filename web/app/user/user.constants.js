define(['./user.module'], function (module) {
  // Define any constants:
  module.constant('userEvents', {
      ROLES_CHANGED: 'Roles Changed'
    });
});