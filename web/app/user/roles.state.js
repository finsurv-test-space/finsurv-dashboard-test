'use strict';

define([
  'require',
  'user/settings/settings.roles.controller'
], function (require) {
  return {
    parent: 'settings',
    name: 'rolesSettings',
    url: '/Roles',
    data: {
      requireLogin: true
    },
    resolve: {
      roles: function ($q, $resource) {
        var deferred = $q.defer();
        $resource('resource/secure/roles').query().$promise
            .then(function (data) {
              deferred.resolve(data);
            });
        return deferred.promise;
      },
      permissions: function ($q, $resource) {
        var deferred = $q.defer();
        $resource('resource/secure/permissions').query().$promise
            .then(function (data) {
              deferred.resolve(data);
            });
        return deferred.promise;
      },
      feedData: function ($q, $resource) {
        var deferred = $q.defer();
        $resource('resource/secure/feeds').query().$promise
            .then(function (data) {
              deferred.resolve(data);
            });
        return deferred.promise;
      }
    },
    views: {
      'layout@root': {
        templateUrl: require.toUrl('user/settings/settings.roles.html'),
        controller: 'rolesSettingsController'
      }
    }
  }
});
