'use strict';

define([
  'require',
  'user/settings/settings.groups.controller'
], function (require) {
  return {
    parent: 'settings',
    name: 'groupsSettings',
    url: '/Groups',
    data: {
      requireLogin: true
    },
    resolve: {
      groups: function ($q, $resource) {
        var deferred = $q.defer();
        $resource('resource/secure/groups').query().$promise
            .then(function (data) {
              deferred.resolve(data);
            });
        return deferred.promise;
      },
      roles: function ($q, $resource) {
        var deferred = $q.defer();
        $resource('resource/secure/roles').query().$promise
            .then(function (data) {
              deferred.resolve(data);
            });
        return deferred.promise;
      }
    },
    views: {
      'layout@root': {
        templateUrl: require.toUrl('user/settings/settings.groups.html'),
        controller: 'groupsSettingsController'
      }
    }

  }

});
