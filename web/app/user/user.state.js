'use strict';

define([
  'require',
  'user/settings/settings.users.controller'

], function (require) {

  return {
    parent: 'settings',
    name: 'usersSettings',
    resolve: {
      users: function ($q, $resource) {
        var deferred = $q.defer();
        $resource('resource/secure/users').query().$promise
            .then(function (data) {
              deferred.resolve(data);
            });
        return deferred.promise;
      },
      groups: function ($q, $resource) {
        var deferred = $q.defer();
        $resource('resource/secure/groups').query().$promise
            .then(function (data) {
              deferred.resolve(data);
            });
        return deferred.promise;
      },
      roles: function ($q, $resource) {
        var deferred = $q.defer();
        $resource('resource/secure/roles').query().$promise
            .then(function (data) {
              deferred.resolve(data);
            });
        return deferred.promise;
      }
    },
    url: '/Users',
    //abstract: true,
    //parent: 'root',
    //resolve: {
    //},
    data: {
      requireLogin: true
    },
    views: {
      'layout@root': {
        templateUrl: require.toUrl('user/settings/settings.users.html'),
        controller: 'usersSettingsController'
      }
    }

  }

});
