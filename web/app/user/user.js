define(['./user.module',
        './user.constants',
        './user.factory',
        './user.state',
        './roles.state',
        './groups.state',
        './user.states',
        './settings/settings.users.controller',
        './settings/settings.roles.controller',
        './settings/settings.groups.controller'],function(userModule){
        return userModule;
})