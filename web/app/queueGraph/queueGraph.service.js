define(['./queueGraph.module', 'angular'], function(module, angular) {

  /*
   This data service gives us access to ALL the inQueue data .
   */

  module.factory('queueGraph', queueGraph);

  // Define the inQueue service:
  queueGraph.$inject = ['$soap', '$timeout'];

  function queueGraph($soap, $timeout) {
    //return
    return {
      getQueueBaseList: getQueueBaseList,
      setUserName: setUserName,
      setWSDLUrl: setWSDLUrl,
      setMethod: setMethod
    };

    $soap.addResponseHandler(function(method) {
      return method + "Response";
    });

    var User = {};
    var url;
    var method;

    function getQueueBaseList() {
      return $soap.post(getWSDLUrl(), getMethod(), getUserName());
    }

    function setMethod(newMethod) {
      method = newMethod;
    }

    function getMethod() {
      return method;
    }

    function setUserName(newUser) {
      User = newUser;
    }

    function setWSDLUrl(newUrl) {
      url = newUrl;
    }

    function getUserName() {
      return {
        User
      };
    }

    function getWSDLUrl() {
      return url;
    }


  }


});
