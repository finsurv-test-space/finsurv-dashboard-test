define(['./transactionData.module', 'angular'], function (module, angular) {

  /*
   This data service gives us access to the current transaction data .
   */

  module.factory('transactionData',transactionData);

  // Define the transactionData service:
  
  function transactionData() {
   
    // Create the return structure:  
    var factory =    {
        getData: getData,
        setData: setData,
        change : true
    };   
    
    var current_transaction = null ;
    
    
    function setData(data)
    {
        current_transaction = data;
    }
    
    function getData()
    {
        return current_transaction;
    }
    
    
    return factory;
  }

});