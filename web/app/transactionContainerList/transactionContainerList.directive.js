define(['require', './transactionContainerList.module'], function (require, module) {
    
    /*
     This directive displays the single row ui-grid for transactions 
     */

    module.directive('trtransactions', transactionContainerListDirective);

    transactionContainerListDirective.$inject = ['queue', 'transactionData', ]


    function transactionContainerListDirective(queue, transactionData) {
        return {
            restrict: 'EA',
            scope: {
                queueName: '=?',
            },
            templateUrl: require.toUrl("./transactionContainerList.html"),
            replace: true,
            controller: ['$scope', 'transactionData', function ($scope, transactionData) {

                $scope.transactionData = transactionData;

                //Define columns for the ui-grid
                $scope.columnDefs = [
                    { field: 'Host', displayName: 'Host', enableCellEdit: false },
                    { field: 'List', displayName: 'List', enableCellEdit: false },
                    { field: 'Flow', displayName: 'Flow', enableCellEdit: false },
                    { field: 'ValueDate', displayName: 'Value Date', enableCellEdit: false },
                    { field: 'Type', displayName: 'Type', enableCellEdit: false },
                    { field: 'Currency', displayName: 'Currency', enableCellEdit: false },
                    { field: 'Amount', displayName: 'Amount', enableCellEdit: false },
                    { field: 'Matched', displayName: 'Matched', cellTemplate: '<input style="vertical-align: middle" type="checkbox" >', enableSorting: false },
                    { field: 'TrnReference', displayName: 'TrnReference', enableCellEdit: false },
                    { field: 'Comment', displayName: 'Comment', enableCellEdit: true }, ];

                $scope.data = [{}]
                
                //Define grid options for the ui-grid
                $scope.trReferenceGridOptions = {     
                   
                    //rowTemplate: rowTemplate(),
                    columnDefs: $scope.columnDefs,
                    enableFiltering: true,
                    //enableRowSelection: true,
                   // enableCellSelection: true,
                    //enableRowHeaderSelection: false,
                    multiSelect: false,
                    minRowsToShow: 1,
                     enableHorizontalScrollbar:0,
                     enableVerticalScrollbar :0,
                };
                $scope.trReferenceGridOptions.data = $scope.data
                
                //Observe the transactionData service's change variable and set data of the bopForm if the variable changes
                $scope.$watch(
                    function () {
                        return $scope.transactionData.change;
                    },
                    function (newvalue, oldValue) {
                        if (newvalue != oldValue) {
                            console.log(transactionData.getData())         
               
                            //create a new  queue object
                            var queuAuthorisationObj = {
                                "Host": " ",
                                "List": " ",
                                "Flow": " ",
                                "ValueDate": " ",
                                "Type": " ",
                                "Currency": "",
                                "Amount": "",
                                "Matched": "",
                                "TrnReference": "",
                                "Comment": " "
                            };
                            queuAuthorisationObj.Host = "Host",
                            queuAuthorisationObj.List = "List",
                            queuAuthorisationObj.Flow = transactionData.getData().Flow
                            queuAuthorisationObj.ValueDate = transactionData.getData().ValueDate
                            queuAuthorisationObj.Type = transactionData.getData().ReportingQualifier
                            queuAuthorisationObj.Currency = transactionData.getData().FlowCurrency
                            queuAuthorisationObj.Amount = transactionData.getData().TotalForeignValue
                            queuAuthorisationObj.Matched = transactionData.getData().IsADLA
                            queuAuthorisationObj.TrnReference = transactionData.getData().TrnReference
                            queuAuthorisationObj.Comment = ""

                            $scope.data[0] = queuAuthorisationObj
                            $scope.trReferenceGridOptions.data = $scope.data

                        }
                    }
                    );

            }]
        }
    }

    return module
})
