/*
  This loads the navigation providers that have been defined.
  The loader dynamically defines the navigationProviders based on the list of navigationProviders that we load.
*/

define(['require', 'angular', 'app', 'navigation-provider-list'], function (require, angular, app, navigationProviderList) {
  'use strict';

  // NOTE: navigation-provider-list is a require module. We will make it an angular constant here too.

  // Define the navigation providers in Angular:
  app.constant('navigationProviderList', navigationProviderList);

  // Keep track of all the navigationProvider promises:
  var navigationProviderPromises = [];

  // Go through each navigationProvider name and queue it up for loading:
  navigationProviderList.forEach(function (navigationProvider)
  {
    // Create the loaded navigationProvider object that we will use:
    var loadedNavigationProvider = {};

    // Get the path to the navigationProvider relative to the navigationProvider loader:
    // NOTE: Since the require config gets set up with a package for each navigationProvider (in main.js), we simply have to ask for the navigationProvider by name (with a prefix) and require will work out the path.
    var navigationProviderPath = 'navigation-providers.' + navigationProvider.name;

    // Save the name for easy access:
    loadedNavigationProvider.name = navigationProvider.name;

    // Save the navigation provider definition:
    loadedNavigationProvider.definition = navigationProvider;

    // Create a promise for the code to be 'required' in:
    loadedNavigationProvider.promise = new Promise(function (resolve, reject)
    {
      // Require in the navigationProvider and all its dependencies:
      //http://stackoverflow.com/questions/17446844/dynamic-require-in-requirejs-getting-module-name-has-not-been-loaded-yet-for-c
      require([navigationProviderPath], function (navigationProviderImplementation)
      {
        // Save the return value from the navigationProvider:
        loadedNavigationProvider.angularModule = navigationProviderImplementation;

        // Save the angular module name for this navigationProvider:
        loadedNavigationProvider.angularModuleName = navigationProviderImplementation.name;

        // Resolve the promise with this data:
        resolve(loadedNavigationProvider);
      }, function (err){
        // There was a problem loading the navigationProvider.
        reject(err);
      });
    });

    // Save the navigationProvider promise in our list:
    navigationProviderPromises.push(loadedNavigationProvider.promise);

  });

  // Return the promise of loading all the navigationProviders:
  // http://pouchdb.com/2015/05/18/we-have-a-problem-with-promises.html
  // Note: We are using the shim for When to provide support for Promise in IE.
  // https://github.com/cujojs/when/blob/master/docs/es6-promise-shim.md
  return Promise.all(navigationProviderPromises)
    .then(function(navigationProviders){
      // Now we have loaded all the navigationProviders.

      // Register all the navigationProviders in angular:
      app.constant('navigationProviders', navigationProviders);

      // Pass on the navigationProviders:
      return navigationProviders;
    });
});