/**
 * Created by daniel on 15/09/17.
 */
define(['settings/settings.module'],function(module){
  module.controller('SettingsMenuController',SettingsMenuController);
  SettingsMenuController.$inject=['$scope','settingsService','$state'];
  function SettingsMenuController($scope,settingsService,$state){
    var currentSelectedSettingsMenu;

    $scope.setActiveMenuItem = function(item){
      if(currentSelectedSettingsMenu){
        currentSelectedSettingsMenu.active=false;
      }
      item.active=true;
      currentSelectedSettingsMenu = item;
    }

    var menuItems = settingsService.getMenu();
    $scope.settingsMenuItems = menuItems;
    //Sort items in the menu items list alphabetically
    menuItems.sort(function(a, b){
      if(a.name < b.name) return -1;
      if(a.name > b.name) return 1;
      return 0;
    });
    //The code below will set the default selected menu item
    $scope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
      //We have to catch this on stateChangeSuccess as we are watching the scope and not the rootscope.
      //So checking on stateChangeStart which is called from an external scope won't register with this function.
      if(toState.name=='settings'){
        //Make sure no current items are set to active;
        menuItems.forEach(function(item){
              item.active=false;
            }
        );
        //Set default selected item;
        $scope.setActiveMenuItem(menuItems[0]);
        $state.go(menuItems[0].state);
      }
      else{
        for(var i=0;i<menuItems.length;i++){
          if(menuItems[i].state == toState.name){
            $scope.setActiveMenuItem( menuItems[i]);
            //Found the item that needs to be selected therefor break out of the for loop.
            break;
          }
        }

      }
    })
  }
  return module;
})