/**
 * Created by daniel on 15/09/16.
 */
define(['angular', 'settings/settings.module'], function (angular, module) {

  settingsConstants= {
    PermissionType:{
      USER: {value: 'User'},
      SCREEN_VIEWER: {value: 'Screen Viewer'},
      USER_MANAGEMENT: {value: 'User Management'},
      EVENT_MANAGEMENT: {value: 'Event Management'},
      FEED_MANAGEMENT: {value: 'Feed Management'},
      FEED_ACTIONS: {value: 'Feed Actions'},
      DATASOURCE_MANAGEMENT: {value: 'Datasource Management'}
    }
  }

  module.constant('settingsConstants', settingsConstants)



  return module;
})