'use strict';

define([
  'require',
  'settings/menu/menu.controller'

], function (require) {

  return {
    name: 'settings',
    url: '/settings',
    //abstract: true,
    parent: 'root',
    //resolve: {
    //},
    data: {
      requireLogin: true
    },
    views: {
      'side-menu@root': {
        templateUrl: require.toUrl('settings/menu/menu.html'),
        controller: 'SettingsMenuController'
      },
      'layout@root': {
        templateUrl: require.toUrl('settings/settings.layout.html')
      }
    }

  }

});


