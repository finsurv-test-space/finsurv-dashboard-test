// Load all the related files for the module:
define(['settings/settings.module',
        'settings/settings.service',
        'settings/settings.constants',
        'settings/settings.state',
        'settings/settings.states',
        'settings/menu/menu.controller'],
    function (module) {
  return module;
});