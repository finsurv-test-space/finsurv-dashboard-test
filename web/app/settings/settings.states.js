define([
  './settings.module',
  './settings.state'
], function (module, dashboardState) {

  // Configure our states at config time:
  module.config(configStates);

  // Define the states for dashboards:
  configStates.$inject = ['$stateProvider'];
  function configStates($stateProvider)
  {
    // Define all the states that we have:
    $stateProvider.state(dashboardState);
  }

});