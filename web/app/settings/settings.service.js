/**
 * Created by daniel on 15/09/16.
 */
define(['angular', 'settings/settings.module'],
    function (angular, module) {
      module.factory('settingsService', settingsService)
      settingsService.$inject = ['user', 'settingsConstants']
      function settingsService(user, settingsConstants) {
        var factory = {}
        var menuItems = [];
        factory.registerMenuItem = registerMenuItem;
        factory.getMenu = getMenu;
        factory.checkSettingsPermissions = checkSettingsPermissions;

        user.subscribe(emptyGetMenuItems);
        function emptyGetMenuItems(user){
          if(!user.loggedIn){
            menuItems = [];
          }
        }

        return factory;

        function checkSettingsPermissions() {
          return user.checkUserPermissions
          (
              [settingsConstants.PermissionType.USER_MANAGEMENT,
                settingsConstants.PermissionType.FEED_MANAGEMENT,
                settingsConstants.PermissionType.DATASOURCE_MANAGEMENT],
              false
          );
        };

        //items is an array containing an object with the name of the menu item to be added
        //and the fontawesome icon to be displayed
        //e.g. {name:'User settings', cssClass:'i-users',description:'User roles mnu',role:['user',admin]}
        function registerMenuItem(items) {
          //Check if items being sent in as an array or a single item
          if (items instanceof Array) {
            items.forEach(function (item) {
              if (menuItems.filter(function(menuItem){return menuItem.name == item.name}).length==0) {
                putItemInSettingsMenu(item);
              }
            })
          }
          else {
            //Items is a single item and not an array.
            if (menuItems.filter(function(menuItem){return menuItem.name == items.name}).length==0) {
              putItemInSettingsMenu(items);
            }
          }
        }

        function putItemInSettingsMenu(item) {
          if (item.permissions) {
            //Subscribe to the user to make sure the user is logged on before checking the  permission.
            user.subscribe(checkPermission)

            //This function is created here because we need to capture the item variable in this callback.
            //If the settings menu item has permission associated it with
            //make sure the current user can view this menu item.
            function checkPermission() {
              if (user.checkUserPermissions(item.permissions)) {
                menuItems.push(item);
              }
              user.unsubscribe(checkPermission);
            }
          }
          else {
            menuItems.push(item);
          }
        }
        function getMenu() {
          return menuItems;
        }
      }
      return module;
    })