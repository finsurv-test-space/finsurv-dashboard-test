define([
      'angular',
      'angular-ui-router',
      'root',
        'dropdown-multiselect'
    ],
    function (angular) {
      var settingsModuleDependencies = ['ui.router','dropdownMultiselect'
      ];
      var module = angular.module('settings', settingsModuleDependencies).run(runBlock);
      runBlock.$inject = ['mainMenuDropdownService','settingsService','$state'];
      function runBlock(mainMenuDropdownService,settingsService,$state) {
        function goToSettings  () {
          $state.go('settings')
        }
        var dropdownSettingsItem = {name: 'Settings', icon: 'fa-gear',show:settingsService.checkSettingsPermissions,click:goToSettings}
        mainMenuDropdownService.registerDropdownItem(dropdownSettingsItem);
      };
      return module;
    });


