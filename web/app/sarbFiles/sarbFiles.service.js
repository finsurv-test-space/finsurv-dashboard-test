define(['./sarbFiles.module', 'angular'], function (module, angular) {



   module.factory('sarbFiles',sarbFiles);

   // Define the sarbFiles service:
   sarbFiles.$inject = ['$http', '$timeout','$soap' ];
   function sarbFiles($http, $timeout, $soap) {
    
    //return
    return {getSARBFiles : getSARBFiles,
            setUserName  : setUserName, 
            setWSDLUrl   : setWSDLUrl, 
            setMethod    : setMethod
    };  
    
    $soap.addResponseHandler(function(method)
    {
    return method + "Response";
    });
    
    var User = {};
    var url  ;
    var method;
    
    function getSARBFiles()
    {
       return  $soap.post(getWSDLUrl(),getMethod(),getUserName());
    }
    
    function setMethod(newMethod)
    {
        method = newMethod;
    }
    
    function getMethod()
    {
        return method;
    }
   
    function setUserName(newUser)
    {
        User = newUser;
    }
    
    function setWSDLUrl(newUrl)
    {
        url  = newUrl;
    }
    
    function getUserName()
    {
        return {User};
    }
    
    function getWSDLUrl()
    {
        return url;
    }      
  }

});