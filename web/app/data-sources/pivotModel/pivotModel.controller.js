define(['angular', './pivotModel.module'], function(angular, module) {

  module.controller('pivotModelDataSourceController', pivotModelController);

  // Define the dependencies:
  pivotModelController.$inject = ['$scope', '$rootScope', '$injector', '$filter', '$interval', '$http'];

  // Define the widget factory:
  function pivotModelController($scope, $rootScope, $injector, $filter, $interval, $http) {

    var dataSourceConfig = $scope.dataSource.config;

    var user = {
      headers: {
        "REMOTE_USER": "admin",
        'Content-Type': 'application/json; charset=utf-8'
      },
      withCredentials: true
    };

    //Initialize the data variable.
    var data;

    $scope.saveChanges = function() {
      //set data according to config - data is an object with a "data" and "query" property
      //The data will be set after the data object Initialized above points to the data received from the server.
      $scope.dashboardScope.setData($scope.dataSource.name,
        data.test == true ? {
          data: dataSourceConfig.sampleData,
          query: dataSourceConfig.query
        } : {
          data: data.data,
          query: dataSourceConfig.query,
        });
    };

    function getAndSetData() {
      $rootScope.pollDateTime = (new Date()).toLocaleString();
      //Make a rest call passing in the query JSON
      $http.post("/finsurv-dashboard/doPost/dashboard/pivotmodel", dataSourceConfig.query, user)
        .then(success, failure)

      function success(response) {
        //On success save the changes
        data = response.data;
        $scope.saveChanges();
      };

      function failure(response) {
        // $scope.saveChanges(); // This needs to be removed when right calls to the backend are made
        console.log(response.data)
      };
    };

    getAndSetData();

    setInterval(function() {
      getAndSetData()
    }, 1800000);

  };
  // Return the constructor function so we can use it for the data source container in the dashboard:
  return pivotModelController;
});
