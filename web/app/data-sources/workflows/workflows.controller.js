define(['angular', './workflows.module'], function(angular, module) {

  module.controller('workflowsDataSourceController', workflowsController);

  // Define the dependencies:
  workflowsController.$inject = ['$scope', '$injector', '$filter', '$interval', '$http'];

  // Define the widget factory:
  function workflowsController($scope, $injector, $filter, $interval, $http) {

    var dataSourceConfig = $scope.dataSource.config;

    //Initialize the data variable.
    var data;

    $scope.saveChanges = function() {
      //set data according to config - data is an object with a "data" property
      //The data will be set after the data object Initialized above points to the data received from the server.
      $scope.dashboardScope.setData($scope.dataSource.name,
        dataSourceConfig.useSampleData == true ? {
          data: dataSourceConfig.sampleData,
        } : {
          data: data,
        });
    };

    function getAndSetData() {
      //Make a rest call
      $http.get("/finsurv-dashboard/doGet/workflows/status")
        .then(success, failure)

      function success(response) {
        //On success save the changes
        data = response.data;
        $scope.saveChanges();
      };

      function failure(response) {
        // $scope.saveChanges(); // This needs to be removed when right calls to the backend are made
        console.log(response.data)
      };
    };

    getAndSetData();

    setInterval(function() {
      getAndSetData()
    }, 15000);

  };
  // Return the constructor function so we can use it for the data source container in the dashboard:
  return workflowsController;
});
