define(['angular', './script.module'], function (angular, module) {

  module.controller('scriptDataSourceController', scriptController);

  // Define the dependencies:
  scriptController.$inject = ['$scope', '$injector'];

  // Define the widget factory:
  function scriptController($scope, $injector)
  {
    //console.log("Hello from script data source controller");

    // The scope has the data source definition.
    console.log($scope.dataSource);

    // Get the config for the data source:
    var dataSourceConfig = $scope.dataSource.config;

    // Make sure we have a source configuration:
    if (!dataSourceConfig.sourceName)
    {
      console.error('No sourceName was specified in the script data source configuration. We need that to know what data to map.')
    }

    // Listen for changes to the data source:
    $scope.dashboardScope.watchData(dataSourceConfig.sourceName, dataChanged, dataSourceConfig.waitForPostLoad === undefined ? false : dataSourceConfig.waitForPostLoad); // Use full object equality implementation.

    /*
    This gets called whenever trade feed data is available or updated.
     */
    function dataChanged(newValue)
    {
      // Initialise to the new value:
      var data = newValue;

      // Transform the data as specified in the config:
      if (dataSourceConfig.mapData)
      {
        // There is a map function in the config.
        try
        {
          // Map the data:
          // NOTE: We pass in the injector so that the mapping code can ask for any other dependencies it has.
          data = dataSourceConfig.mapData(data, $injector);
        }
        catch(err)
        {
          console.error('Error when running the script data source mapData() function.')
          console.error(err);
        }
      }

      // Set the data for the data source specified in the config:
      $scope.dashboardScope.setData($scope.dataSource.name, data);
    }
  }

  // Return the constructor function so we can use it for the data source container in the dashboard:
  return scriptController;
});


