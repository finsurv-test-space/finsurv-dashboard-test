// Declare all the dependencies that need to be required in for this data source:
define([
  './text.module',
  './text.controller'
], function (module) {

  // We simply return the Angular module for this data source:
  return module;
});


