/*
  This loads the data sources that have been defined.
  The loader dynamically defines the dataSources based on the list of dataSources that we load.
*/

define(['require', 'angular', 'app', 'data-source-list'], function (require, angular, app, dataSourceList) {
  'use strict';

  // NOTE: data-source-list is a require module. We will make it an angular constant here too.

  // Define the data sources in Angular:
  app.constant('dataSourceList', dataSourceList);

  // Keep track of all the dataSource promises:
  var dataSourcePromises = [];

  // Go through each dataSource name and queue it up for loading:
  dataSourceList.forEach(function (dataSource)
  {
    // Create the loaded dataSource object that we will use:
    var loadedDataSource = {};

    // Get the path to the dataSource relative to the dataSource loader:
    // NOTE: Since the require config gets set up with a package for each dataSource (in main.js), we simply have to ask for the dataSource by name (with a prefix) and require will work out the path.
    var dataSourcePath = 'data-sources.' + dataSource.name;

    // Save the name for easy access:
    loadedDataSource.name = dataSource.name;

    // Save the data source definition:
    loadedDataSource.definition = dataSource;

    // Create a promise for the code to be 'required' in:
    loadedDataSource.promise = new Promise(function (resolve, reject)
    {
      // Require in the dataSource and all its dependencies:
      //http://stackoverflow.com/questions/17446844/dynamic-require-in-requirejs-getting-module-name-has-not-been-loaded-yet-for-c
      require([dataSourcePath], function (dataSourceImplementation)
      {
        // Save the return value from the dataSource:
        loadedDataSource.angularModule = dataSourceImplementation;

        // Save the angular module name for this dataSource:
        loadedDataSource.angularModuleName = dataSourceImplementation.name;

        // Resolve the promise with this data:
        resolve(loadedDataSource);
      }, function (err){
        // There was a problem loading the dataSource.
        reject(err);
      });
    });

    // Save the dataSource promise in our list:
    dataSourcePromises.push(loadedDataSource.promise);

  });

  // Return the promise of loading all the dataSources:
  // http://pouchdb.com/2015/05/18/we-have-a-problem-with-promises.html
  // Note: We are using the shim for When to provide support for Promise in IE.
  // https://github.com/cujojs/when/blob/master/docs/es6-promise-shim.md
  return Promise.all(dataSourcePromises)
    .then(function(dataSources){
      // Now we have loaded all the dataSources.

      // Register all the dataSources in angular:
      app.constant('dataSources', dataSources);

      // Pass on the dataSources:
      return dataSources;
    });
});