define(['angular', './clock.module'], function (angular, module) {

  module.controller('clockDataSourceController', clockController);

  // Define the dependencies:
  clockController.$inject = ['$scope', '$injector','$filter','$interval'];

  // Define the widget factory:
  function clockController($scope, $injector,$filter,$interval)
  {
     // The scope has the data source definition.
     
    // Get the config for the data source:
    var dataSourceConfig = $scope.dataSource.config;
    $scope.config = dataSourceConfig;
   

var timer ;
  //var data="hello world"
  
  $scope.saveChanges = function()
  {
    $interval.cancel(timer);
    
    timer = $interval(refreshDate,dataSourceConfig.refreshRate);
  }
    
 timer = $interval(refreshDate,dataSourceConfig.refreshRate);
        
 function refreshDate(){ $scope.dashboardScope.setData($scope.dataSource.name, new Date())}
         
   
  }
  // Return the constructor function so we can use it for the data source container in the dashboard:
  return clockController;
});


