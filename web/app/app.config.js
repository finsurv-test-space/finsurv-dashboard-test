'use strict';

/* Controllers */

define(['./app.module', './background','jquery'], function (appModule, background, $) {

  appModule.config(['$locationProvider', '$stateProvider', '$urlRouterProvider', 'defaultState','widgets','brand', function ($locationProvider, $stateProvider, $urlRouterProvider,defaultState,widgets,brand) {
    // we want the pretty urls
    $locationProvider.html5Mode(true);
    
  
  
    /**
     * Because dashboards have dynamically created states, when the user to tries to navigate to them
     * the app will send the user to the default url as it would appear those states do no exist.
     * In order to bypass this extract from the url the state that the user was intending on navigating to
     * and store it in the pendingState variable in the params object of the state.
     * Once the dynamic states are defined we then extract the pendingState variable and direct the user to correct state.
     */
    $urlRouterProvider.otherwise(function ($injector, $location) {
      //Because of the fact that our dashboards have dynamic states we need to do
      //a more complex check for the url path
            
      var path = $location.path()
      var cleanedPath = path;
      //Get rid of trailing /
      if(path.lastIndexOf('/')==path.length-1){
        cleanedPath = path.slice(0,path.length-1);
      }
      var $state = $injector.get('$state');
      //Split the path into an array
      var pathArray = cleanedPath.split('/');
      //The first element in pathArary will an "" because of the leading / therefor must check if the length of the array is greater then 2
      if (pathArray.indexOf('dashboard')!=-1) {
        //Create an empty params object which will hold the value of the state to transition to.
        var params = {};
        if (pathArray.length >2) {
          //Only add parameters to the params object if the array contains more then just dashboard as an element
          //Remove the empty element
          pathArray.splice(0, 2);
          //Create the state name that the user was intending being directed to.
          var pendingState = "/" + pathArray.join('/');
          params.pendingState = pendingState;
        }
        //Please note that in order to use params like this, in the file
        //that we define the dashboard staten we have to add the params attribute.
        $state.go('dashboard', params);
      }
   else{
     $state.go(defaultState);
      }
    });

    /////////////////////
    // Init Background...
    /////////////////////

    $(window).on('resize', function (e) {

   
        background.createBackground({bgElId: 'baseCanvas', seed: 4 ,colors :brand.backGroundColor})
    })

    background.createBackground({bgElId: 'baseCanvas', seed: 4 ,colors : brand.backGroundColor});


   
  }]);

  // override the normal action of $exceptionHandler, to make angular exceptions fail hard when they happen
  appModule.factory('$exceptionHandler', function () {
    return function (exception, cause) {
      exception.message += ' (caused by "' + cause + '")';
      throw exception;
    };
  })
})