/**
 * Created on 2017/02/06.
 */

define(['angular', './delayedEntries.module', 'isHoliday', 'underscore'], function(angular, module, isHoliday, _) {

    module.controller('delayedEntriesWidgetController', delayedEntriesController);

    // Define the dependencies:
    delayedEntriesController.$inject = ['$scope', '$rootScope', '$q', '$http', 'modelService'];

    // Define the widget factory:
    function delayedEntriesController($scope, $rootScope, $q, $http, modelService) {

        $scope.widgetHeader = 'Delayed Entries Widget';
        $scope.isHoliday = isHoliday;

    }

    return delayedEntriesController;
});