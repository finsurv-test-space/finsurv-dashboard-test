// Declare all the widget dependencies that need to be required in for this widget:
define([
  './information.module',
  './information.controller'
], function(module) {

  // We simply return the Angular module for this widget:
  return module;
});
