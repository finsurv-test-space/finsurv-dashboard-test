
define(['require', 'angular', './information.module', 'nvd3Graphs'], function(require, angular, module, nvd3Graphs) {

  module.controller('informationWidgetController', informationChartController);

  // Define the dependencies:
  informationChartController.$inject = ['$scope', '$rootScope', '$q', '$http'];

  // Define the widget factory:
  function informationChartController($scope, $rootScope, $q, $http) {


    $http.get("/finsurv-dashboard/doGet/settings/startup")
        .then(success, failure)

    function success(response){
      $scope.databaseName = response.data.Setting[0].Value;
      $scope.instanceName = response.data.Setting[0].Name;
    };

    function failure(response){
      // $scope.saveChanges(); // This needs to be removed when right calls to the backend are made
      console.log(response.data)
    };

    $scope.pollDateTime = $rootScope.pollDateTime;

  };
  return informationChartController;

});
