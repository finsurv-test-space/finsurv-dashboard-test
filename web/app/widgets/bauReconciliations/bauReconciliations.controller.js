define(['angular', './bauReconciliations.module', 'isHoliday', 'underscore'], function(angular, module, isHoliday, _) {

    module.controller('bauReconciliationsWidgetController', bauReconciliationsController);

    // Define the dependencies:
    bauReconciliationsController.$inject = ['$scope', '$rootScope', '$q', '$http', 'modelService'];

    // Define the widget factory:
    function bauReconciliationsController($scope, $rootScope, $q, $http, modelService) {

        $scope.widgetHeader = 'BAU Recons';
        $scope.isHoliday = isHoliday;

    }

    return bauReconciliationsController;
});