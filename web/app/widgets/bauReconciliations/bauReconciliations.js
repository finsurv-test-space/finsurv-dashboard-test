/**
 * Created on 2017/02/09.
 */
define([
    './bauReconciliations.module',
    './bauReconciliations.controller'
], function(module) {
    // We simply return the Angular module for this widget:
    return module;
});