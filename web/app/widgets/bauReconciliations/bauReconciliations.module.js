/**
 * Created on 2017/02/09.
 */
define(['angular'], function(angular) {
    return angular.module('widgets.bauReconciliations', []);
});

