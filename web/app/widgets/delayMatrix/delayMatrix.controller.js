//Data is over the last two weeks
//Data is represented matrix

define(['angular', './delayMatrix.module', 'isHoliday', 'underscore'], function(angular, module, isHoliday, _) {

  module.controller('delayMatrixWidgetController', delayMatrixChartController);

  // Define the dependencies:
  delayMatrixChartController.$inject = ['$scope', '$rootScope', '$q', '$http', '$uibModal', 'modelService'];

  // Define the widget factory:
  function delayMatrixChartController($scope, $rootScope, $q, $http, $modal, modelService) {


    var widgetConfig = $scope.widget.config;

    function getDate(delay) {
      d = new Date();
      d.setDate(d.getDate() - delay);
      return d.toISOString().substr(0, 10);
    };


    function dataChanged(transactions) //transactions is a object with a data property and the query property
    {
      //Getting the pivot query from the datasouce config
      $scope.pivotQuery = transactions.query;

      $scope.isEmpty = function() {
        return transactions.data.length == 0;
      };

      var baseObject = _.range(14).reduce(function(memo, item, index){
        var obj = {
          date: getDate(index),
          data: _.range(15).map(function(item){
            return {
              delay: String(-1*item), //want the delay as a string for drilldown
              colour: item+1,
              count: 0
            }
          }),
          total: 0
        };
        obj.data.unshift({
          delay: "> 0",
          colour: 0,
          count: 0
        });
        obj.data.push({
          delay: "<= -15",
          colour: 16,
          count: 0
        });
        memo.push(obj);
        return memo;
      },[])


      $scope.vmData = transactions.data.reduce(function(memo, item, index){
        var foundDate = memo.find(function(i){
          return i.date == item.date;
        });
        if (foundDate){
          var foundDelay = foundDate.data.find(function(i){
            return i.delay == item.delay;
          });
          if (foundDelay){
            foundDelay.count = foundDelay.count+parseInt(item.count);
            foundDate.total = foundDate.total+parseInt(item.count);
          };
        };
        return memo;
      }, baseObject);

    }
   

    //This method gets the date, delay  from a click event on the DOM
    // $scope.setModelQuery = function(date, delay, system, count){
    $scope.setModelQuery = function(date, delay, count) {
      //Access the pivot query in this function scope
      var pivotQuery = $scope.pivotQuery;

      //Generate the drill down query from the pivot query and the date, delay and system.
      var modelQuery = {
        model: pivotQuery.model,
        startAt: pivotQuery.startAt,
        maxRecords: 20,
        filter: [],
        orderBy: ["asc('System')"]
      }


      for (var key in pivotQuery.select) {
        if (key == "date") {
          modelQuery.filter.push(pivotQuery.select[key] + ".equals('" + date + "')");
        } else if (key == "delay") {
          modelQuery.filter.push(pivotQuery.select[key] + ".equals('" + delay + "')");
        }
      }


      //Need a user object because there is no Dashboard login
      var user = {
        headers: {
          "REMOTE_USER": "admin"
        }
      };

      open(count, modelQuery, user);

    };

    //Function to open the drill down modal
    // var open = function(data, count, modelQuery, user) {
    var open = function(count, modelQuery, user) {

      var outerScope = $scope;
      var modalInstance = $modal.open({
        templateUrl: require.toUrl(
          //Use this partial for the modal
          'app/widgets/delayMatrix/delayMatrix.modal.html'
        ),
        controller: ["$scope", "$uibModalInstance",
          function($scope, $uibModalInstance) {
            $scope.ok = function() {
              $uibModalInstance.close();
            };

            //Set attach the data to the modal scope
            $scope.data = []; //Uncomment to use live data

            $scope.totalItems = count;

            $scope.currentPage = 1;
            $scope.numPerPage = 20;

            $scope.maxSize = 5;

            $http.post("/finsurv-dashboard/doPost/dashboard/getmodel", JSON.stringify(modelQuery), user)
              .then(success, failure)

            function success(response) {
              //On success get the data and open a modal by passing in data to the open() function
              var res = response.data.data;

              $scope.data = res.map(function(item, index) {
                item["num"] = $scope.currentPage + index;
                return item;
              });

            };

            function failure(response) {
              // open({}); //Needs to be removed when a connection to DB is established
              console.log(response.data);
            };



            $scope.$watch('currentPage + numPerPage', function() {
              $scope.data = [];

              var begin = $scope.currentPage == 1 ? 1 : (($scope.currentPage - 1) * $scope.numPerPage + 1);
              var end = $scope.currentPage == 1 ? $scope.numPerPage : ($scope.currentPage * $scope.numPerPage);

              modelQuery["startAt"] = begin;


              $http.post("/finsurv-dashboard/doPost/dashboard/getmodel", JSON.stringify(modelQuery), user)
                .then(success, failure)

              function success(response) {
                //On success get the data and open a modal by passing in data to the open() function

                var res = response.data.data;

                $scope.data = res.map(function(item, index) {
                  item["num"] = begin + index;
                  return item;
                });

              };

              function failure(response) {
                // open({}); //Needs to be removed when a connection to DB is established
                console.log(response.data);
              };

            });

          }
        ]
      });
    };


    $scope.getColour = function(colour) {
      var hue = 125 - Math.abs((((colour)) / 13) * 125) - 10;
      if (hue < 0) {
        hue = 0;
      }
      return hue;
    };

    //Exposing isHoliday module to the scope
    $scope.isHoliday = isHoliday;

    $scope.dashboardScope.allWidgetsLoadedPromise.then(function() {
      $scope.$on("widgetResized", function(event, updatedWidget) {
        debugger;
        $scope.widgetWidth = updatedWidget.config.width;
        $scope.tableHeight = updatedWidget.config.height - updatedWidget.config.height / 4;
      });
      $scope.dashboardScope.watchData(widgetConfig.dataSourceName, dataChanged, true); //This is true to work around the bug of timing for other widgets
    });

  };
  return delayMatrixChartController;

});
