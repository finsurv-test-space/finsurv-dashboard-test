//This widget represents Banking as Usual


define(['angular', './problemTransactions.module'], function(angular, module) {

  module.controller('problemTransactionsWidgetController', problemTransactionsChartController);

  // Define the dependencies:
  problemTransactionsChartController.$inject = ['$scope', '$rootScope', '$q', '$http', '$uibModal'];

  // Define the widget factory:
  function problemTransactionsChartController($scope, $rootScope, $q, $http, $modal) {




    var widgetConfig = $scope.widget.config;


    function dataChanged(transactions)
    {
      $scope.data = transactions.data.sort(function(a,b){
        if(a.count > b.count) return -1;
        if(a.count < b.count) return 1;
        return 0;
      });

      $scope.pivotQuery = transactions.query;

      $scope.isEmpty = function(){
        return transactions.data.length == 0;
      };

      $scope.totalCount = $scope.data.reduce(function(memo, item){
        memo += parseInt(item.count);
        return memo;
      },0);
    };

    //This method gets the date, delay and system from a click event on the DOM
    $scope.setModelQuery = function(reason, count){
      //Access the pivot query in this function scope
      var pivotQuery = $scope.pivotQuery;

      //Generate the drill down query from the pivot query and the date, delay and system.

      var modelQuery = {
        model: pivotQuery.model,
        startAt: pivotQuery.startAt,
        maxRecords: 20,
        filter: [],
        orderBy: ["desc('reason')"]
      };


      for (var key in pivotQuery.select){
        if (key == "reason"){
          modelQuery.filter.push(pivotQuery.select[key] + ".equals('"+reason+"')");
        }
      }

      console.log(modelQuery);

      //Need a user object because there is no Dashboard login
      var user = {
        headers: {
          "REMOTE_USER": "admin"
        }
      };

      open(count, modelQuery, user);

    };

    //Function to open the drill down modal
    // var open = function(data, count, modelQuery, user) {
    var open = function( count, modelQuery, user) {

      var outerScope = $scope;
      var modalInstance = $modal.open({
        templateUrl: require.toUrl(
          //Use this partial for the modal
          'app/widgets/delayMatrix/delayMatrix.modal.html'
        ),
        controller: ["$scope", "$uibModalInstance",
          function($scope, $uibModalInstance) {
            $scope.ok = function() {
              $uibModalInstance.close();
            };
            // $scope.data = modelService; //Mocked drill down data

            //Set attach the data to the modal scope
            $scope.data = []; //Uncomment to use live data

            $scope.totalItems = count;

            console.log($scope.totalItems);

            $scope.currentPage = 1;
            $scope.numPerPage = 20;

            $scope.maxSize = 5;

            $http.post("/finsurv-dashboard/doPost/dashboard/getmodel", JSON.stringify(modelQuery),  user)
            .then(success, failure)

            function success(response){
              //On success get the data and open a modal by passing in data to the open() function
              var res = response.data.data;

              $scope.data = res.map(function(item, index){
                item["num"] = $scope.currentPage + index;
                return item;
              });

              //Want to show the drill down in sorted order
              $scope.data = $scope.data.map(function(item){
                tempObject = {};
                var temp = Object.keys(item).sort().forEach(function(i){
                  tempObject[i] = item[i];
                });
                item = tempObject;
                return item;
              });


            };

            function failure(response){
              // open({}); //Needs to be removed when a connection to DB is established
              console.log(response.data);
            };



            $scope.$watch('currentPage + numPerPage', function() {
              $scope.data = [];


                var begin = $scope.currentPage == 1 ? 1 : (($scope.currentPage - 1) * $scope.numPerPage +1);
                var end = $scope.currentPage == 1 ? $scope.numPerPage : ($scope.currentPage * $scope.numPerPage);

                modelQuery["startAt"] = begin;


                $http.post("/finsurv-dashboard/doPost/dashboard/getmodel", JSON.stringify(modelQuery),  user)
                .then(success, failure)

                function success(response){
                  //On success get the data and open a modal by passing in data to the open() function

                  var res = response.data.data;

                  $scope.data = res.map(function(item, index){
                    item["num"] = begin + index ;
                    return item;
                  });

                  //Want to show the drill down in sorted order
                  $scope.data = $scope.data.map(function(item){
                    tempObject = {};
                    var temp = Object.keys(item).sort().forEach(function(i){
                      tempObject[i] = item[i];
                    });
                    item = tempObject;
                    return item;
                  });

                };

                function failure(response){
                  // open({}); //Needs to be removed when a connection to DB is established
                  console.log(response.data);
                };

              });

          }
        ]
      });
    };



    $scope.dashboardScope.allWidgetsLoadedPromise.then(function(){
      $scope.$on("widgetResized",function(event,updatedWidget){
          //Setting width and height of the graph according to height and width of the widget
          //Important for resizings
          //since the px value is taken, the heights and widths need to be reduced
          $scope.tableWidth = updatedWidget.config.width;
          $scope.tableHeight = updatedWidget.config.height - updatedWidget.config.height/4;
      });
      $scope.dashboardScope.watchData(widgetConfig.dataSourceName, dataChanged, true);

    });


  };
  return problemTransactionsChartController;

});
