//This widget shows how much time it took for files to get pieChart
//Data is represented on current state on the entire system
//Data is represented on a pie graph


define(['angular', './pieChart.module', 'nvd3Graphs'], function (angular, module, nvd3Graphs) {

    module.controller('pieChartWidgetController', pieChartChartController);

    // Define the dependencies:
    pieChartChartController.$inject = ['$scope', '$http', '$uibModal'];

    // Define the widget factory:
    function pieChartChartController($scope, $http, $modal) {

        var widgetConfig = $scope.widget.config;

        function dataChanged(transactions) {
                  
            $scope.pivotQuery = transactions.query;

            var baseObj = [
                {
                    key: "< 2 days",
                    y: 0
                }, {
                    key: "< 3 days",
                    y: 0
                },
                {
                    key: ">= 3 days",
                    y: 0
                }
            ];

            $scope.isEmpty = function () {
                return transactions.data.length == 0;
            };

            $scope.data = transactions.data.reduce(function (memo, item, index) {

                var foundKey = memo.find(function (i) {
                    return i.key == item.state;
                });

                if (foundKey) {
                    foundKey.y = parseInt(item.count);
                }

                return memo;
            }, baseObj);

            $scope.pieChart.chart.callback = function(chart){
                chart.pie.dispatch.on('elementClick', function(e){

                    var count = parseInt(e.data.y);
                    var key = e.data.key;

                    $scope.modelQuery = {
                        model: $scope.pivotQuery.model,
                        startAt: $scope.pivotQuery.startAt,
                        maxRecords: 20,
                        filter: [],
                        orderBy: [
                            "asc('SARBFileID')"
                        ]
                    };

                    $scope.modelQuery.filter.push(
                        "field('WaitingInSubmitted').equals('"+key+"')"
                    );

                    var user = {
                        headers: {
                            "REMOTE_USER": "admin"
                        }
                    };

                    open(count, $scope.modelQuery, user);
                })
            }

        }

        var open = function (count, modelQuery, user) {

            var outerScope = $scope;
            var modalInstance = $modal.open({
                templateUrl: require.toUrl(
                    //Use this partial for the modal
                    'app/widgets/pieChart/pieChart.modal.html'
                ),
                controller: ["$scope", "$uibModalInstance",
                function ($scope, $uibModalInstance) {

                    $scope.ok = function () {
                    $uibModalInstance.close();
                    };
                    // $scope.data = modelService; //Mocked drill down data

                    //Set attach the data to the modal scope
                    // $scope.data = []; //Uncomment to use live data

                    $scope.totalItems = count;

                    $scope.currentPage = 1;
                    $scope.numPerPage = 20;

                    $scope.maxSize = 5;


                    $http.post("/finsurv-dashboard/doPost/dashboard/getmodel", JSON.stringify(modelQuery), user)
                        .then(success, failure)

                    function success(response) {
                    //On success get the data and open a modal by passing in data to the open() function
                    var res = response.data.data;

                    $scope.data = res.map(function (item, index) {
                        item["num"] = $scope.currentPage + index;
                        return item;
                    });
                    }

                    function failure(response) {
                    // open({}); //Needs to be removed when a connection to DB is established
                    console.log(response.data);
                    };

                    $scope.$watch('currentPage + numPerPage', function () {
                    $scope.data = [];


                    var begin = $scope.currentPage == 1 ? 1 : (($scope.currentPage - 1) * $scope.numPerPage + 1);
                    var end = $scope.currentPage == 1 ? $scope.numPerPage : ($scope.currentPage * $scope.numPerPage);

                    modelQuery["startAt"] = begin;


                    $http.post("/finsurv-dashboard/doPost/dashboard/getmodel", JSON.stringify(modelQuery), user)
                        .then(success, failure)

                    function success(response) {
                        //On success get the data and open a modal by passing in data to the open() function

                        var res = response.data.data;

                        $scope.data = res.map(function (item, index) {
                        item["num"] = begin + index;
                        return item;
                        });

                    }

                    function failure(response) {
                        console.log(response.data);
                    }

                  });

                }
              ]
            });
        };


        $scope.dashboardScope.allWidgetsLoadedPromise.then(function () {

            //Using the nvd3 pie chart
            $scope.pieChart = nvd3Graphs.pieChart();
            //Setting color to the chart.color property of the pie chart
            // $scope.pieChart.chart.color = ["#84FF49", "#52D017", "green", "orange", "#980000", "#700000"];
            $scope.pieChart.chart.color = ["green", "orange", "#700000"];

            $scope.pieChart.chart.valueFormat = function (d) {
                return d3.format('d')(d);
            };

            $scope.dashboardScope.watchData(widgetConfig.dataSourceName, dataChanged, true);

        });

    }

    return pieChartChartController;

});
