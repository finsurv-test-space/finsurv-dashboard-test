define(function(){

  // http://www.webcal.fi/en-ZA/other_file_formats.php
    var publicHolidays = [
      {
        "date": "2016-01-01",
        "name": "New Year's Day"
      },
      {
        "date": "2016-03-21",
        "name": "Human Rights Day"
      },
      {
        "date": "2016-03-25",
        "name": "Good Friday"
      },
      {
        "date": "2016-03-27",
        "name": "Easter"
      },
      {
        "date": "2016-03-28",
        "name": "Family Day"
      },
      {
        "date": "2016-04-27",
        "name": "Freedom Day"
      },
      {
        "date": "2016-05-01",
        "name": "Workers' Day"
      },
      {
        "date": "2016-05-02",
        "name": "Public holiday (Workers' Day)"
      },
      {
        "date": "2016-06-16",
        "name": "Youth Day"
      },
      {
        "date": "2016-08-09",
        "name": "National Women's Day"
      },
      {
        "date": "2016-09-24",
        "name": "Heritage Day"
      },
      {
        "date": "2016-12-16",
        "name": "Day of Reconciliation"
      },
      {
        "date": "2016-12-25",
        "name": "Christmas Day"
      },
      {
        "date": "2016-12-26",
        "name": "Day of Goodwill"
      },
      {
        "date": "2016-12-27",
        "name": "Public holiday (Christmas Day)"
      },
      {
        "date": "2017-01-01",
        "name": "New Year's Day"
      },
      {
        "date": "2017-01-02",
        "name": "Public holiday (New Year's Day)"
      },
      {
        "date": "2017-03-21",
        "name": "Human Rights Day"
      },
      {
        "date": "2017-04-14",
        "name": "Good Friday"
      },
      {
        "date": "2017-04-16",
        "name": "Easter"
      },
      {
        "date": "2017-04-17",
        "name": "Family Day"
      },
      {
        "date": "2017-04-27",
        "name": "Freedom Day"
      },
      {
        "date": "2017-05-01",
        "name": "Workers' Day"
      },
      {
        "date": "2017-06-16",
        "name": "Youth Day"
      },
      {
        "date": "2017-08-09",
        "name": "National Women's Day"
      },
      {
        "date": "2017-09-24",
        "name": "Heritage Day"
      },
      {
        "date": "2017-09-25",
        "name": "Public holiday (Heritage Day)"
      },
      {
        "date": "2017-12-16",
        "name": "Day of Reconciliation"
      },
      {
        "date": "2017-12-25",
        "name": "Christmas Day"
      },
      {
        "date": "2017-12-26",
        "name": "Day of Goodwill"
      },
      {
        "date": "2018-01-01",
        "name": "New Year's Day"
      },
      {
        "date": "2018-03-21",
        "name": "Human Rights Day"
      },
      {
        "date": "2018-03-30",
        "name": "Good Friday"
      },
      {
        "date": "2018-04-01",
        "name": "Easter"
      },
      {
        "date": "2018-04-02",
        "name": "Family Day"
      },
      {
        "date": "2018-04-27",
        "name": "Freedom Day"
      },
      {
        "date": "2018-05-01",
        "name": "Workers' Day"
      },
      {
        "date": "2018-06-16",
        "name": "Youth Day"
      },
      {
        "date": "2018-08-09",
        "name": "National Women's Day"
      },
      {
        "date": "2018-09-24",
        "name": "Heritage Day"
      },
      {
        "date": "2018-12-16",
        "name": "Day of Reconciliation"
      },
      {
        "date": "2018-12-17",
        "name": "Public holiday (Day of Reconciliation)"
      },
      {
        "date": "2018-12-25",
        "name": "Christmas Day"
      },
      {
        "date": "2018-12-26",
        "name": "Day of Goodwill"
      },
      {
        "date": "2019-01-01",
        "name": "New Year's Day"
      },
      {
        "date": "2019-03-21",
        "name": "Human Rights Day"
      },
      {
        "date": "2019-04-19",
        "name": "Good Friday"
      },
      {
        "date": "2019-04-21",
        "name": "Easter"
      },
      {
        "date": "2019-04-22",
        "name": "Family Day"
      },
      {
        "date": "2019-04-27",
        "name": "Freedom Day"
      },
      {
        "date": "2019-05-01",
        "name": "Workers' Day"
      },
      {
        "date": "2019-06-16",
        "name": "Youth Day"
      },
      {
        "date": "2019-06-17",
        "name": "Public holiday (Youth Day)"
      },
      {
        "date": "2019-08-09",
        "name": "National Women's Day"
      },
      {
        "date": "2019-09-24",
        "name": "Heritage Day"
      },
      {
        "date": "2019-12-16",
        "name": "Day of Reconciliation"
      },
      {
        "date": "2019-12-25",
        "name": "Christmas Day"
      },
      {
        "date": "2019-12-26",
        "name": "Day of Goodwill"
      },
      {
        "date": "2020-01-01",
        "name": "New Year's Day"
      },
      {
        "date": "2020-03-21",
        "name": "Human Rights Day"
      },
      {
        "date": "2020-04-10",
        "name": "Good Friday"
      },
      {
        "date": "2020-04-12",
        "name": "Easter"
      },
      {
        "date": "2020-04-13",
        "name": "Family Day"
      },
      {
        "date": "2020-04-27",
        "name": "Freedom Day"
      },
      {
        "date": "2020-05-01",
        "name": "Workers' Day"
      },
      {
        "date": "2020-06-16",
        "name": "Youth Day"
      },
      {
        "date": "2020-08-09",
        "name": "National Women's Day"
      },
      {
        "date": "2020-08-10",
        "name": "Public holiday (National Women's Day)"
      },
      {
        "date": "2020-09-24",
        "name": "Heritage Day"
      },
      {
        "date": "2020-12-16",
        "name": "Day of Reconciliation"
      },
      {
        "date": "2020-12-25",
        "name": "Christmas Day"
      },
      {
        "date": "2020-12-26",
        "name": "Day of Goodwill"
      },
      {
        "date": "2021-01-01",
        "name": "New Year's Day"
      },
      {
        "date": "2021-03-21",
        "name": "Human Rights Day"
      },
      {
        "date": "2021-03-22",
        "name": "Public holiday (Human Rights Day)"
      },
      {
        "date": "2021-04-02",
        "name": "Good Friday"
      },
      {
        "date": "2021-04-04",
        "name": "Easter"
      },
      {
        "date": "2021-04-05",
        "name": "Family Day"
      },
      {
        "date": "2021-04-27",
        "name": "Freedom Day"
      },
      {
        "date": "2021-05-01",
        "name": "Workers' Day"
      },
      {
        "date": "2021-06-16",
        "name": "Youth Day"
      },
      {
        "date": "2021-08-09",
        "name": "National Women's Day"
      },
      {
        "date": "2021-09-24",
        "name": "Heritage Day"
      },
      {
        "date": "2021-12-16",
        "name": "Day of Reconciliation"
      },
      {
        "date": "2021-12-25",
        "name": "Christmas Day"
      },
      {
        "date": "2021-12-26",
        "name": "Day of Goodwill"
      },
      {
        "date": "2021-12-27",
        "name": "Public holiday (Day of Goodwill)"
      }
    ];

    var isWeekend = function(d){
      var date = new Date(d);
      return date.getDay() == 6 || date.getDay() == 0 ? true : false;
    };

    var isPublicHoliday = function(d){
      return publicHolidays.find(function(i){
        return i.date == d;
      });
    };

     function isHoliday(d){
       return isWeekend(d) || isPublicHoliday(d) ? true : false;
    };

    return {
      isHoliday: isHoliday
    }
})
