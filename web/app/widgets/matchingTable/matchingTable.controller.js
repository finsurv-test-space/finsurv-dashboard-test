//This widget represents Banking as Usual


define(['angular', './matchingTable.module'], function (angular, module) {

    module.controller('matchingTableWidgetController', matchingTableChartController);

    // Define the dependencies:
    matchingTableChartController.$inject = ['$scope', '$rootScope', '$q', '$http', '$uibModal'];

    // Define the widget factory:
    function matchingTableChartController($scope, $rootScope, $q, $http, $modal) {

        var widgetConfig = $scope.widget.config;

        function dataChanged(transactions) {

            var transData = transactions.data;

            $scope.data = transData.reduce(function(accum, item) {

                var matchConfidence = function(elem) {
                    return elem.confidence == item.confidence && elem.matchingRule == item.matchingRule;
                };

                var tempIndex = accum.findIndex(matchConfidence);

                if(tempIndex >= 0) {
                    if(parseInt(item.yesterdayCount) > 0) {
                        accum[tempIndex].yesterdayCount = parseInt(item.count) / parseInt(item.yesterdayCount)
                    }

                    else if(parseInt(item.lastWeekCount) > 0) {
                        accum[tempIndex].lastWeekCount = parseInt(item.count) / parseInt(item.lastWeekCount)
                    }

                    else if(parseInt(item.beforeCount) > 0) {
                        accum[tempIndex].beforeCount = parseInt(item.count) / parseInt(item.beforeCount)
                    }

                    else {
                        accum[tempIndex].lastMonthCount = parseInt(item.count) / parseInt(item.lastMonthCount)
                    }
                }
                else {
                    accum.push({
                        matchingRule: item.matchingRule,
                        confidence: Number(item.confidence),
                        yesterdayCount: parseInt(item.count) * parseInt(item.yesterdayCount),
                        lastWeekCount: parseInt(item.count) * parseInt(item.lastWeekCount),
                        lastMonthCount: parseInt(item.count) * parseInt(item.lastMonthCount),
                        beforeCount: parseInt(item.count) * parseInt(item.beforeCount)    
                    });
                }

                return accum;

            }, []);

            $scope.pivotQuery = transactions.query;

            $scope.isEmpty = function () {
                return transactions.data.length == 0;
            };

            $scope.totals = $scope.data.reduce(function (accum, item) {

                accum['yesterday'] += item.yesterdayCount;
                accum['lastWeek'] += item.lastWeekCount;
                accum['lastMonth'] += item.lastMonthCount;
                accum['before'] += item.beforeCount;

                return accum;
            }, {yesterday: 0, lastWeek: 0, lastMonth: 0, before: 0});

            $scope.getTableHeight = function() {
                var rowHeight = 30; // your row height
                var headerHeight = 30; // your header height
                return {
                    height: ($scope.data.length * rowHeight + headerHeight) + "px"
                };
            };

            $scope.gridOptions.data = $scope.data;

        };

        $scope.gridOptions = {
            multiSelect: false,
            enableSorting: true,
            enableCellSelection: true,
            enableRowSelection: false, 
            enableRowHeaderSelection: false,
            rowHeight: 30,
            columnDefs: [
                {field: "matchingRule", displayName: "Matching Rule", enableColumnResizing: true , enableHiding: false},
                {field: "confidence", displayName: "Confidence", enableColumnResizing: true, enableHiding: false, cellTemplate: '<div class="ui-grid-cell-contents" > {{COL_FIELD.toFixed(2)*100 + "%" CUSTOM_FILTERS}} </div>'},
                {field: "yesterdayCount", displayName: "Yesterday Count", enableColumnResizing: true, enableHiding: false, cellTemplate: '<div> <div ng-click="grid.appScope.cellClicked(row,col)" class="ui-grid-cell-contents" title="TOOLTIP">{{COL_FIELD CUSTOM_FILTERS}}</div> </div>'},
                {field: "lastWeekCount", displayName: "Last Week Count", enableColumnResizing: true, enableHiding: false, cellTemplate: '<div> <div ng-click="grid.appScope.cellClicked(row,col)" class="ui-grid-cell-contents" title="TOOLTIP">{{COL_FIELD CUSTOM_FILTERS}}</div> </div>'},
                {field: "lastMonthCount", displayName: "Last Month Count", enableColumnResizing: true, enableHiding: false, cellTemplate: '<div> <div ng-click="grid.appScope.cellClicked(row,col)" class="ui-grid-cell-contents" title="TOOLTIP">{{COL_FIELD CUSTOM_FILTERS}}</div> </div>'},                
                {field: "beforeCount", displayName: "Before Last Month Count", enableColumnResizing: true, enableHiding: false, cellTemplate: '<div> <div ng-click="grid.appScope.cellClicked(row,col)" class="ui-grid-cell-contents" title="TOOLTIP">{{COL_FIELD CUSTOM_FILTERS}}</div> </div>'},                
            ]
        };


        $scope.cellClicked = function (row, col){
            var matchingRule = row.entity.matchingRule;
            var confidence = row.entity.confidence;
            var selectKey = col.name;
            var count = row.entity[selectKey];
            $scope.setModelQuery(matchingRule, confidence, selectKey, count);
        };

        //This method gets the date, delay and system from a click event on the DOM
        // $scope.setModelQuery = function(date, delay, system, count){
        $scope.setModelQuery = function (matchingRule, confidence, selectKey, count) {

            // Do not generate a drill down if count is zero
            if(count == 0) return;

            //Access the pivot query in this function scope
            var pivotQuery = $scope.pivotQuery;

            //Generate the drill down query from the pivot query and the date, delay and system.

            var modelQuery = {
                model: pivotQuery.model,
                startAt: pivotQuery.startAt,
                maxRecords: 20,
                filter: [],
                orderBy: ["desc('Confidence')"]
            };

            for (var key in pivotQuery.select) {
                if (key == "matchingRule") {
                    modelQuery.filter.push(pivotQuery.select[key] + ".equals('" + matchingRule + "')");
                } else if (key == "confidence") {
                    modelQuery.filter.push(pivotQuery.select[key] + ".equals(" + confidence + ")");
                }
            }
            modelQuery.filter.push(pivotQuery.select[selectKey] + ".equals(1)");

            //Need a user object because there is no Dashboard login
            var user = {
                headers: {
                    "REMOTE_USER": "admin"
                }
            };

            open(count, modelQuery, user);

        };

        //Function to open the drill down modal
        // var open = function(data, count, modelQuery, user) {
        var open = function (count, modelQuery, user) {

            var outerScope = $scope;
            var modalInstance = $modal.open({
                templateUrl: require.toUrl(
                    //Use this partial for the modal
                    'app/widgets/delayMatrix/delayMatrix.modal.html'
                ),
                controller: ["$scope", "$uibModalInstance",
                    function ($scope, $uibModalInstance) {
                        $scope.ok = function () {
                            $uibModalInstance.close();
                        };
                        // $scope.data = modelService; //Mocked drill down data

                        //Set attach the data to the modal scope
                        $scope.data = []; //Uncomment to use live data

                        $scope.totalItems = count;

                        $scope.currentPage = 1;
                        $scope.numPerPage = 20;

                        $scope.maxSize = 5;

                        $http.post("/finsurv-dashboard/doPost/dashboard/getmodel", JSON.stringify(modelQuery), user)
                            .then(success, failure)

                        function success(response) {
                            //On success get the data and open a modal by passing in data to the open() function
                            var res = response.data.data;

                            $scope.data = res.map(function (item, index) {
                                item["num"] = $scope.currentPage + index;
                                return item;
                            });

                            //Want to show the drill down in sorted order
                            $scope.data = $scope.data.map(function (item) {
                                tempObject = {};
                                var temp = Object.keys(item).sort().forEach(function (i) {
                                    tempObject[i] = item[i];
                                });
                                item = tempObject;
                                return item;
                            });


                        };

                        function failure(response) {
                            // open({}); //Needs to be removed when a connection to DB is established
                            console.log(response.data);
                        };


                        $scope.$watch('currentPage + numPerPage', function () {
                            $scope.data = [];


                            var begin = $scope.currentPage == 1 ? 1 : (($scope.currentPage - 1) * $scope.numPerPage + 1);
                            var end = $scope.currentPage == 1 ? $scope.numPerPage : ($scope.currentPage * $scope.numPerPage);

                            modelQuery["startAt"] = begin;


                            $http.post("/finsurv-dashboard/doPost/dashboard/getmodel", JSON.stringify(modelQuery), user)
                                .then(success, failure)

                            function success(response) {
                                //On success get the data and open a modal by passing in data to the open() function

                                var res = response.data.data;

                                $scope.data = res.map(function (item, index) {
                                    item["num"] = begin + index;
                                    return item;
                                });

                                //Want to show the drill down in sorted order
                                $scope.data = $scope.data.map(function (item) {
                                    tempObject = {};
                                    var temp = Object.keys(item).sort().forEach(function (i) {
                                        tempObject[i] = item[i];
                                    });
                                    item = tempObject;
                                    return item;
                                });

                            };

                            function failure(response) {
                                // open({}); //Needs to be removed when a connection to DB is established
                                console.log(response.data);
                            };

                        });

                    }
                ]
            });
        };


        $scope.dashboardScope.allWidgetsLoadedPromise.then(function () {
            $scope.dashboardScope.watchData(widgetConfig.dataSourceName, dataChanged, true);
        });


    };
    return matchingTableChartController;

});
