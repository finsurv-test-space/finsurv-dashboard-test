define(['angular'], function(angular) {
  return angular.module('widgets.matchingTable', [    
    'ui.grid.edit', 
    'ui.grid',
    'ui.grid.rowEdit', 
    'ui.grid.selection',
    'ui.grid.expandable',
    'ui.grid.autoResize',
    'ui.grid.resizeColumns'  
  ]);
});
