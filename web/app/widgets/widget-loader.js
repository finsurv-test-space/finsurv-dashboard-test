/*
 This loads the widget providers for each widget that has been defined.
 The widget loader dynamically defines the widgetProviders based on the list of widgets that we load.
 */

define(['require', 'angular', 'app', 'widget-list'], function (require, angular, app, widgetList)
{
  'use strict';

  // NOTE: widget-list is a require module. We will make it an angular constant here too.

  // Define the widgets in Angular:
  app.constant('widgetList', widgetList);

  // Keep track of all the widget promises:
  var widgetPromises = [];

  // Go through each widget name and queue it up for loading:
  widgetList.forEach(function (widget)
  {
    // Create the loaded widget object that we will use:
    var loadedWidget = {};

    // Get the path to the widget relative to the widget loader:
    // NOTE: Since the require config gets set up with a package for each widget (in main.js), we simply have to ask for the widget by name (with a prefix) and require will work out the path.
    var widgetPath = 'widgets.' + widget.name;

    // Define the module name for this widget:
    loadedWidget.angularModuleName = 'widgets.' + widget.name;

    // Create a promise for the code to be 'required' in:
    loadedWidget.promise = new Promise(function (resolve, reject)
    {
      // Require in the widget and all its dependencies:
      //http://stackoverflow.com/questions/17446844/dynamic-require-in-requirejs-getting-module-name-has-not-been-loaded-yet-for-c
      require([widgetPath], function (widgetImplementation)
      {
        // Save the return value from the widget:
        loadedWidget.implementation = widgetImplementation;

        // Resolve the promise with this data:
        resolve(loadedWidget);
      }, function (err)
      {
        // There was a problem loading the widget.
        reject(err);
      });
    });

    // Save the widget promise in our list:
    widgetPromises.push(loadedWidget.promise);

  });

  // Return the promise of loading all the widgets:
  // http://pouchdb.com/2015/05/18/we-have-a-problem-with-promises.html
  // Note: We are using the shim for When to provide support for Promise in IE.
  // https://github.com/cujojs/when/blob/master/docs/es6-promise-shim.md
  return Promise.all(widgetPromises)
    .then(function (widgets)
    {
      // Now we have loaded all the widgets.

      // Register all the widgets in angular:
      app.constant('widgets', widgets);

      // Pass on the widgets:
      return widgets;
    });
});