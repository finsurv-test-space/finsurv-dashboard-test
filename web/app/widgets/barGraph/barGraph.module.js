define(['angular'], function(angular) {
  return angular.module('widgets.barGraph', ['nvd3']);
});
