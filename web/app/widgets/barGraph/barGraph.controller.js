define(['angular', './barGraph.module', 'nvd3Graphs', 'underscore'], function (angular, module, nvd3Graphs, _) {

  module.controller('barGraphWidgetController', barGraphChartController);

  // Define the dependencies:
  barGraphChartController.$inject = ['$scope', '$rootScope', '$q', '$http', '$uibModal', '$window', '$timeout'];

  // Define the widget factory:
  function barGraphChartController($scope, $rootScope, $q, $http, $modal, $window, $timeout) {

    var widgetConfig = $scope.widget.config;

    function dataChanged(transactions) {

      $scope.pivotQuery = transactions.query;

      if ($scope.widget.name == "Count of Rejected Bounces from Regulator" || $scope.widget.name == "Count of Replacement Bounces") {

        $scope.isEmpty = function () {
          return transactions.data.length == 0;
        }

        $scope.noDataAvailableDescription = "No Transaction Bounces";

        $scope.xAxisData = ['2', '3', '4', '5', '6', '7', '>7'];

        var baseObj = $scope.xAxisData.reduce(function (memo, item) {
          memo.push({
            x: item,
            y: 0
          });
          return memo;
        }, []);

        var mappedData = transactions.data.reduce(function (memo, item) {

          var foundBucket = memo.find(function (i) {
            return i.x == item.bucket;
          });

          if (foundBucket) {
            foundBucket.y = item.count;
          }


          return memo;
        }, baseObj);


        $scope.data = [{
          key: "Counts",
          values: mappedData.reduce(function (memo, item, index) {
            memo.push({
              x: index,
              y: parseInt(item.y)
            })
            return memo;
          }, [])
        }]

        $scope.multiBarChart.chart.xAxis.tickFormat = function (d) {
          return $scope.xAxisData[d];
        };
        $scope.widget.name == "Count of Rejected Bounces from Regulator" ? $scope.multiBarChart.chart.xAxis.axisLabel = "Number of Times Rejected" : $scope.multiBarChart.chart.xAxis.axisLabel = "Number of Times Replaced";
        $scope.multiBarChart.chart.yAxis.axisLabel = "Transaction Count";

        $scope.multiBarChart.chart.yAxis.tickFormat = function (d) {
          return d3.format('d')(d);
        };

        $scope.multiBarChart.chart.callback = function (chart) {
          chart.multibar.dispatch.on('elementClick', function (e) {
            
            var count = parseInt(e.data.y);
            var date = $scope.xAxisData[e.data.x];

            $scope.modelQuery = {
              model: $scope.pivotQuery.model,
              startAt: $scope.pivotQuery.startAt,
              maxRecords: 20,
              filter: [],
              orderBy: [
                "asc('System')",
                "asc('TrnReference')"
              ]
            };

            for (var key in $scope.pivotQuery.select) {
              if (key == "date") {
                $scope.modelQuery.filter.push($scope.pivotQuery.select[key] + ".equals('" + date + "')"); //need to set this date from the arr
              } else if (key == "bucket") {
                $scope.modelQuery.filter.push($scope.pivotQuery.select[key] + ".equals('" + date + "')"); //need to set this date from the arr
              }
            }

            var user = {
              headers: {
                "REMOTE_USER": "admin"
              }
            };

            open(count, $scope.modelQuery, user);
          });
        };

      }
      else {

        $scope.noDataAvailableDescription = "No File Statuses Available";

        var data = [];
        
        if ($scope.widget.name == "File Status"){
          data = _.filter(transactions.data, function(item){
            return (item.state == "New") || (item.state == "File Submitted") || (item.state == "File Processed Successfully"); 
          });
        }else{
          data = transactions.data;
        }

        $scope.isEmpty = function () {
          return data.length == 0;
        }

        $scope.xAxisData = ["1","2","3","4","5","6","7",">7"];

        var baseObj = data.reduce(function (memo, item, index) {
          var foundState = memo.find(function (i) {
            return i.key == item.state;
          });

          if (!foundState) {
            foundState = {
              key: item.state,
              values: _.range(8).reduce(function (memo2, item2, index2) {
                memo2.push({
                  x: index2,
                  y: 0
                });
                return memo2;
              }, [])
            };

            memo.push(foundState);
          }

          return memo;

        }, []);

        $scope.data = data.reduce(function (memo, item, index) {

          var foundState = memo.find(function (i) {
            return i.key == item.state;
          });

          if (foundState) {
            var foundAge = foundState.values.find(function (i) {
              return i.x == $scope.xAxisData.indexOf(item.age);
            });
            if (foundAge) {
              foundAge.y = parseInt(item.count);
            }
          }
          return memo;
        }, baseObj);

        $scope.multiBarChart.chart.xAxis.tickFormat = function (d) {
          return $scope.xAxisData[d];
        };

         $scope.multiBarChart.chart.callback = function (chart) {
            chart.multibar.dispatch.on('elementClick', function (e) {

              var state = e.data.key;              
              var count = parseInt(e.data.y);
              var age = $scope.xAxisData[e.data.x];

              $scope.modelQuery = {
                model: $scope.pivotQuery.model,
                startAt: $scope.pivotQuery.startAt,
                maxRecords: 20,
                filter: [],
                orderBy: [
                  "asc('SARBFileID')"
                ]
              };

              for (var key in $scope.pivotQuery.select) {
                if (key == "state") {
                  $scope.modelQuery.filter.push($scope.pivotQuery.select[key] + ".equals('" + state + "')"); //need to set this date from the arr
                } else if (key == "age") {
                  $scope.modelQuery.filter.push($scope.pivotQuery.select[key] + ".equals('" + age + "')"); //need to set this date from the arr
                }
              }

              var user = {
                headers: {
                  "REMOTE_USER": "admin"
                }
              };

              open(count, $scope.modelQuery, user);
            });
          };

      }

    };


    var open = function (count, modelQuery, user) {

      var outerScope = $scope;
      var modalInstance = $modal.open({
        templateUrl: require.toUrl(
            //Use this partial for the modal
            'app/widgets/barGraph/barGraph.modal.html'
        ),
        controller: ["$scope", "$uibModalInstance",
          function ($scope, $uibModalInstance) {

            $scope.ok = function () {
              $uibModalInstance.close();
            };
            // $scope.data = modelService; //Mocked drill down data

            //Set attach the data to the modal scope
            // $scope.data = []; //Uncomment to use live data

            $scope.totalItems = count;

            console.log($scope.totalItems);

            $scope.currentPage = 1;
            $scope.numPerPage = 20;

            $scope.maxSize = 5;


            $http.post("/finsurv-dashboard/doPost/dashboard/getmodel", JSON.stringify(modelQuery), user)
                .then(success, failure)

            function success(response) {
              //On success get the data and open a modal by passing in data to the open() function
              var res = response.data.data;

              $scope.data = res.map(function (item, index) {
                item["num"] = $scope.currentPage + index;
                return item;
              });
            }

            function failure(response) {
              // open({}); //Needs to be removed when a connection to DB is established
              console.log(response.data);
            };

            $scope.$watch('currentPage + numPerPage', function () {
              $scope.data = [];


              var begin = $scope.currentPage == 1 ? 1 : (($scope.currentPage - 1) * $scope.numPerPage + 1);
              var end = $scope.currentPage == 1 ? $scope.numPerPage : ($scope.currentPage * $scope.numPerPage);

              modelQuery["startAt"] = begin;


              $http.post("/finsurv-dashboard/doPost/dashboard/getmodel", JSON.stringify(modelQuery), user)
                  .then(success, failure)

              function success(response) {
                //On success get the data and open a modal by passing in data to the open() function

                var res = response.data.data;

                $scope.data = res.map(function (item, index) {
                  item["num"] = begin + index;
                  return item;
                });

              }

              function failure(response) {
                console.log(response.data);
              }

            });

          }
        ]
      });
    };

    $scope.dashboardScope.allWidgetsLoadedPromise.then(function () {

      //Using the nvd3 MultiBar chart
      $scope.multiBarChart = nvd3Graphs.multiBarChart();
      $scope.multiBarChart.chart.legend = {

        updateState: false,
        width: 560,
        margin: {
          left: 5,
          right: 5,
          bottom: 30
        },
        maxKeyLength: 560,
        padding: 50
      };

      //Setting X axis label to the chart.xAxis.axisLabel property
      $scope.multiBarChart.chart.xAxis.axisLabel = "Age (days)";
      //Setting Y axis label to the chart.yAxis.axisLabels property
      $scope.multiBarChart.chart.yAxis.axisLabel = "Count";
      // Format y-axis labels to int
      $scope.multiBarChart.chart.yAxis.tickFormat = function (d) {
        return d3.format('d')(d);
      };


      $scope.dashboardScope.watchData(widgetConfig.dataSourceName, dataChanged, true);

    });

  };
  return barGraphChartController;

});
