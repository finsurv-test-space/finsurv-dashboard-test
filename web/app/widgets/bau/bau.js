// Declare all the widget dependencies that need to be required in for this widget:
define([
  './bau.module',
  './bau.controller'
], function(module) {
  // We simply return the Angular module for this widget:
  return module;
});
