//This widget represents Banking as Usual


define(['angular', './bau.module'], function(angular, module) {

  module.controller('bauWidgetController', bauChartController);

  // Define the dependencies:
  bauChartController.$inject = ['$scope', '$rootScope', '$q', '$uibModal','$http'];

  // Define the widget factory:
  function bauChartController($scope, $rootScope, $q, $modal, $http) {

    var widgetConfig = $scope.widget.config;

    function dataChanged(transactions)
    {
      $scope.pivotQuery = transactions.query; 

      $scope.isEmpty = function(){
        return transactions.data.length == 0;
      };
      
      var keys = ["incomplete", "internalRejected", "sarbRejected", "unmatchedTransactions", "unmatchedAccountEntries", "authorise"];

      $scope.data = keys.reduce((function(memo,item, index){

        tempData = {
          data:   _.range(15).reduce(function(memo1, item1, index1){
            memo1.push({
              delay: String(-1*item1),
              colour: item1+1,
              count: 0
            });
            return memo1;
          },[]),
          total: 0
        };
        tempData.data.unshift({
          delay: "> 0",
          colour: 0,
          count: 0
        });
        tempData.data.push({
          delay: "<= -15",
          colour: 16,
          count: 0
        });

        memo[item] = tempData;

        return memo;

      }), {});


      transactions.data.map(function(item){
        var foundDelay = $scope.data[item.state].data.find(function(i){
          return i.delay == item.delay;
        })
        if (foundDelay){
          foundDelay.count += parseInt(item.count);
          $scope.data[item.state].total += parseInt(item.count);
        };
      });


    };

    $scope.getColour = function(colour) {
      var hue = 125 - Math.abs((((colour)) / 13) * 125) - 10;
      if (hue < 0) {
        hue = 0;
      }
      return hue;
    };

    $scope.setModelQuery = function(state, delay, count) {
      //Access the pivot query in this function scope
      var pivotQuery = $scope.pivotQuery;

      //Generate the drill down query from the pivot query and the date, delay and system.
      var modelQuery = {
        model: pivotQuery.model,
        startAt: pivotQuery.startAt,
        maxRecords: 20,
        filter: [],
        orderBy: ["asc('BAUState')"]
      };

      for (var key in pivotQuery.select) {
        if (key == "state") {
          modelQuery.filter.push(pivotQuery.select[key] + ".equals('" + state + "')");
        } else if (key == "delay") {
          modelQuery.filter.push(pivotQuery.select[key] + ".equals('" + delay + "')");
        }
      };

      //Need a user object because there is no Dashboard login
      var user = {
        headers: {
          "REMOTE_USER": "admin"
        }
      };

      open(count, modelQuery, user);

    };

    //Function to open the drill down modal
    var open = function(count, modelQuery, user) {

      var outerScope = $scope;
      var modalInstance = $modal.open({
        templateUrl: require.toUrl(
          //Use this partial for the modal
          'app/widgets/delayMatrix/delayMatrix.modal.html'
        ),
        controller: ["$scope", "$uibModalInstance",
          function($scope, $uibModalInstance) {
            $scope.ok = function() {
              $uibModalInstance.close();
            };

            //Set attach the data to the modal scope
            $scope.data = []; //Uncomment to use live data

            $scope.totalItems = count;

            $scope.currentPage = 1;
            $scope.numPerPage = 20;

            $scope.maxSize = 5;

            $http.post("/finsurv-dashboard/doPost/dashboard/getmodel", JSON.stringify(modelQuery), user)
              .then(success, failure)

            function success(response) {
              //On success get the data and open a modal by passing in data to the open() function
              var res = response.data.data;

              $scope.data = res.map(function(item, index) {
                item["num"] = $scope.currentPage + index;
                return item;
              });

            };

            function failure(response) {
              // open({}); //Needs to be removed when a connection to DB is established
              console.log(response.data);
            };

            $scope.$watch('currentPage + numPerPage', function() {
              $scope.data = [];

              var begin = $scope.currentPage == 1 ? 1 : (($scope.currentPage - 1) * $scope.numPerPage + 1);
              var end = $scope.currentPage == 1 ? $scope.numPerPage : ($scope.currentPage * $scope.numPerPage);

              modelQuery["startAt"] = begin;


              $http.post("/finsurv-dashboard/doPost/dashboard/getmodel", JSON.stringify(modelQuery), user)
                .then(success, failure)

              function success(response) {
                //On success get the data and open a modal by passing in data to the open() function

                var res = response.data.data;

                $scope.data = res.map(function(item, index) {
                  item["num"] = begin + index;
                  return item;
                });

              };

              function failure(response) {
                // open({}); //Needs to be removed when a connection to DB is established
                console.log(response.data);
              };

            });

          }
        ]
      });
    };


    $scope.dashboardScope.allWidgetsLoadedPromise.then(function(){
      $scope.dashboardScope.watchData(widgetConfig.dataSourceName, dataChanged, true);
    });

  };
  return bauChartController;

});
