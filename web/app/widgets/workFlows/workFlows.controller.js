//Data is represented on current state on the entire system

define(['angular', './workFlows.module'], function(angular, module) {

  module.controller('workFlowsWidgetController', workFlowsChartController);

  // Define the dependencies:
  workFlowsChartController.$inject = ['$scope', '$rootScope', '$q'];

  // Define the widget factory:
  function workFlowsChartController($scope, $rootScope, $q) {


    var widgetConfig = $scope.widget.config;

    function dataChanged(workFlowList) {

        $scope.isEmpty = function(){
          return workFlowList.data.length == 0;
        };

        $scope.activeWorkflows = [];
        $scope.pausedWorkflows = [];

        workFlowList.data.activeWorkflows.map(function(item){
          var comment = item.comment;
          var tempString = comment.substr(comment.indexOf(";")+2, comment.length);
          tempString.indexOf("-") == -1 ? $scope.activeWorkflows.push(item) : $scope.pausedWorkflows.push(item);
        });

        $scope.stoppedWorkflows = workFlowList.data.stoppedWorkflows;
        console.log("");
    };


    $scope.dashboardScope.allWidgetsLoadedPromise.then(function(){
      $scope.$on("widgetResized",function(event,updatedWidget){
          //Setting width and height of the graph according to height and width of the widget
          //Important for resizings
          //since the px value is taken, the heights and widths need to be reduced
          $scope.tableWidth = updatedWidget.config.width;
          $scope.tableHeight = updatedWidget.config.height - updatedWidget.config.height/4;
      });
      $scope.dashboardScope.watchData(widgetConfig.dataSourceName, dataChanged, true);

    });
  };
  return workFlowsChartController;
});
