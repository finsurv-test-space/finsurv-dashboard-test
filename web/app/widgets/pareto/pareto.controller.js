
define(['angular', './pareto.module', 'nvd3Graphs'], function(angular, module, nvd3Graphs) {

  module.controller('paretoWidgetController', paretoChartController);

  // Define the dependencies:
  paretoChartController.$inject = ['$scope', '$rootScope', '$q', '$http', '$uibModal'];

  // Define the widget factory:
  function paretoChartController($scope, $rootScope, $q, $http, $modal) {

      var widgetConfig = $scope.widget.config;

      function dataChanged(errors) //errors is a object with a data property and the query property
      {
        //Getting the pivot query from the datasouce config
        $scope.pivotQuery = errors.query;

        $scope.isEmpty = function(){
          return errors.data.length == 0;
        };

        //First change every count to an integer
        errors.data.map(function(item){
          item.count = parseInt(item.count);
        });

        //Calculate the total of the counts
        var totalCount = errors.data.reduce(function(memo, item){
          memo += item.count;
          return memo;
        }, 0);

        //Sort the object in descending order according to the count
        errors.data = errors.data.sort(function(a,b){
          if (a.count < b.count) {
            return 1;
          }
          if (a.count > b.count) {
            return -1;
          }
          return 0;
        });

        //Get an array of the error groups
        var xAxisData = errors.data.reduce(function(memo, item){
          memo.push(item.errorCode);
          return memo;
        }, []);


        var accumCount = 0;

        //reduce data to VM
        $scope.data = [
          {
            key: "count",
            bar: true,
            values: errors.data.reduce(function(memo, item, index){
              memo.push({
                x: index,
                y: item.count
              });
              return memo;
            },[])
          },
          {
            key: "percentage",
            values: errors.data.reduce(function(memo, item, index){
              accumCount += item.count;
              memo.push({
                x: index,
                y: parseInt((accumCount/totalCount * 100).toFixed(2))
              });
              return memo;
            },[])
          }
        ];


        //Using linePlusBarChart from nvd3Graphs
        $scope.LineBarChart = nvd3Graphs.linePlusBarChart();

        //Setting the X Axis of the graph
        $scope.LineBarChart.chart.xAxis.tickFormat = function(d){
          return xAxisData[d];
        }

        $scope.LineBarChart.chart.y1Axis.axisLabel = "Error Count";
        $scope.LineBarChart.chart.y2Axis.axisLabel = "Error Percentage";
        $scope.LineBarChart.chart.xAxis.axisLabel = "Error Code";        


        $scope.LineBarChart.chart.callback = function(chart){
          chart.bars.dispatch.on('elementClick', function(e){
            
              var count = e.data.y;
              var errorCode = xAxisData[e.data.x];

              $scope.modelQuery = {
                model: $scope.pivotQuery.model,
                startAt: $scope.pivotQuery.startAt,
                maxRecords: 20,
                filter: [],
                orderBy: [
                  "asc('System')",
                  "asc('TrnReference')"
                ]
              };

              $scope.modelQuery.filter.push(
                $scope.pivotQuery.select["errorCode"] + ".equals('" + errorCode + "')"
              );

              var user = {
                headers: {
                  "REMOTE_USER": "admin"
                }
              };

              open(count, $scope.modelQuery, user)
            });
          };
        };

        var open = function (count, modelQuery, user) {

          var outerScope = $scope;
          var modalInstance = $modal.open({
            templateUrl: require.toUrl(
                //Use this partial for the modal
                'app/widgets/pareto/pareto.modal.html'
            ),
            controller: ["$scope", "$uibModalInstance",
              function ($scope, $uibModalInstance) {

                $scope.ok = function () {
                  $uibModalInstance.close();
                };
                // $scope.data = modelService; //Mocked drill down data

                //Set attach the data to the modal scope
                // $scope.data = []; //Uncomment to use live data

                $scope.totalItems = count;

                console.log($scope.totalItems);

                $scope.currentPage = 1;
                $scope.numPerPage = 20;

                $scope.maxSize = 5;


                $http.post("/finsurv-dashboard/doPost/dashboard/getmodel", JSON.stringify(modelQuery), user)
                    .then(success, failure)

                function success(response) {
                  //On success get the data and open a modal by passing in data to the open() function
                  var res = response.data.data;

                  $scope.data = res.map(function (item, index) {
                    item["num"] = $scope.currentPage + index;
                    return item;
                  });
                }

                function failure(response) {
                  // open({}); //Needs to be removed when a connection to DB is established
                  console.log(response.data);
                };

                $scope.$watch('currentPage + numPerPage', function () {
                  $scope.data = [];


                  var begin = $scope.currentPage == 1 ? 1 : (($scope.currentPage - 1) * $scope.numPerPage + 1);
                  var end = $scope.currentPage == 1 ? $scope.numPerPage : ($scope.currentPage * $scope.numPerPage);

                  modelQuery["startAt"] = begin;


                  $http.post("/finsurv-dashboard/doPost/dashboard/getmodel", JSON.stringify(modelQuery), user)
                      .then(success, failure)

                  function success(response) {
                    //On success get the data and open a modal by passing in data to the open() function

                    var res = response.data.data;

                    $scope.data = res.map(function (item, index) {
                      item["num"] = begin + index;
                      return item;
                    });

                  }

                  function failure(response) {
                    console.log(response.data);
                  }

                });

              }
            ]
          });
      };


    $scope.dashboardScope.allWidgetsLoadedPromise.then(function(){
      // Listen for changes to the data source:
      $scope.dashboardScope.watchData(widgetConfig.dataSourceName, dataChanged, true); 
    });



  };

  return paretoChartController;

});
