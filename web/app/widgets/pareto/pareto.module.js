define(['angular'], function(angular) {
  return angular.module('widgets.pareto', ['nvd3']);
});
