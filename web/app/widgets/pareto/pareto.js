define([
  './pareto.module',
  './pareto.controller'
], function(module) {
  // We simply return the Angular module for this widget:
  return module;
});
