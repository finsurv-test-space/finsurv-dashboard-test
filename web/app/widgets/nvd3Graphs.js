//This file contains all the specs for the Angular nvd3 graphs
//Each graph also has a spec of the data structure that has to passed to the DOM for each graph
//  |-> Note: The structure of the data is not necessarily the same for each graph
//The graph specs only contain the minimum that is needed to draw the graph
//  |-> Any other changes to the graph will have to be done in the widget controller
//  |-> For other properties: http://krispo.github.io/angular-nvd3/
//added as dependency in the "app.dependencies.js" file which exports "nvd3Graphs"
//to use this graphs in a controller add it in the controller definition:
  /*
    eg: define(['angular', './nvd3Graphs'], function(angular, module) {
          ...
        }
  */
define(function(){

  var nvd3Graphs = {
    //**********************************************************************************
    //MultiBar Chart
    //**********************************************************************************

    /*
      Array of key/values pairs, where values is an array of x/y pairs, where x and y are integers
      data=[
        {
          key: "New",
          values: [
            {x: 0, y: 25},
            {x: 1, y: 57},
            {x: 2, y: 14},
            {x: 3, y: 74},
            {x: 4, y: 71},
            {x: 5, y: 92},
            ...
          ]
        }, {

        }, ...
      ]
    */

    multiBarChart: function(){
      return {
        chart: {
          type: "multiBarChart",
          height: 450,
          margin: {
            top: 20,
            right: 70,
            bottom: 70,
            left: 70
          },
          clipEdge: true,
          duration: 500,
          stacked: true,
          xAxis: {
            axisLabel: "",
            showMaxMin: false,
            axisLabelDistance: 0
          },
          yAxis: {
            axisLabel: "",
            axisLabelDistance: 0
          }
        },
        title: {
          enable: true,
          text: ""
        }
      }
    },

    //**********************************************************************************
    //Pie Chart
    //**********************************************************************************

    /*
      Array of key/y pairs, where y is an integer
      data=[
          {key: "Safe", y: 14},
          {key: "Warning", y: "20"},
          {key: "Danger", y: "31"}, ...
      ]
    */

    pieChart: function(){
      return {
        chart: {
          type: 'pieChart',
          height: 300,
          x: function(d){return d.key;},
          y: function(d){return d.y;},
          showLabels: true,
          duration: 500,
          labelThreshold: 0.01,
          labelSunbeamLayout: true,
          margin: {
            bottom: 0,
            top: 0,
            left: 0,
            right: 0
          }
        },
        title: {
          enable: true,
          text: ""
        }
      }
    },


    //**********************************************************************************
    //Line + Bar Chart
    //**********************************************************************************

    /*
      Array of Objects
      data=[
        {
          key : "foo",
          bar: true,
          values: [
            [0,1],
            [2,3],
            ...
          ]
        },
        {
          key: "bar",
          values: [
            [4,5],
            [9,3],
            ...
          ]
        }
      ]
    */

    linePlusBarChart: function(){
      return  {
        chart: {
            type: 'linePlusBarChart',
            height: 500,
            margin: {
                top: 30,
                right: 80,
                bottom: 70,
                left: 80
            },
            bars: {
                forceY: [0]
            },
            bars2: {
                forceY: [0]
            },
            color: ['lightblue', 'orange'],
            xAxis: {
                axisLabel: '',
                tickFormat: function(d) {
                    // return xAxisData[d];
                    return d;
                },
                rotateLabels: 0
            },
            x2Axis: {
              showMaxMin: false
            },
            y1Axis: {
                axisLabel: '',
                tickFormat: function(d){
                    return d;
                },
                axisLabelDistance: 6
            },
            y2Axis: {
                axisLabel: '',
                tickFormat: function(d) {
                    return d;
                },
                axisLabelDistance: 6

            },
            "y3Axis": {},
            "y4Axis": {},
            yDomain: [0,100]
          },
          interactive: true,
          tooltip:true,
        title: {
          enable: true,
          text: ""
        }
      };
    },


    //**********************************************************************************
    //multiBar Horizontal Chart
    //**********************************************************************************

    /*
      Array of Objects
      data=[
      {
         "key": "Series1",
         "color": "#d62728",
         "values": [
             {
                 "label" : "Group A" ,
                 "value" : -1.8746444827653
             } , ...
         ]
       },

       {
        "key": "Series2",
        "color": "#1f77b4",
        "values": [
            {
                "label" : "Group A" ,
                "value" : 25.307646510375
            } , ...
        }

      ]
    */

    multiBarHorizontalChart: function(){
      return {
        chart: {
          type: 'multiBarHorizontalChart',
          height: 450,
          x: function(d){return d.label;},
          y: function(d){return d.value;},
          showControls: true,
          showValues: true,
          duration: 500,
          xAxis: {
              axisLabel: "Count",
              showMaxMin: false
          },
          yAxis: {
              axisLabel: "Date"
          }
        }

      };
    }





  };
  return nvd3Graphs;
});
