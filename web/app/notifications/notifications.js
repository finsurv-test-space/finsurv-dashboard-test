// Load all the related files for the module:
define([
  './notifications.module',
  './notifications.constants',
  './notifications-data.service'
], function (module) {
  return module;
});