'use strict';

define(['angular'], function (angular) {

  // Define the module:
  var module = angular.module('notifications', ['webSocket', 'user', 'navigation', 'notifyPopup']);

  return module;
});


