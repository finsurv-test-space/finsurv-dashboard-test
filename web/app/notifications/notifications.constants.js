define(['./notifications.module'], function (module) {
  // Define any constants:

  module.constant('notificationThreshold', {
    NONE: {value: 'NONE'},
    SET: {value: 'SET'},
    INFO: {value: 'INFO'},
    WARN: {value: 'WARN'},
    ERROR: {value: 'ERROR'}
  });

});