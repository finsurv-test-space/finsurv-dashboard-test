define(['./notifications.module', 'angular', 'pnotify', 'underscore'], function (module, angular, PNotify, _) {

  // Define the trade feed data service:
  module.factory('notificationsData', factory);

  factory.$inject = ['webSocketConnection',
    '$http',
    'user',
    'notifyPopup',
    'navigationTree']; // The navigationTree is from the navigation-tree-service in the navigation module.
  function factory(webSocketConnection, $http, user, notifyPopup, navigationTree) {

    // Create the trade feed data structure:
    var notificationData = {
      notifications: [],
      isAvailable: false,
      subscribe: subscribe,
      unsubscribe: unsubscribe
    };

    // Define the callbacks:
    var callbacks = [];

    // Get the current user:
    var currentUser = user;

    // Subscribe to the navigation tree so that we know when the actual tree has loaded (after all providers have had a chance to run)
    // NOTE: We need this so that we can add all notifications to the right place in the tree (without this, we were missing counts on the tree).
    navigationTree.subscribe(navigationDataAvailable);

    // Keep track of any pending notifications that need to be processed while we are waiting for the real navigation tree to come back:
    var pendingNotifications = [];

    // Get the initial feed data:
    var notificationsDataPromise = $http.get('/resource/secure/notifications/' + currentUser.id).then(function (response) {
      // Just return the feed data from the HTTP response:
      var notifications = response.data;

      // Save the notification data:
      notificationData.notifications = notifications;

      // Process all the notifications:
      notifications.forEach(function (notification) {
        // Check whether we already have the real navigation tree or whether we should keep this notification for later:
        if (navigationTree.loaded) {
          // The real navigation tree has loaded.
          // Process the notification without displaying a popup:
          processNotification(notification, false);
        }
        else {
          // The real navigation tree has not loaded yet.
          // Add the notification to a list of pending notifications to process later when the actual navigation tree actually loads:
          pendingNotifications.push(notification);
        }
      });

      // Flag that the data is available:
      notificationData.isAvailable = true;

      // Publish changes:
      publish();

      return notifications;
    }).catch(function (err) {
      notifyPopup('Failed to retrieve notifications. ' + err);
    });

    // Save the promise for the trade feed data:
    notificationData.promise = notificationsDataPromise;

    // Subscribe to messages from the trade feed:
    webSocketConnection.subscribe(handleMessage);

    // Add functions for working with the notifications:
    notificationData.updateDiffs = updateDiffs;
    notificationData.removeNotice = removeNotice;
    notificationData.markRead = markRead;
    notificationData.markAllRead = markAllRead;
    notificationData.initializeNotifications = initializeNotifications;
    notificationData.createNotification = createNotification;
    notificationData.removeNotification = removeNotification;


    /*
     This gets called once the real navigation tree is loaded.
     */
    function navigationDataAvailable() {
      // Now the real navigation tree is loaded.
      // Check whether there are any pending notifications that we need to process:
      if (pendingNotifications) {
        pendingNotifications.forEach(function (notification) {
          // Process the notification without displaying a popup:
          processNotification(notification, false);
        })
      }
      pendingNotifications = [];
    }

    /*
     This subscribes to notification data updates.
     Whenever notification data is available or changes, the callback will be called with the notification data object as the argument.
     */
    function subscribe(callback) {
      // Save the callback:
      callbacks.push(callback);

      // Check whether the trade feed data is already available:
      if (notificationData.isAvailable) {
        // Publish the data immediately to the callback:
        callback(notificationData);
      }
    }

    /*
     This un-subscribes from trade feed data updates.
     */
    function unsubscribe(callback) {
      var index = callbacks.indexOf(callback);
      if (index > -1) {
        // We found this callback.
        // Remove it:
        callbacks.splice(index, 1);
      }
    }

    /*
     This publishes data to all subscribers.
     */
    function publish() {
      // Make sure to copy the array in case one of the callbacks adds or removes more callbacks:
      var oldCallbacks = callbacks.slice(); // shallow copy of the array: http://stackoverflow.com/questions/7486085/copying-array-by-value-in-javascript
      oldCallbacks.forEach(function (callback) {
        // Pass the updated data to the callback:
        callback(notificationData);
      });
    }

    /*
     This handles a message from the web socket.
     */
    function handleMessage(message, data) {
      // Get the incoming notification:
      var incomingNotification = data.notification;

      // Check if the message has notification data:
      if (incomingNotification) {
        // Get the type of message:
        var type = incomingNotification.threshold == 'INFO' ? 'info' :
            incomingNotification.threshold == 'WARN' ? 'warn' : 'error';

        // Get the header for the message:
        var header = incomingNotification.header;
        if (header == null) {
          header = incomingNotification.threshold == 'INFO' ? 'Info' :
              incomingNotification.threshold == 'WARN' ? 'Warning' : 'Error';
        }

        // Get the message:
        var message = incomingNotification.message;

        if (_.findIndex(notificationData.notifications, {id: incomingNotification.id}) == -1) {
          // Update the notification:
          incomingNotification.header = header;
          incomingNotification.message = message;
          incomingNotification.type = type;
          incomingNotification.header = header;

          // Create a new notification:
          var notification = addNotification(incomingNotification, true);

          // Publish the new data:
          publish();
        }
      }
    }


    function updateDiffs() {
      notificationData.notifications.forEach(function (notification) {
        setTimeDifference(notification);
      });
    };

    function setTimeDifference(notification) {
      var timestamp = new Date(notification.timestamp);
      var difference = (new Date().getTime() - timestamp.getTime()) / 1000;
      if (difference < 1) {
        difference = 0;
      }

      var hours = Math.floor(difference / 3600);
      var minutes = Math.floor(difference / 60);
      var seconds = Math.floor(difference - (hours * 3600) - (minutes * 60));

      if (hours > 1) {
        notification.diff = hours + ' hours ago';
      }
      else if (hours == 1) {
        notification.diff = 'An hour ago';
      }
      else if (minutes > 1) {
        notification.diff = minutes + ' minutes ago';
      }
      else if (minutes == 1) {
        notification.diff = 'A minute ago';
      }
      else if (seconds > 9) {
        notification.diff = seconds + ' seconds ago';
      }
      else {
        notification.diff = 'a few moments ago';
      }
    };

    function removeNotice(notification) {
      var notice = _.filter(PNotify.notices, function (obj) {
        return obj.options.id == notification.id;
      })

      if (notice.length > 0) {
        notice[0].remove(true);
      }
    };

    /**
     * This marks the specific notification as being read.
     * @param notification The notification to mark as read.
     */
    function markRead(notification) {
      // Look for the corresponding notification in our actual data (because the incoming notification might be a clone).
      var notificationIndex = _.findIndex(notificationData.notifications, {id: notification.id});

      // Make sure we have this notification:
      if (notificationIndex == -1) return;

      // Get the real notification:
      notification = notificationData.notifications[notificationIndex];

      // Capture the read time:
      var readTime = new Date(); // Now.
      $http.get('/resource/secure/notification/' + notification.id + "/mark")
          .then(function (data) {
            // Remove the notification from the existing list:
            // NOTE: We don't want to remove the notification, just update it as being read.
            //notificationData.notifications = _.without(notificationData.notifications, notification);

            // Update the read time:
            notification.readTime = readTime;

            // Remove the popup notice:
            notificationData.removeNotice(notification);

            // Remove the notification:
            navigationTree.removeBadgesFromNavTree(notification);

            // Publish changes to anyone that is listening:
            publish();
            //$rootScope.$broadcast('removeNotification', notification.id);
          })
      //.error(function ()
      //{
      //  notifyPopup('Failed to mark notification as read');
      //});
    };

    /**
     * This marks ALL the notifications as being read.
     */
    function markAllRead() {
      // Capture the read time:
      var readTime = new Date(); // Now.

      // Create a temporary copy of the notifications so we update the correct notifications:
      // NOTE: It is possible that a new notification comes in while we are waiting for this call to complete.
      var notificationsToProcess = notificationData.notifications.slice(); // shallow copy of the array: http://stackoverflow.com/questions/7486085/copying-array-by-value-in-javascript

      $http.get('/resource/secure/notifications/' + (currentUser.id || 0) + '/history/mark')
          .success(function (data, responseCode) {
            // Go through each notification and update it:
            notificationsToProcess.forEach(function (notification) {
              // a notification that has a readTime has already been read, we are interested in unread notifications.
              if (notification.readTime === undefined) {

                // Update the read time:
                notification.readTime = readTime;

                // Remove the popup notice:
                notificationData.removeNotice(notification);

                // Remove the notification:
                navigationTree.removeBadgesFromNavTree(notification);
              }
            });

            // Publish changes to anyone that is listening:
            publish();
          })
      //.error(function (data, responseCode) {
      //  notifyPopup("Processing error: " + (data && data.message ? data.message : status));
      //});
    };

    //This function takes the initial notifications array, modifies it and then attaches badges to the nav tree and finally displays notifications.
    //Display is a boolean which determines whether or not a notification is shown
    function initializeNotifications(display) {
      notificationData.notifications.forEach(function (notification) {
        processNotification(notification, display);
      });
    };

    /*
     This processes a new notification that came in from the server side.
     It attaches any additional properties that are required.
     It also does any additional handling of the notification.
     This is one common place to define all the operations that are to be performed on a new notification from the server.
     */
    function processNotification(notification, display) {
      // Allow any navigation providers to identify if any badges should be updated with this notification:
      navigationTree.addBadgesToNavTree('notification', notification)

      if (display) {
        // Display a popup for this notification:
        displayNotification(notification)
      }
    }


    //Display is a boolean which determines whether or not a notification is shown
    function addNotification(notification, display) {
      notificationData.notifications.push(notification);
      processNotification(notification, display);
      return notification;
    };

    /*
     Creates a new notification and adds it.
     Display is a boolean which determines whether or not a notification is shown.
     */
    function createNotification(id, header, message, type, display) {
      var notification = {
        id: id,
        header: header,
        message: message,
        type: type
      };
      return addNotification(notification, display);
    };

    //This is api function for removing notifications.
    function removeNotification(notification) {
      console.log(notification);
      if (notification) {
        removeBadgeFromNavTree(notification);
        //This code removes the notifications from the actual notification array
        notifications.splice(notifications.indexOf(notification), 1);
      }
    };

    function createCallbackButtons(notificationActions) {
      var callBackButtonArray = [];
      if (notificationActions) {
        _.forEach(notificationActions, function (action) {
          var callBackButton = {
            text: action.name,
            addClass: 'btn-primary',
            click: function (notice) {
              $rootScope.$apply(function () {
                action.callback();
                notice.remove();
              })
            }
          };
          callBackButtonArray.push(callBackButton);
        })
      }
      return callBackButtonArray;
    };

    function displayNotification(notification) {
      new PNotify({
        //Todo do we display the number of times the same error occured? i.e. if badge count is 5 do we 5 Recon errors?

        title: notification.header,
        text: notification.message,
        mouse_reset: false,
        addclass: 'stack-bottomright',
        buttons: {
          closer_hover: true
        },
        confirm: {
          confirm: true,
          buttons: createCallbackButtons(notification.actions)
        },
        stack: {"dir1": "up", "dir2": "left", "firstpos1": 25, "firstpos2": 25}
        //hide: data.notification.delay > 0 ? true : false,
        //animate_speed: data.notification.delay,

      })
    };

    return notificationData;
  }

});