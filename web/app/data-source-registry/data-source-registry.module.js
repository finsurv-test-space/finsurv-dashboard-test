define([
  'angular',
  'data-source-list'
], function (angular, dataSourceList) {

  // Define what other module this one depends on:
  // We want this module to depend on all the data source implementations:
  var moduleDependencies = dataSourceList.map(function(ds){return 'data-sources.' + ds.name;});

  // Define the module:
  var module = angular.module('dataSourceRegistry', moduleDependencies);

  return module;
});


