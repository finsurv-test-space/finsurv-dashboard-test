define([
  './data-source-registry.module',
  'require'
], function (module, require) {

  module.factory('dataSourceRegistry', factory);
  factory.$inject = ['dataSourceList']; // dataSourceList is injected by the data source loader.
  function factory(dataSourceList)
  {
    return {
      hasDataSource: hasDataSource,
      getDataSourceHtmlUrl: getDataSourceHtmlUrl,
      getDataSourceControllerConstructor: getDataSourceControllerConstructor
    };

    /*
      This checks whether the given data source type exists.
     */
    function hasDataSource(dashedDataSourceType)
    {
      // Search for any data source with that type:
      return dataSourceList.filter(function(ds){ return ds.name == dashedDataSourceType; }).length > 0;
    }

    /*
     This gets the URL to the html for the data source type.
     */
    function getDataSourceHtmlUrl(dashedDataSourceType)
    {
      // We rely on the fact that the data source was registered as a package, therefore the data source name will resolve.
      return require.toUrl('data-sources.' + dashedDataSourceType + '/' + dashedDataSourceType + '.html')
    }

    /*
     This gets the constructor function for the data source controller for the widget type.
     */
    function getDataSourceControllerConstructor(dashedDataSourceType)
    {
      // We rely on the fact that the data source was registered as a package, therefore the data source name will resolve.
      return require('data-sources.' + dashedDataSourceType + '/' + dashedDataSourceType + '.controller')
    }
  }

});