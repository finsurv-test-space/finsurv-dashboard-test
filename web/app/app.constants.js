/*
  This is a where application level constants get defined.
  Rather aim to keep constants for modules in the modules themselves.
 */
'use strict';
define(['./app.module'], function (appModule) {

  // Define any application level constants:

   appModule.constant('version', '0.1');
   appModule.constant('defaultState','dashboard')
   appModule.constant('brand',{
     //The brand name to be configured as per developer's preference
      brandName:'<i class="marketstream-logo">tx</i>stream',
      
      //The background color array to be configured by the developer and it overrides the default color in background.js
      backGroundColor:[
                  
                   {r: 19, g: 200, b: 140, a: 0.9},
                   {r: 49, g: 227, b: 176, a: 0.9}
                     ]
        })     

});