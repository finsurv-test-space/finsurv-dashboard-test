'use strict';

define(['angular'], function (angular) {

  var module = angular.module('modelService', []);
  return module;
});
