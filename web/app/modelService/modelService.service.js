define(['./modelService.module', 'angular'], function (module, angular) {

  /*
   This data service gives us access to ALL the queue data .
   */

  module.factory('modelService',modelService);

  // Define the queue service:
 modelService.$inject = ['$http', '$timeout'];
  function modelService($http, $timeout) {

    // Create the return structure:


    //******************************************************** Mock Data ********************************************************
    var modelServiceData={
      data: [
        {TrnReference: "HJ5272DF12", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 350.89, FlowCurrency: "USD"},
        {TrnReference: "HJ5272DF13", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 234.00, FlowCurrency: "USD"},
        {TrnReference: "HJ5272DF14", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 543.00, FlowCurrency: "GBP"},
        {TrnReference: "HJ8383DF53", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 433.34, FlowCurrency: "ZAR"},
        {TrnReference: "HJ5445DF33", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 342.04, FlowCurrency: "USD"},
        {TrnReference: "HJr234DF43", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 445.23, FlowCurrency: "IR"},
        {TrnReference: "HJ0933DF53", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 713.35, FlowCurrency: "USD"},
        {TrnReference: "HJ3525DF72", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 432.34, FlowCurrency: "GBP"},
        {TrnReference: "HJ3255DF15", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 928.32, FlowCurrency: "ZAR"},
        {TrnReference: "HJ0373DF19", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 373.00, FlowCurrency: "USD"},
        {TrnReference: "HJ0243DF83", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 194.42, FlowCurrency: "IR"},
        {TrnReference: "HJ7893DF02", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 749.32, FlowCurrency: "ZAR"},
        {TrnReference: "HJ9439DF04", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 983.23, FlowCurrency: "USD"},
        {TrnReference: "HJ2934DF18", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 141.93, FlowCurrency: "ZAR"},
        {TrnReference: "HJ5272DF12", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 350.89, FlowCurrency: "USD"},
        {TrnReference: "HJ5272DF13", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 234.00, FlowCurrency: "USD"},
        {TrnReference: "HJ5272DF14", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 543.00, FlowCurrency: "GBP"},
        {TrnReference: "HJ8383DF53", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 433.34, FlowCurrency: "ZAR"},
        {TrnReference: "HJ5445DF33", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 342.04, FlowCurrency: "USD"},
        {TrnReference: "HJr234DF43", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 445.23, FlowCurrency: "IR"},
        {TrnReference: "HJ0933DF53", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 713.35, FlowCurrency: "USD"},
        {TrnReference: "HJ3525DF72", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 432.34, FlowCurrency: "GBP"},
        {TrnReference: "HJ3255DF15", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 928.32, FlowCurrency: "ZAR"},
        {TrnReference: "HJ0373DF19", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 373.00, FlowCurrency: "USD"},
        {TrnReference: "HJ0243DF83", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 194.42, FlowCurrency: "IR"},
        {TrnReference: "HJ7893DF02", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 749.32, FlowCurrency: "ZAR"},
        {TrnReference: "HJ9439DF04", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 983.23, FlowCurrency: "USD"},
        {TrnReference: "HJ2934DF18", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 141.93, FlowCurrency: "ZAR"},
        {TrnReference: "HJ5272DF12", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 350.89, FlowCurrency: "USD"},
        {TrnReference: "HJ5272DF13", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 234.00, FlowCurrency: "USD"},
        {TrnReference: "HJ5272DF14", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 543.00, FlowCurrency: "GBP"},
        {TrnReference: "HJ8383DF53", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 433.34, FlowCurrency: "ZAR"},
        {TrnReference: "HJ5445DF33", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 342.04, FlowCurrency: "USD"},
        {TrnReference: "HJr234DF43", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 445.23, FlowCurrency: "IR"},
        {TrnReference: "HJ0933DF53", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 713.35, FlowCurrency: "USD"},
        {TrnReference: "HJ3525DF72", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 432.34, FlowCurrency: "GBP"},
        {TrnReference: "HJ3255DF15", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 928.32, FlowCurrency: "ZAR"},
        {TrnReference: "HJ0373DF19", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 373.00, FlowCurrency: "USD"},
        {TrnReference: "HJ0243DF83", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 194.42, FlowCurrency: "IR"},
        {TrnReference: "HJ7893DF02", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 749.32, FlowCurrency: "ZAR"},
        {TrnReference: "HJ9439DF04", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 983.23, FlowCurrency: "USD"},
        {TrnReference: "HJ2934DF18", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 141.93, FlowCurrency: "ZAR"},
        {TrnReference: "HJ5272DF12", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 350.89, FlowCurrency: "USD"},
        {TrnReference: "HJ5272DF13", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 234.00, FlowCurrency: "USD"},
        {TrnReference: "HJ5272DF14", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 543.00, FlowCurrency: "GBP"},
        {TrnReference: "HJ8383DF53", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 433.34, FlowCurrency: "ZAR"},
        {TrnReference: "HJ5445DF33", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 342.04, FlowCurrency: "USD"},
        {TrnReference: "HJr234DF43", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 445.23, FlowCurrency: "IR"},
        {TrnReference: "HJ0933DF53", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 713.35, FlowCurrency: "USD"},
        {TrnReference: "HJ3525DF72", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 432.34, FlowCurrency: "GBP"},
        {TrnReference: "HJ3255DF15", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 928.32, FlowCurrency: "ZAR"},
        {TrnReference: "HJ0373DF19", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 373.00, FlowCurrency: "USD"},
        {TrnReference: "HJ0243DF83", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 194.42, FlowCurrency: "IR"},
        {TrnReference: "HJ7893DF02", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 749.32, FlowCurrency: "ZAR"},
        {TrnReference: "HJ9439DF04", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 983.23, FlowCurrency: "USD"},
        {TrnReference: "HJ2934DF18", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 141.93, FlowCurrency: "ZAR"},
        {TrnReference: "HJ5272DF12", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 350.89, FlowCurrency: "USD"},
        {TrnReference: "HJ5272DF13", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 234.00, FlowCurrency: "USD"},
        {TrnReference: "HJ5272DF14", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 543.00, FlowCurrency: "GBP"},
        {TrnReference: "HJ8383DF53", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 433.34, FlowCurrency: "ZAR"},
        {TrnReference: "HJ5445DF33", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 342.04, FlowCurrency: "USD"},
        {TrnReference: "HJr234DF43", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 445.23, FlowCurrency: "IR"},
        {TrnReference: "HJ0933DF53", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 713.35, FlowCurrency: "USD"},
        {TrnReference: "HJ3525DF72", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 432.34, FlowCurrency: "GBP"},
        {TrnReference: "HJ3255DF15", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 928.32, FlowCurrency: "ZAR"},
        {TrnReference: "HJ0373DF19", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 373.00, FlowCurrency: "USD"},
        {TrnReference: "HJ0243DF83", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 194.42, FlowCurrency: "IR"},
        {TrnReference: "HJ7893DF02", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 749.32, FlowCurrency: "ZAR"},
        {TrnReference: "HJ9439DF04", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 983.23, FlowCurrency: "USD"},
        {TrnReference: "HJ2934DF18", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 141.93, FlowCurrency: "ZAR"},
        {TrnReference: "HJ5272DF12", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 350.89, FlowCurrency: "USD"},
        {TrnReference: "HJ5272DF13", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 234.00, FlowCurrency: "USD"},
        {TrnReference: "HJ5272DF14", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 543.00, FlowCurrency: "GBP"},
        {TrnReference: "HJ8383DF53", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 433.34, FlowCurrency: "ZAR"},
        {TrnReference: "HJ5445DF33", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 342.04, FlowCurrency: "USD"},
        {TrnReference: "HJr234DF43", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 445.23, FlowCurrency: "IR"},
        {TrnReference: "HJ0933DF53", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 713.35, FlowCurrency: "USD"},
        {TrnReference: "HJ3525DF72", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 432.34, FlowCurrency: "GBP"},
        {TrnReference: "HJ3255DF15", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 928.32, FlowCurrency: "ZAR"},
        {TrnReference: "HJ0373DF19", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 373.00, FlowCurrency: "USD"},
        {TrnReference: "HJ0243DF83", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 194.42, FlowCurrency: "IR"},
        {TrnReference: "HJ7893DF02", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 749.32, FlowCurrency: "ZAR"},
        {TrnReference: "HJ9439DF04", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 983.23, FlowCurrency: "USD"},
        {TrnReference: "HJ2934DF18", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 141.93, FlowCurrency: "ZAR"},
        {TrnReference: "HJ5272DF12", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 350.89, FlowCurrency: "USD"},
        {TrnReference: "HJ5272DF13", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 234.00, FlowCurrency: "USD"},
        {TrnReference: "HJ5272DF14", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 543.00, FlowCurrency: "GBP"},
        {TrnReference: "HJ8383DF53", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 433.34, FlowCurrency: "ZAR"},
        {TrnReference: "HJ5445DF33", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 342.04, FlowCurrency: "USD"},
        {TrnReference: "HJr234DF43", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 445.23, FlowCurrency: "IR"},
        {TrnReference: "HJ0933DF53", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 713.35, FlowCurrency: "USD"},
        {TrnReference: "HJ3525DF72", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 432.34, FlowCurrency: "GBP"},
        {TrnReference: "HJ3255DF15", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 928.32, FlowCurrency: "ZAR"},
        {TrnReference: "HJ0373DF19", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 373.00, FlowCurrency: "USD"},
        {TrnReference: "HJ0243DF83", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 194.42, FlowCurrency: "IR"},
        {TrnReference: "HJ7893DF02", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 749.32, FlowCurrency: "ZAR"},
        {TrnReference: "HJ9439DF04", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 983.23, FlowCurrency: "USD"},
        {TrnReference: "HJ2934DF18", ValueDate: "2016-05-21", ArrivalDate: "2016-05-19", FlowAmount: 141.93, FlowCurrency: "ZAR"}

      ]
    };
    //***************************************************************************************************************************

    return modelServiceData;

  }

});
