// Load all the related files for the app:
define([
  './app.module',
  './app.config',
  './background',
  './app.directives',
  './app.filters',
  './app.services',
  './app.constants'
], function (appModule) {
  // We use the app module for the app:
  return appModule;
});