define([
  'angular',
  'widget-list'
], function (angular, widgetList) {

  // Define what other module this one depends on:
  // We want this module to depend on all the widget implementations:
  var moduleDependencies = widgetList.map(function(w){return 'widgets.' + w.name;});

  // Define the module:
  var module = angular.module('widgetRegistry', moduleDependencies);

  return module;
});


