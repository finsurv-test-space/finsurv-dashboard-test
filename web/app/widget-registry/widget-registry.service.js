define([
  './widget-registry.module',
  'require'
], function (module, require) {

  module.factory('widgetRegistry', factory);
  factory.$inject = ['widgetList']; // widgetList is injected by the widget loader.
  function factory(widgetList)
  {
    return {
      hasWidget: hasWidget,
      getWidgetHtmlUrl: getWidgetHtmlUrl,
      getWidgetControllerConstructor: getWidgetControllerConstructor
    };

    /*
      This checks whether the given widget type exists.
     */
    function hasWidget(dashedWidgetType)
    {
      // Search for any widget with that type:
      return widgetList.filter(function(w){ return w.name == dashedWidgetType; }).length > 0;
    }

    /*
      This gets the URL to the html for the widget type.
     */
    function getWidgetHtmlUrl(dashedWidgetType)
    {
      // We rely on the fact that the widget was registered as a package, therefore the widget name will resolve.
      return require.toUrl('widgets.' + dashedWidgetType + '/' + dashedWidgetType + '.html')
    }

    /*
     This gets the constructor function for the widget controller for the widget type.
     */
    function getWidgetControllerConstructor(dashedWidgetType)
    {
      // We rely on the fact that the widget was registered as a package, therefore the widget name will resolve.
      return require('widgets.' + dashedWidgetType + '/' + dashedWidgetType + '.controller')
    }
  }

});