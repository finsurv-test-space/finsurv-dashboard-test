/**
 * Created by daniel on 15/10/02.
 */
define(['./dropdown-multiselect.module','require'], function (module,require) {

  module.directive('dropdownMultiselect', dropdownMultiselect);
  //dropdownMultiSelect.$inject=[''];
  function dropdownMultiselect() {
    return {
      restrict: 'E',
      scope: {
        model: '=',
        options: '=',
        field: '@',
        tooltip_field: '=tooltipField',
        empty_message: '@emptyMessage',
        pre_selected: '=preSelected'
      },
      templateUrl:require.toUrl( "./dropdownMultiselect.html"),

      controller: ['$scope', function ($scope) {
        $scope.openDropdown = function () {
          $scope.selected_items = [];
          if ($scope.pre_selected != undefined) {

            for (var i = 0; i < $scope.pre_selected.length; i++) {
              $scope.selected_items.push($scope.pre_selected[i].id);
            }
          }
        };

        $scope.resolveDisplayName = function (option) {
          return $scope.field ? option[$scope.field] : option.name;
        };

        $scope.resolveTooltip = function (option) {
          return $scope.tooltip_field ? option[$scope.tooltip_field] : option.description;
        };

        $scope.selectAll = function () {
          $scope.model = _.pluck($scope.options, 'id');
        };
        $scope.deselectAll = function () {
          $scope.model = [];
        };
        $scope.setSelectedItem = function () {
          if ($scope.model == undefined) {
            $scope.model = [];
          }
          var id = this.option.id;
          if (_.contains($scope.model, id)) {
            $scope.model = _.without($scope.model, id);
          } else {
            $scope.model.push(id);
          }
          return false;
        };
        $scope.isChecked = function (id) {
          if (_.contains($scope.model, id)) {
            return 'fa fa-check pull-right';
          }
          return false;
        };

        $scope.hasSelection = function () {
          for (var index in $scope.options) {
            if ($scope.isChecked($scope.options[index].id)) {
              return true;
            }
          }

          return false;
        };
      }]
    }
  }

  return module
})
