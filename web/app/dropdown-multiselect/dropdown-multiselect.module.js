/**
 * Created by daniel on 15/10/02.
 */
define(['angular'],function(angular){
  var dropDownMultiselectModuleDependencies = [];
  var dropDownMultiselectModule = angular.module('dropdownMultiselect',dropDownMultiselectModuleDependencies);
  return dropDownMultiselectModule;
})