define([
  'angular',
  'pnotify'
],
  function (angular) {

  // Define the module:
  var module = angular.module('webSocket', ['pnotify', 'user']);

  return module;
});


