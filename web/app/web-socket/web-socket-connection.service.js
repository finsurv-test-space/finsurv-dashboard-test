define([
  './web-socket.module',
  'jquery'
], function (module, $) {

  module.factory('webSocketConnection', factory);
  factory.$inject = ['notifyPopup', '$state', 'user', '$rootScope'];
  function factory(notifyPopup, $state, user, $rootScope)
  {
    var service = {};
    service.callbacks = []; // The array will hold the callbacks.
    service.disconnect = function(){
      if(service.ws){
        service.ws.close();
        service.ws=undefined;
      }
    };
    service.connect = function (currentUser)
    {

      if (!currentUser || !currentUser.loggedIn)
        return;

      if (service.ws)
        return;

      var ws = new WebSocket(
        "ws://" + (document.location.hostname == "" ? "localhost" : document.location.hostname) + ":" +
        (document.location.port == "" ? "8080" : document.location.port) + "/client/" + currentUser.name
      );

      ws.onopen = function ()
      {
      };

      ws.onerror = function ()
      {
        notifyPopup('Error', 'Server connection lost.', function ()
        {
          // Hide any open dialogs/notifications
          $(".modal-dialog").each(
            function ()
            {
              $(this).hide();
            }
          );
          $(".ui-pnotify").each(
            function ()
            {
              $(this).hide();
            }
          );

          $state.go("login");
        }, {}, false);
      };

      ws.onmessage = function (message)
      {
        // Make sure we are in an apply block:
        $rootScope.$apply(function(){

          // Get the data for the message:
          var data = angular.fromJson(message.data);

          angular.forEach(service.callbacks, function (callback)
          {
            callback(message, data);
          });
        });

      };
      service.ws = ws;
    };

    service.send = function (message)
    {
      service.ws.send(message);
    };

    service.subscribe = function (callback)
    {
      // Add the callback:
      service.callbacks.push(callback);
    };

    service.unsubscribe = function (callback)
    {
      // Search for this callback:
      var index = service.callbacks.indexOf(5);
      if (index > -1) {
        // We found this callback.
        // Remove it:
        service.callbacks.splice(index, 1);
      }
    };


    // Get the current user so we can establish the web socket connection:
    var currentUser = user;

    // Connect automatically with the current user:
    service.connect(currentUser);

    user.subscribe(function(currentUser){
      if(currentUser.loggedIn){
        service.connect(currentUser)
      }
      else if(!currentUser || !currentUser.loggedIn){
        service.disconnect();
      }
    });

    return service;
  }

});