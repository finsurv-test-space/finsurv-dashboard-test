// Load all the related files for the module:
define([
  './web-socket.module',
  './web-socket-connection.service'
], function (module) {
  return module;
});