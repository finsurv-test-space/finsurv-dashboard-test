'use strict';

define(['angular'], function (angular) {

    // Define the module:
    var module = angular.module('alerts', []);

    return module;
});
