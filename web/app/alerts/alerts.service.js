define(['./alerts.module', 'angular'], function (module, angular) {

    /*
     This data service gives us access to ALL the sarbFiles data 
     */

    module.factory('alerts', alerts);


  // Define the sarbFiles service:
 alerts.$inject = ['$http', '$timeout', '$soap'];
  function alerts($http, $timeout, $soap) {
      
   $soap.addResponseHandler(function(method){
      return method + "Response";});
    
    return {getOperationalMessageQueueList: getOperationalMessageQueueList,
            getOperationalMessageList     : getOperationalMessageList,
            setUserName                   : setUserName,
            setWSDLUrl                    : setWSDLUrl, 
            setMethod                     : setMethod
    }
    
    var User ={};
    var url;
    var method;
    
    function getOperationalMessageQueueList()
    {
        return  $soap.post(getWSDLUrl(),getMethod(),getUser());
    }
    
     function getOperationalMessageList()
    {
        return $soap.post(getWSDLUrl(),"GetOpMessageList", {Queue: "", StartRow: 1, EndRow: 10000 });
    }
    
    function setMethod(newMethod)
    {
       method = newMethod;    
    }
    
    function getMethod()
    {
        return method;
    }
    
    function setUserName(newUser)
    {
        User = newUser;
    }
    
    function setWSDLUrl(newWSDLUrl)
    {
        url = newWSDLUrl;
    }
    
    function getWSDLUrl()
    {
        return url;
    }
    
    function getUser()
    {
        return {User};
    }
    
      
    
  }


});