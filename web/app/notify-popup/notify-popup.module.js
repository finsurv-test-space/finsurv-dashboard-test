define([
  'angular',
  'angular-bootstrap'
],
  function (angular) {

  // Define the module:
  var module = angular.module('notifyPopup', ['ui.bootstrap.modal']);

  return module;
});


