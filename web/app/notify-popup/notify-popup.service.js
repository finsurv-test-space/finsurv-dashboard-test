define([
  './notify-popup.module',
  'underscore'
], function (module, _) {

  module.factory('notifyPopup', factory);
  factory.$inject = ['$uibModal'];
  function factory($modal)
  {
    return function (heading,body,resultCB,scope,confirm){
      var modalInstance = $modal.open({
        template  : '<div class="modal-header"><h3>'+heading+'</h3></div>' +
        (body?'<div class="modal-body">'+body+'</div>':"") +
        '<div class="modal-footer">' +
        (confirm?'<button class="button" style="margin-right: 5px" ng-click="cancel()" focus-on>Cancel</button>':"") +
        '<button class="color green button" ng-click="ok()"' + (confirm ? '' : ' focus-on') + '>Ok</button>' +
        '</div>',
        size      : 'sml',
        controller:['$scope','$modalInstance', function ($scope, $modalInstance) {
          _.extend($scope,scope);
          $scope.ok = function () {
            $modalInstance.close();
          };
          $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
          };
        }]
      });
      modalInstance.result.then(resultCB);
    }
  }

});