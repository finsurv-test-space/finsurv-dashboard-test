'use strict';

/*
 Here we have a catch all place for general Directives.
 Aim to keep directives with the modules where they make sense, but if you can't, dump them here.
 */

define(['require', './app.module'], function (require, appModule)
{
  appModule
    .directive('appVersion', ['version', function (version)
    {
      return function (scope, elm, attrs)
      {
        elm.text(version);
      };
    }])
    // original source for this directive: http://jsfiddle.net/jaredwilli/vUSPu/

    .directive('logo', function ()
    {
      return {
        restrict: 'E',
        templateUrl: require.toUrl("./logo.html")
      }
    })
})
;