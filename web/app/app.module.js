'use strict';

define(['angular', 'angular-ui-router', 'angular-sanitize',], function (angular) {

  // The following line defines the app module and all of it's module dependencies. Therefore, if you want
  // Define the Angular module dependencies for the app:
  var moduleDependencies = [
    'ui.router',
    'ngSanitize',
    'ui.bootstrap',
    'ui.bootstrap.modal',
    'ui.bootstrap.datepicker'          
          ];

  // Define the app:
  var app = angular.module('app', moduleDependencies);
 

  app.run(['$rootScope', '$state', '$stateParams',
    function ($rootScope, $state, $stateParams) {

      
      // It's very handy to add references to $state and $stateParams to the $rootScope
      // so that you can access them from any scope within your applications.For example,
      // <li ui-sref-active="active }"> will set the <li> // to active whenever
      // 'contacts.list' or one of its descendants is active.
      //reconciliationData.subscribe(function(a){
      //  return a});
      $rootScope.$state = $state;
      $rootScope.$stateParams = $stateParams;

    }]);

  return app;
});


