define([
    './transactionList.module',
    './transactionList.directive',
    './transactionList.filter'
], function (module) {
    return module;
});