define(['angular', './transactionList.module'], function (angular, module) {

    module.filter('transactionListGrid', transactionListGrid);
    
    // Define the dependencies:
    transactionListGrid.$inject = []

    // Define the widget filter:
    function transactionListGrid() {

        return function (data, grid, query) {

            var matches = [];
            
            //no filter defined so bail
            if (query === undefined || query === '') {

                return data;
            }
            query = query.toLowerCase();
            //loop through data items and visible fields searching for match
            for (var i = 0; i < data.length; i++) {
                for (var j = 0; j < grid.columnDefs.length; j++) {
                    var dataItem = data[i];
                    var fieldName = grid.columnDefs[j]['name'];        
                    //as soon as search term is found, add to match and move to next dataItem
                  
                    if (dataItem[fieldName].toString().toLowerCase().indexOf(query) > -1) {
                        matches.push(dataItem);
                        break;
                    }
                }
            }
            return matches;
        }

    }
    return transactionListGrid;
});