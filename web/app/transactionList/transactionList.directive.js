define(['require', './transactionList.module'], function (require, module) {

    module.directive('transactions', transactionListDirective);

    transactionListDirective.$inject = ['queue', 'transactionData', 'transactionListGridFilter']


    function transactionListDirective(queue, transactionData, transactionListGrid) {
        return {
            restrict: 'EA',
            scope: {
                queueName: '=?',
                columns: '=columnDefs',
                data: '=data',
                rows: '=rows',
                height:'=height'
            },
            templateUrl: require.toUrl("./transactionList.html"),
            replace: true,
            controller: ['$scope', 'transactionData', function ($scope, transactionData) {
                                           
                // Transactions grid row template that enables the double click event
                function rowTemplate() {
                    return '<div ng-dblclick="grid.appScope.rowDblClick(row)" >' +
                        '  <div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader }"  ui-grid-cell></div>' +
                        '</div>';
                }
             
                //Define options for the ui-Grid
                $scope.transactionTableGridOptions = {
                    data: $scope.data,
                    rowTemplate: rowTemplate(),
                    columnDefs: $scope.columns,
                    enableFiltering: true,
                    //   enableRowSelection: true,
                    //enableCellSelection: true,
                    // enableRowHeaderSelection: false,
                    multiSelect: false,
                 //  minRowsToShow: $scope.rows,
                   enableHorizontalScrollbar:0,
                   enableVerticalScrollbar :0,
                };

                $scope.rowDblClick = function (row) {
                                     
                    var TrnReference = row.entity.TrnReference
                    var data = getData(TrnReference, queue)
                    function getData(TrnReference, data) {
                        for (var i = 0; i <= data.length; i++) {
                            if (data[i].TrnReference == TrnReference)
                                return data[i]
                        }
                    }
                 
                    //activate edit and transactions tab              
                    transactionData.setData(data);
                    // transactionData.change = true ? false : true ;
                    if (transactionData.change == true) {
                        transactionData.change = false;
                    }
                    else {
                        transactionData.change = true;
                    }                   

                };
                $scope.gridHeight=$scope.height+"px"
              
                

                $scope.text = { searchText: '' };
                $scope.refreshData = function () {
                     
                    $scope.transactionTableGridOptions.data = transactionListGrid($scope.data, $scope.transactionTableGridOptions, $scope.text.searchText);
                 
                    
                };

            }]
        }
    }

    return module
})
