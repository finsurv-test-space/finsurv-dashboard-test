/*
  This is a dumping ground for generic filters that are applicable across the app.
  Rather put filters in the modules that need them.
 */
'use strict';

/* Filters */
define(['./app.module'], function (appModule) {
    appModule
        .filter('interpolate', ['version', function (version) {
            return function (text) {
                return String(text).replace(/\%VERSION\%/mg, version);
            };
        }])
});