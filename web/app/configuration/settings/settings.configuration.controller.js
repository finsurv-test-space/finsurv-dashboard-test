define(['configuration/configuration.module', 'require'], function (module, require) {
  module.controller('configurationSettingsController', configurationSettingsController);
  configurationSettingsController.$inject = ['$scope', '$http', '$uibModal', '$stateParams', 'configurations', 'notifyPopup', 'tradeFeedData', 'configurationData'];
  //Configurations is defined in the resolve section of the configurations.state.js file
  function configurationSettingsController($scope, $http, $modal, $stateParams, configurations, notifyPopup, tradeFeedData, configurationData) {
    $scope.configurations = [];
    function getFeedData() {
      $scope.feeds = tradeFeedData.feeds;
      //Sort items in the menu items list alphabetically
      $scope.feeds.sort(function (a, b) {
        if (a.name < b.name) return -1;
        if (a.name > b.name) return 1;
        return 0;
      });
      if (!$scope.selectedFeed) {
        $scope.selectedFeed = $scope.feeds[0];
        configurationData.queryConfigurationData($scope.selectedFeed.id);
      }
    };
    tradeFeedData.subscribe(getFeedData);


    function getConfiguration() {
      $scope.configurations = configurationData.configurations;
    }

    configurationData.subscribe(getConfiguration);

    $scope.$watch('selectedFeed', function () {
      if ($scope.selectedFeed) {
        configurationData.queryConfigurationData($scope.selectedFeed.id);
      }
    })

    $scope.editConfiguration = function (configuration) {
      var modalInstance =
          $modal.open({
            resolve:{
              //Wrapping the resolve with a function to return a promise
              //http://stackoverflow.com/questions/18871810/angular-ui-modal-sending-data-into-modal-controller-from-http
              selectedFeed: function() {
                return $scope.selectedFeed;
              }
            },
            templateUrl: require.toUrl('configuration/settings/editConfigurations.html'),
            controller: ['$scope', '$modalInstance','selectedFeed', function ($scope, $modalInstance,selectedFeed) {
              $scope.configuration = angular.copy(configuration);
              //Todo not sure where this state params is being set?
              $scope.configuration.feedId = selectedFeed.id;
              $scope.saving = false;

              $scope.ok = function () {
                var resource = null;
                if ($scope.configuration.id == undefined) {
                  resource = $http.post('resource/secure/feed_configurations', $scope.configuration);
                } else {
                  resource = $http.put('resource/secure/feed_configuration/' + $scope.configuration.id, $scope.configuration);
                }
                $scope.saving = true;

                resource.then(function(response){
                  if (response.status == 200) {
                    var index = -1;
                    if ((index = _.findIndex(configurations, {id: $scope.configuration.id})) == -1) {
                      //configurations.push(response.data);
                      configurationData.queryConfigurationData(selectedFeed.id);
                      notifyPopup("Configuration successfully saved.");
                    } else {
                      configurationData.queryConfigurationData(selectedFeed.id);
                    }
                    $modalInstance.close();
                  }
                },function(response){
                  notifyPopup("Error saving configuration: " + (response.data && response.message ? response.message : status));
                  $scope.saving = false;
                });
              };

              $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
              };

              $scope.open = function ($event) {
                $event.preventDefault();
                $event.stopPropagation();
                $scope.opened = true;
              };
            }],
            size: 'lg'
          });
    }
  }

  return module;
})
