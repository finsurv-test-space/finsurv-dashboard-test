define(['./configuration.module', 'angular'], function (module, angular) {

  // Define the trade feed data service:
  module.factory('configurationData', factory);

  factory.$inject = ['webSocketConnection','$http', '$rootScope', 'userEvents'];
  function factory(webSocketConnection, $http, $rootScope, userEvents)
  {
    // Here is an example structure of a configuration:
    /*
     {
       feedId: 1
       id: 3
       key: "LastTradeID"
       value: "4"
     };
     */

    // Create the configuration data structure:
    var configurationData = {
      configurations:[],
      queryConfigurationData:queryConfigurationData,
      isAvailable: false,
      subscribe: subscribe,
      unsubscribe: unsubscribe
    };

    // Define the callbacks:
    var callbacks = [];

    /**
     * This will query for the configuration data from the server.
     */
    function queryConfigurationData(feedId)
    {
      // Get the
      var configurationDataPromise = $http.get('resource/secure/feed_configurations/'+feedId).then(function (response)
      {
        // Just return the configuration data from the HTTP response:
        var configurations = response.data;

        // Save the configuration data:
        configurationData.configurations = configurations;

        // Flag that the data is available:
        configurationData.isAvailable = true;

        // Publish changes:
        publish();

        return configurations;
      });

      // Save the promise for the tconfiguration data:
      configurationData.promise = configurationDataPromise;
    }

    // Listen for changes to user roles so we can refresh the data:
    $rootScope.$on(userEvents.ROLES_CHANGED, function (event)
    {
      // The user roles have changed.
      // Re-query the configuration data from the server:
      queryConfigurationData();
    });

    function subscribe(callback)
    {
      // Save the callback:
      callbacks.push(callback);

      // Check whether the trade configuration is already available:
      if (configurationData.isAvailable)
      {
        // Publish the data immediately to the callback:
        callback(configurationData);
      }
    }

    /*
     This un-subscribes from configuration data updates.
     */
    function unsubscribe(callback)
    {
      var index = callbacks.indexOf(callback);
      if (index > -1) {
        // We found this callback.
        // Remove it:
        callbacks.splice(index, 1);
      }
    }

    /*
    This publishes data to all subscribers.
     */
    function publish()
    {
      // Make sure to copy the array in case one of the callbacks adds or removes more callbacks:
      var oldCallbacks = callbacks.slice(); // shallow copy of the array: http://stackoverflow.com/questions/7486085/copying-array-by-value-in-javascript
      oldCallbacks.forEach(function(callback)
      {
        // Pass the updated data to the callback:
        callback(configurationData);
      });
    }


    return configurationData;
  }

});