define(['configuration/configuration.module',
        'configuration/configuration.states',
        'configuration/configuration.state',
        'configuration/configuration-data.service',
        'configuration/settings/settings.configuration.controller',
],function(module){
  return module;
})