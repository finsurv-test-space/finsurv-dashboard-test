define(['angular'], function (angular) {
  var configurationModuleModuleDependencies = [
    'ui.router',
    'settings',
    'user'
  ];
  var configurationModule = angular.module('configuration', configurationModuleModuleDependencies).run(runBlock);
  runBlock.$inject = ['settingsService', 'settingsConstants', 'user'];
  function runBlock(settingsService, settingsConstants, user) {
    //This is to register the  user settings menu item
    function registerConfigurationSettings() {
      var configurationSettingsMenuItem = [
        {
          name: 'Configuration',
          cssClass: 'i_configuration',
          description: 'Manage static configuration',
          roles: ['admin', 'user'],
          state: 'configurationSettings',
          permissions: settingsConstants.PermissionType.FEED_MANAGEMENT
        }];
      settingsService.registerMenuItem(configurationSettingsMenuItem);
    }
    //Only register the settings once the user has signed in

    //Only register the settings once the user has signed in
    //The reason for doing this in the runblock is so that the module can register its settings menu as it loads up.
    //One problem with doing this here is that the run block only gets run once at the app start up. This means if you logout
    //and log back in again as another user then the new settings items will not be loaded into the settings menu.
    //Subscribing the RegisterSettings function with the user module, means that as the user is updated we can then check to see if we need to register
    //these settings modules. This means that users will have the setting that are specific to them.
    function RegisterSettings(currentUser) {
      if (currentUser.loggedIn) {
        registerConfigurationSettings()
      }
    }

    user.subscribe(RegisterSettings);
  }


  return configurationModule;
})
