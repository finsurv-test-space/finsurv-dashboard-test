'use strict';

define([
  'require',
  'configuration/settings/settings.configuration.controller'

], function (require) {

  return {
    parent: 'settings',
    name: 'configurationSettings',
    url: '/Configuration',
    //abstract: true,
    //parent: 'root',
    //resolve: {
    //},
    resolve : {
      //Todo what are we going to do with the state params here
      configurations : function($q, $resource, $stateParams) {
        var deferred = $q.defer();
        //Todo $stateParams.feed is an int id. Not sure where it was meant to be coming from so for now going to stub this value.
        $stateParams.feed = 1;
        $resource('resource/secure/feed_configurations/' + $stateParams.feed).query().$promise
            .then(function(data) {
              deferred.resolve(data);
            });
        return deferred.promise;
      }
    },
    data: {
      requireLogin: true
    },
    views: {
      'layout@root': {
        templateUrl: require.toUrl('configuration/settings/settings.configuration.html'),
        controller: 'configurationSettingsController'
      }
    }

  }

});
