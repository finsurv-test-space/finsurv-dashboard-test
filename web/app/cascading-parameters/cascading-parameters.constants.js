define(['./cascading-parameters.module'], function (module) {

  // Define any constants:

  // Save the constructor for cascading parameters as a constant.
  // People using this can new up a cascading parameters object using this.
  module.constant('CascadingParameters', CascadingParameters);


  /**
   * This is the constructor for new cascading parameters.
   * To access parameters that have cascaded, use get() and set().
   * If you want to access local variables only then use cascadingParameters.locals[param] or getLocal();
   * Every time you set a parameter, it will apply the changes recursively down the tree.
   * To set many parameters at once, use setMany({ p1:v1, p2:v2 ...}) instead. It will recurse once.
   * @param parent The parent cascading parameters object. If this is null then the new object is considered the root.
   * @constructor
   */
  function CascadingParameters(parent)
  {
    // Save a closure to this instance:
    var me = this;

    // Create a collection for children:
    this.children = [];

    // Initialise the local and combined properties:
    this.locals = {};
    this.all = {};

    // Define the API for getting or setting parameters:
    this.get = get;
    this.getLocal = getLocal;
    this.set = set;
    this.setMany = setMany;
    this.has = has;
    this.hasLocal = hasLocal;

    // Define the code to recursively cascade parameters down:
    this.cascade = cascade;

    // Save the parent:
    this.setParent = setParent;
    this.setParent(parent);

    /**
     * This adds the current object to the parent.
     * It first disconnects from the existing parent if there is one.
     * @param parent The parent cascading parameter object to add to. To disconnect from the parent, set parent to null or undefined.
     */
    function setParent(parent)
    {
      // Check if we must disconnect from an existing parent:
      if (me.parent && me.parent.children)
      {
        // We have an existing parent.

        // Get our index in the parent:
        var index = me.parent.children.indexOf(me);
        if (index != -1)
        {
          // We found ourselves in the parent.
          // Remove ourselves from the parent:
          me.parent.children.splice(index, 1);
        }
      }
      // Now we are disconnected.

      // Save the new parent:
      me.parent = parent;

      // Add the new child to the parent:
      if (parent && parent.children)
      {
        // Add ourselves to the parent:
        parent.children.push(me);
      }
    }


    /**
     * This gets the parameter with the given name.
     * @param name The name of the parameter to get.
     * @returns {*} The parameter with the given name (including any inherited parameters). Undefined if it doesn't exist.
     */
    function get(name)
    {
      return me.all[name];
    }

    /**
     * This gets the local parameter with the given name.
     * @param name The name of the parameter to get.
     * @returns {*} The parameter with the given name (excluding any inherited parameters). Undefined if it doesn't exist.
     */
    function getLocal(name)
    {
      return me.locals[name];
    }

    /**
     * This gets the parameter with the given name.
     * @param name The name of the parameter to get.
     * @returns {*} The parameter with the given name (including any inherited parameters). Undefined if it doesn't exist.
     */
    function set(name, value)
    {
      // Save the value:
      me.locals[name] = value;

      // Cascade the parameters down:
      cascade();
    }

    /**
     * This gets the parameter with the given name.
     * @param values The fields on this object will all be set on the cascading parameters before cascading down once.
     */
    function setMany(values)
    {
      // Set multiple values:
      // http://stackoverflow.com/questions/8312459/iterate-through-object-properties
      for (var property in values)
      {
        if (values.hasOwnProperty(property))
        {
          // Set the local parameter:
          me.locals[property] = values[property];
        }
      }

      // Cascade the parameters down:
      cascade();
    }

    /**
     * This checks whether we have a parameter with the given name (including cascaded parameters).
     * @param name The name of the parameter to get.
     * @returns {*} True if the parameter exists (including any inherited parameters). False if it doesn't.
     */
    function has(name)
    {
      return me.all[name] !== undefined;
    }

    /**
     * This checks whether we have a local parameter with the given name.
     * @param name The name of the parameter to get.
     * @returns {*} True if the parameter exists (excluding any inherited parameters). False if it doesn't.
     */
    function hasLocal(name)
    {
      return me.locals[name] !== undefined;
    }

    /**
     * This cascades parameters down the tree recursively.
     * First local variables override any cascaded values (in 'all').
     * Then the parameters in 'all' are cascaded down to each child's 'all' collection.
     * The algorithm repeats recursively on each child.
     */
    function cascade()
    {
      // Override any cascaded parameters with local ones:
      // http://stackoverflow.com/questions/8312459/iterate-through-object-properties
      for (var property in me.locals)
      {
        if (me.locals.hasOwnProperty(property))
        {
          // Overwrite the cascaded parameter with the local one:
          me.all[property] = me.locals[property];
        }
      }
      // Now locals have overwritten cascaded parameters.

      // Copy all cascaded parameters onto the actual parameters object for easy access:
      for (var property in me.all)
      {
        if (me.all.hasOwnProperty(property))
        {
          // Check whether it's any of our API values so we don't overwrite them:
          switch(property)
          {
            case 'parent':
            case 'children':
            case 'locals':
            case 'all':
            case 'get':
            case 'getLocal':
            case 'set':
            case 'setMany':
            case 'has':
            case 'hasLocal':
            case 'cascade':
            case 'setParent':
              // This is one of our API methods.
              // Ignore it.
              break;
            default:
              // This is not one of our well know API methods.
              // Set the field:
              me[property] = me.all[property];
              break;
          }
        }
      }
      // Now cascaded parameters are fields on the current object for easy API access.

      // Update children:
      if (me.children && me.children.length > 0)
      {
        // We have children.
        // Copy all cascaded parameters down to the child:
        for (var property in me.all)
        {
          if (me.all.hasOwnProperty(property))
          {
            // Go through each child:
            me.children.forEach(function (child)
            {
              // Trickle the parameter down:
              child.all[property] = me.all[property];
            });
          }
        }
        // Now all cascaded parameters have trickled down to the children.

        // Recursively cascade each child:
        me.children.forEach(function (child)
        {
          // Cascade the parameters recursively:
          child.cascade();
        });
      }
    }
  }


  return CascadingParameters;
});