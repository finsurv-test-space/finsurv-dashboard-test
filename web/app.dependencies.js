'use strict';

/*
 This is one place where we define all the application dependencies.
 We put it into this one structure because the places that need the information will pull it out of this structure.
 Previously, we had at least 3 different places to update when adding dependencies, which was a pain.
 This file is processed before RequireJS has booted up. Therefore it is not in a define block.
 This file drives the RequireJS config as well as the Angular config.
 If you find a module that you want to use, do the following:
   1)     Add it to the bower dependencies and run 'bower update'
   2)     Add the RequireJS mapping to main.js to reference it from the 'bower_components' directory.
   3)     Add another entry below which describes the require-path, require-name, angular-name etc.
     3.1)   require: The name of the require module for the entry.
     3.2)   angular: The name of the angular module for the entry. Usually there is a difference in naming to the require module name. Any module names added here will automatically become module dependencies for the app.
     3.3)   type: Specifies what type of require dependency this is. It can be 'package', 'file', 'global' or 'dynamic'. 'file' means that it is loaded as a normal require js file. 'global' means that it is loaded in the global scope (probably in index.html). 'package' means that it is added as a require package. 'dynamic' means that we use the dynamic loader which returns a promise for when the implementations have been required in. This happens before we boot up angular.
     3.3.1) main: if the type is 'package' then an optional 'main' field can be specified for the main entry of the package.
     3.4)   shim: The value to write in the require shim configuration. This can be left off to not add any shim entry. Shims are only needed for non-require libraries.
     3.5)   path: The path to the actual file relative to the baseURL (js folder). NOTE: Normally, it's enough to have this as a string for the path, however an array of paths can be specified if you want path fallbacks (http://requirejs.org/docs/api.html#pathsfallbacks). You could specify a library from a CDN and fallback to a local version as a second path.
 */

// Define all the application dependencies:
//                                                                                          Only put a shim if the library doesn't use require.
var appDependencies = [
  { require: 'app.dependencies',                                              type: 'global',  shim: {exports: "appDependencies"}  },         // This file!
  { require: 'widget-list',                                                   type: 'global',  shim: {exports: "widgetList"}  },              // The list of widgets to load dynamically!
  { require: 'navigation-provider-list',                                      type: 'global',  shim: {exports: "navigationProviderList"}  },  // The list of navigation providers to load dynamically!
  { require: 'data-source-list',                                              type: 'global',  shim: {exports: "dataSourceList"}  },          // The list of data sources for dashboards to load dynamically!
  { require: 'config.list',                                                   type: 'global',  shim: {exports: "configList"} },               // The list of configurations for the app!
  { require: 'navigation-tree',                                               type: 'file',                                                   path: 'navigation-tree'},
  { require: 'nvd3Graphs',                                                    type: 'file',                                                  path: 'app/widgets/nvd3Graphs'},
  { require: 'isHoliday',                                                    type: 'file',                                                  path: 'app/widgets/isHoliday'},

  { require: 'sarbFiles',                  angular: 'sarbFiles',              type: 'package', main:'sarbFiles',                              path: 'app/sarbFiles'},
  {require: 'queueGraph',                  angular: 'queueGraph',             type: 'package', main: 'queueGraph',                            path: 'app/queueGraph'},
  { require: 'transactionList',            angular: 'transactionList',        type: 'package', main:'transactionList',                        path: 'app/transactionList'},
  { require: 'transactionContainerList',   angular: 'transactionContainerList',type: 'package', main:'transactionContainerList',              path: 'app/transactionContainerList'},
  { require: 'transactionData',            angular: 'transactionData',        type: 'package', main:'transactionData',                        path: 'app/transactionData'},
  { require: 'alerts',                     angular: 'alerts',                 type: 'package', main:'alerts',                                 path: 'app/alerts'},
  { require: 'queue',                      angular: 'queue',                  type: 'package', main:'queue',                                  path: 'app/queue'},
  { require: 'inQueue',                    angular: 'inQueue',                type: 'package', main:'inQueue',                                path: 'app/inQueue'},
  { require: 'transactionHistory',         angular: 'transactionHistory',     type: 'package', main:'transactionHistory',                     path: 'app/transactionHistory'},
  { require: 'modelService',               angular: 'modelService',           type: 'package', main:'modelService',                           path: 'app/modelService'},


  // { require: 'bopapp',                                                        type: 'package', main:'app.module', shim: {  exports: 'bopapp' },path:  'bopformserver/electronic-bop-form/src'},
  // { require: 'coreRules',                                                     type: 'package', main:'coreRules', shim: {  exports: 'coreRules' },path:  'bopformserver/electronic-bop-form/rules'},
// // { require: 'customRules',                                                    type: 'file',                                                   path:  'rules/customRules/*'},
  // { require: 'dummyData',                                                      type: 'file',                                                  path:  'bopformserver/electronic-bop-form/testData/dummyData'},


  { require: 'handlebars',                                                    type: 'file',   shim: { exports: 'handlebars'},                 path:  'bower_components/handlebars/handlebars'},
  { require: 'app',                        angular: 'app',                    type: 'package', main:'app',                                    path: 'app'},
  { require: 'cascading-parameters',       angular: 'cascadingParameters',    type: 'package', main:'cascading-parameters',                   path: 'app/cascading-parameters'},
  //{ require: 'configuration',              angular: 'configuration',          type: 'package', main:'configuration',                          path: 'app/configuration'},
  { require: 'dropdown-multiselect',       angular: 'dropdownMultiselect',    type: 'package', main:'dropdown-multiselect',                   path: 'app/dropdown-multiselect'},
  { require: 'dashboards',                 angular: 'dashboards',             type: 'package', main:'dashboards',                             path: 'app/dashboards'},
  //{ require: 'login',                      angular: 'login',                  type: 'package', main:'login',                                  path: 'app/login'},
  //{ require: 'user',                       angular: 'user' ,                  type: 'package', main:'user',                                   path: 'app/user'},
  { require: 'navigation',                 angular: 'navigation',             type: 'package', main:'navigation',                             path: 'app/navigation'},
  { require: 'widget-registry',            angular: 'widgetRegistry',         type: 'package', main:'widget-registry',                        path: 'app/widget-registry'},
  { require: 'data-source-registry',       angular: 'dataSourceRegistry',     type: 'package', main:'data-source-registry',                   path: 'app/data-source-registry'},
  //{ require: 'main-menu-brand-service',       angular: 'mainMenuService',     type: 'package', main:'root',                                   path: 'app/root/main-menu.brand.service'},
  //{ require: 'web-socket',                 angular: 'webSocket',              type: 'package', main:'web-socket',                             path: 'app/web-socket'},
  //{ require: 'notifications',              angular: 'notifications',          type: 'package', main:'notifications',                          path: 'app/notifications'},
  { require: 'notify-popup',               angular: 'notifyPopup',            type: 'package', main:'notify-popup',                           path: 'app/notify-popup'},
  //{ require: 'settings',                   angular: 'settings',               type: 'package', main:'settings',                               path: 'app/settings'},
  { require: 'root',                       angular: 'root',                   type: 'package', main:'root',                                   path: 'app/root'},
  { require: 'pnotify',                                                       type: 'file',                                                   path: 'app/pnotify/pnotify.custom.min'}, // We minified all the files together for convenience.
  { require: 'pnotify-angular',            angular: 'root',                   type: 'package', main:'pnotify-angular',                        path: 'app/pnotify-angular'},
  { require: 'require',                                                       type: 'file',                                                   path: 'bower_components/requirejs/require'},
  { require: 'when',                                                          type: 'package', main:'when',                                   path: 'bower_components/when'}, // This is a Promise Library called 'When'. https://github.com/cujojs/when/blob/master/docs/installation.md
  { require: 'jquery',                                                        type: 'file',    shim: { exports: '$'},                         path: 'bower_components/jquery/dist/jquery.min'}, // See: http://requirejs.org/docs/jquery.html
  { require: 'jquery-ui',                                                     type: 'file',                                                   path: 'bower_components/jquery-ui/jquery-ui'},
  { require: 'less',                                                          type: 'file',    shim: { exports: 'less'},                      path: 'bower_components/less/dist/less-1.7.5' },
  { require: 'underscore',                                                    type: 'file',    shim: { exports: "_"},                         path: 'bower_components/underscore/underscore'},
  { require: "underscore.string",                                             type: 'file',                                                   path: 'bower_components/underscore.string/dist/underscore.string'},
  { require: 'js-soap-client',                                                type: 'file',    main:'js-soap-client',                         path: 'bower_components/angular-soap-finsurv/soapclient'}, // See: http://requirejs.org/docs/jquery.html
  { require: 'bootstrap',                                                     type: 'file',    shim: ['jquery'],                              path: 'bower_components/bootstrap/dist/js/bootstrap.min'},
  { require: 'angular',                                                       type: 'file',    shim: {exports: "angular", deps: ['jquery']},  path: 'bower_components/angular/angular'},
  { require: 'angular-sanitize',            angular: 'ngSanitize',            type: 'file',    shim: ['angular'],                             path: 'bower_components/angular-sanitize/angular-sanitize'},
  { require: 'angular-resource',            angular: 'ngResource',            type: 'file',    shim: ['angular'],                             path: 'bower_components/angular-resource/angular-resource'},
  { require: 'angular-cookies',             angular: 'ngCookies',             type: 'file',    shim: ['angular'],                             path: 'bower_components/angular-cookies/angular-cookies.min'},
  { require: 'angular-ui-router',           angular: 'ui.router',             type: 'file',    shim: ['angular'],                             path: 'bower_components/angular-ui-router/release/angular-ui-router'},
  { require: 'angular-bootstrap',           angular: 'ui.bootstrap',          type: 'file',    shim: ['angular'],                             path: 'bower_components/angular-bootstrap/ui-bootstrap'},
  { require: 'angular-bootstrap-templates', angular: 'ui.bootstrap.tpls',     type: 'file',    shim: ['angular'],                             path: 'bower_components/angular-bootstrap/ui-bootstrap-tpls'},
  { require: 'angular-soap',                angular: 'angularSoap',           type: 'file',    shim: ['angular'],                             path: 'bower_components/angular-soap-finsurv/angular.soap'},
  { require: 'angular-animate',                                               type: 'file',    shim: ['angular'],                             path: 'bower_components/angular-animate/angular-animate'},
  { require: 'requirejs-text',                                                type: 'file',                                                   path: 'bower_components/requirejs-text/text'},
  { require: 'ie8',                                                           type: 'file',                                                   path: 'bower_components/ie8/build/ie8'},
 // { require: 'firebug-lite',                                                  type: 'file',                                                   path: 'bower_components/firebug-lite/build/firebug-lite'},
  { require: 'd3',                                                            type: 'file',    shim: {exports: "d3"},                         path: 'bower_components/d3/d3.min'},
  { require: 'd3tip',                                                         type: 'file',    shim: {deps:['d3'],exports: "d3-tip"},         path: 'bower_components/d3-tip/index'},
  { require: 'nvd3',                                                         type: 'file',    shim: {deps:['d3'],exports: "nvd3"},         path: 'bower_components/nvd3/build/nv.d3'},
  { require: 'angular-nvd3',                              type: 'file',    shim: {deps:["nvd3", "angular"],exports: "angular-nvd3"},         path: 'bower_components/angular-nvd3/dist/angular-nvd3'},


   // { require: 'ng-sortable',                 angular: 'ui.sortable',           type: 'file',    shim: ['angular'],                             path: 'bower_components/ng-sortable/dist/ng-sortable'},
//{ require: 'angular-ui-sortable',         angular: 'ui.sortable',           type: 'file',    shim: ['angular'],                             path: 'bower_components/angular-ui-sortable/sortable'}, // Commented out because it is the JQuery wrapper. See ngSortable instead.
{ require: 'angular-ui-grid',             angular: 'ui.grid',               type: 'file',    shim: { deps:['angular'], exports: 'ngGrid' }, path: 'bower_components/angular-ui-grid/ui-grid'},
  // { require: 'angular-aside',               angular: 'ngAside',               type: 'file',    shim: { deps:['angular'], exports: 'ngAside' },path: 'bower_components/angular-aside/dist/js/angular-aside.min'},
 //  { require: 'angular-gridster',            angular: 'gridster',              type: 'file',                                                   path: 'bower_components/angular-gridster/dist/angular-gridster.min'},
 //  { require : 'jquery.resize', type: 'file', shim: ['jquery'], path:'bower_components/javascript-detect-element-resize/jquery.resize'}


];
