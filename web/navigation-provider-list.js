// This defines all the navigation providers that exist for expanding the navigation tree.
// This list is used to load all the factories that handle the creation of navigation providers in the navigation module.
// This file is processed before RequireJS has booted up. Therefore it is not in a define block.
// This file drives the RequireJS config as well as the Angular config.
var navigationProviderList = [
      
    ];