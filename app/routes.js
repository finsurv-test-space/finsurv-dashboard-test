var http = require('http');
var querystring = require('querystring');
var url = require('url');

var proxy = require('express-http-proxy');

var sampleWorkFlow = {
	"activeWorkflows": [
		{
			"name": "transactionStagingListener",
			"status": "Pause",
			"comment": "Yielded At: 1489657342980 (1823120024); Resume After: 10000 (-1823110024)"
		},
		{
			"name": "accountUploadListener",
			"status": "Pause",
			"comment": "Yielded At: 1489657343083 (1823119921); Resume After: 10000 (-1823109921)"
		},
		{
			"name": "accountChecksumUploadListener",
			"status": "Pause",
			"comment": "Yielded At: 1489657343261 (1823119743); Resume After: 10000 (-1823109743)"
		},
		{
			"name": "submitToDEX",
			"status": "Pause",
			"comment": "Yielded At: 1489657343363 (1823119641); Resume After: 60000 (-1823059641)"
		},
		{
			"name": "pollStatusOnDEX",
			"status": "Pause",
			"comment": "Yielded At: 1489657348389 (1823114615); Resume After: 300000 (-1822814615)"
		},
		{
			"name": "pickupCompleteTransactions",
			"status": "Pause",
			"comment": "Yielded At: 1489657348425 (1823114579); Resume After: 30000 (-1823084579)"
		},
		{
			"name": "pickupReadyTransactions",
			"status": "Pause",
			"comment": "Yielded At: 1489657348444 (1823114560); Resume After: 30000 (-1823084560)"
		},
		{
			"name": "generateArtifacts",
			"status": "Pause",
			"comment": "Yielded At: 1489657348457 (1823114547); Resume After: 60000 (-1823054547)"
		},
		{
			"name": "composeAccurateArtifacts",
			"status": "Pause",
			"comment": "Yielded At: 1489657348471 (1823114533); Resume After: 60000 (-1823054533)"
		},
		{
			"name": "composeGoAMLFile",
			"status": "Running",
			"comment": "composeGoAMLFile: ListenerWorkflow -> ListenerWorkflow -> Listen -> TransactionScope -> OR.ComposeGoAMLFile"
		},
		{
			"name": "matchTransactions",
			"status": "Running",
			"comment": "matchTransactionsno root activity"
		},
		{
			"name": "alertProcessing",
			"status": "Running",
			"comment": "alertProcessingno root activity"
		},
		{
			"name": "sarbdexUploadListener",
			"status": "Running",
			"comment": "sarbdexUploadListenerno root activity"
		},
		{
			"name": "transactionUpdateNotification",
			"status": "Running",
			"comment": "transactionUpdateNotificationno root activity"
		},
		{
			"name": "processCancellationInstructions",
			"status": "Running",
			"comment": "processCancellationInstructionsno root activity"
		},
		{
			"name": "accountEntryClassification",
			"status": "Running",
			"comment": "accountEntryClassificationno root activity"
		},
		{
			"name": "accountEntrySharding",
			"status": "Running",
			"comment": "accountEntryShardingno root activity"
		},
		{
			"name": "transactionSharding",
			"status": "Running",
			"comment": "transactionShardingno root activity"
		},
		{
			"name": "shardMatching",
			"status": "Running",
			"comment": "shardMatchingno root activity"
		},
		{
			"name": "housekeeping",
			"status": "Running",
			"comment": "housekeepingno root activity"
		},
		{
			"comment": "FOO2",
			"name": "BAR2",
			"status": "fooBar2"
		},
		{
			"comment": "FOO2",
			"name": "BAR2",
			"status": "fooBar2"
		},
		{
			"comment": "FOO2",
			"name": "BAR2",
			"status": "fooBar2"
		},
		{
			"comment": "FOO2",
			"name": "BAR2",
			"status": "fooBar2"
		}
	],
	"stoppedWorkflows": [
		{
			"comment": "FOO2",
			"name": "BAR2",
			"status": "fooBar2"
		},
		{
			"comment": "FOO2",
			"name": "BAR2",
			"status": "fooBar2"
		},
		{
			"comment": "FOO2",
			"name": "BAR2",
			"status": "fooBar2"
		},
		{
			"comment": "FOO2",
			"name": "BAR2",
			"status": "fooBar2"
		},
		{
			"comment": "FOO2",
			"name": "BAR2",
			"status": "fooBar2"
		}
	]
}


module.exports = function (app) {

	// app.use('/rest', proxy('localhost:28080',{
	// 	forwardPath: function(req,res){
	// 		return '/tXstreamServer/spring-rest'+(url.parse(req.url).path);
	// 	}
	// }));
	app.post('/finsurv-dashboard/doPost/dashboard/pivotmodel',function(req,res){
		res.send('{"test":true}');
	});


	app.get('/finsurv-dashboard/doGet/workflows/status',function(req,res){
		res.send(JSON.stringify(sampleWorkFlow));
	});

	app.get('/finsurv-dashboard/doGet/settings/startup',function(req,res){
		res.send('{"Setting":[{"Value":"test","Name":"test"}]}');
	})

	app.use('/rest', proxy('http://ec2-52-66-110-18.ap-south-1.compute.amazonaws.com',{
		forwardPath: function(req,res){
			return '/tXstreamServer/spring-rest'+(url.parse(req.url).path);
		}
	}));

	app.get('*', function (req, res) {
		res.sendFile('./web/index.html', { root: __dirname + '/../'});
	});
};
