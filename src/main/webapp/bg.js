/**
 * Created by petruspretorius on 17/02/2015.
 */
define(function () {

  var hexw = .5;//Math.sin(1 / 6 * Math.PI);
  var hexh = Math.sin(1 / 3 * Math.PI);


  function createRandomGenerator(seed) {
    return function () {
      seed = Math.sin(seed) * 10000;
      return seed - Math.floor(seed);
    }
  }

  var rand;

  function drawHex(ctx, l, pos, scale) {

    ctx.translate(pos.x, pos.y);
    ctx.scale(scale, scale);
    ctx.translate(-l / 2, -(hexh * l));

    ctx.moveTo(0, 0);
    ctx.lineTo(l, 0);
    ctx.lineTo(hexw * l + l, hexh * l);
    ctx.lineTo(l, hexh * l * 2.0);
    ctx.lineTo(0, hexh * l * 2.0);
    ctx.lineTo(-hexw * l, hexh * l);
    ctx.lineTo(0, 0);

    ctx.resetTransform();
  }

  function makeColor(r, g, b, a) {

    return "rgba(" + r + "," + g + "," + b + "," + a + ")"

  }


  function drawHexGrid(ctx, _options) {

    var defaultOptions = {
      color        : {
        r: 255,
        g: 255,
        b: 255,
        a: 1.0
      },
      fill         : undefined,
      stroke       : undefined,
      variance     : undefined,
      colVariance  : {r: 0, g: 0, b: 0, a: 0},
      offset       : undefined,
      lineWidth    : 1.0,
      scale        : 1.0,
      rows         : 10,
      cols         : 10,
      l            : 40
    }

    var options = _.extend(defaultOptions, _options);

    ctx.lineWidth = options.lineWidth;

    //var l = 100;
    for (var i = 0; i < options.cols; i++) {
      for (var j = 0; j < options.rows; j++) {
        if ((rand() + .2) < (j / options.rows)) {


          ctx.beginPath();

          var w = (j % 2 == 0) ? 0 : (1 + hexw) * options.l;
          if (options.offset)
            ctx.translate(options.offset.x, options.offset.y);
          var variance = (options.variance ? (rand() * (options.variance) + (1.0 - options.variance)) : 1.0);
          drawHex(ctx, options.l, {
            x: i * options.l * 3 + w,
            y: j * options.l * hexh
          }, variance * options.scale);


          ctx.closePath();

          var color = makeColor(
            Math.floor(options.color.r + ((rand() - 0.5) * options.colVariance.r) * 255),
            Math.floor(options.color.g + ((rand() - 0.5) * options.colVariance.g) * 255),
            Math.floor(options.color.b + ((rand() - 0.5) * options.colVariance.b) * 255),
            (options.color.a + ((rand() - 0.5) * options.colVariance.a))
          );

          if (options.fill) {
            ctx.fillStyle = color;
            ctx.fill();
          }
          if (options.stroke) {
            ctx.strokeStyle = color;
            ctx.stroke();
          }

        }
      }
    }


  }


  return {
    createBackground: function (_options) {


      var defaultOptions = {
        seed  : 100
      }

      var options = _.extend(defaultOptions, _options);


      rand = createRandomGenerator(options.seed);

      var canvas = document.createElement('canvas');

      document.getElementsByTagName('body')[0].appendChild(canvas);
      //var canvas = document.getElementById(options.bgElId)

      canvas.width = window.innerWidth;
      canvas.height = window.innerHeight;


      var ctx = canvas.getContext("2d");

      var my_gradient = ctx.createLinearGradient(canvas.width, 0, 0, canvas.height);
      my_gradient.addColorStop(0, "rgb(255,255,255)");
      my_gradient.addColorStop(0.6, "rgb(255,255,255)");
      my_gradient.addColorStop(1, "rgb(49,176,227)");

      ctx.fillStyle = my_gradient;//"rgba(255,255,255,0)";
      ctx.fillRect(0, 0, canvas.width, canvas.height);

      if(_.isUndefined(ctx.resetTransform)){

      }else{
        drawHexGrid(ctx, {
          rows     : 28,
          cols     : 15,
          l        : 30,
          scale    : 1,
          lineWidth: 4.0,
          stroke   : true,
          color    : {r: 19, g: 140, b: 200, a: 0.9}
        });
        drawHexGrid(ctx, {
          rows       : 28,
          cols       : 15,
          l          : 30,
          scale      : .9,
          variance   : .4,
          fill       : true,
          colVariance: {r: 0.1, g: 0.2, b: 0.2, a: 0.5},
          color      : {r: 49, g: 176, b: 227, a: 0.9}
        });
        drawHexGrid(ctx, {
          rows    : 28,
          cols    : 15,
          l       : 30,
          scale   : .9,
          variance: .7,
          offset  : {x: 15, y: 15},
          fill    : true,
          colVariance: {r: 0.0, g: 0.1, b: 0.2, a: 0.5},
          color   : {r: 49, g: 176, b: 227, a: 0.5}
        });
      }



      var my_gradient = ctx.createLinearGradient(0, 0, 0, canvas.height);

      my_gradient.addColorStop(0, "rgba(255,255,255,0.9)");
      my_gradient.addColorStop(0.8, "rgba(255,255,255,0.5)");
      my_gradient.addColorStop(1, "rgba(49,176,227,0.5)");
      ctx.fillStyle = my_gradient;
      ctx.fillRect(0, 0, canvas.width, canvas.height);


      return canvas.toDataURL('image/jpeg');

    }
  }
})
