<%@ page import="java.util.Properties,
                 java.util.Enumeration,
                 java.util.Map,
                 javax.naming.InitialContext" %>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>TXstream Login</title>
	<link rel="stylesheet" type="text/css" href="Client.css" />
	<link rel="stylesheet" type="text/css" href="tXstream.css" />
	<script type="text/javascript" src="jquery.min.js" ></script>
	<script type="text/javascript">
			$(document).ready(function(){
					$(".slidingDiv").hide();
					$(".show_hide").show();
				$('.show_hide').click(function(){
				$(".slidingDiv").slideToggle();
				});
			});
	</script>

</head>

  <body>

        	<div class="banklogo">
        	</div>
        	<div class="container">
        		<div class="midCol">
        			<img src="images/synthesis.png" width="303" height="65"  alt="Synthesis Logo"/>

        		</div>
        		<br class="clearleft"/>
        		<div class="formcontainer">

        			<form method="POST" action='<%=response.encodeURL( "j_security_check" )%>'>
        				<h1 class="welcome">Welcome to TXstream 3.1 Dashboards</h1>
        				<div class="errormsg">
        					<%
        				    if (request.getParameter("retry") != null) {
        				        out.println("<b>User name or password incorrect. Please try again.</b>");
        				    }
        					if (request.getParameter("timeout") != null) {
        			        	out.println("<b>Your session has timed out. Please login again.</b></td>");
        			    	}
        					%>
        				</div>
        				<table align="center">
        					<tr>
        						<td><label>User Name</label></td>
        						<td><input type="text" name="j_username"></td>
        					</tr>
        					<tr>
        						<td><label>Password</label></td>
        						<td><input type="password" name="j_password"></td>
        					</tr>
        					<tr align="right">
        						<td colspan="2"><input type="submit" value="Login"></td>
        					</tr>
        				</table>
        			</form>
        		</div>
        	</div>

		<script type="text/javascript" src="require.js" ></script>


		<!-- <script>
			require.config({
				paths: {
				bg: 'bg',
				lodash: 'lodash.min'
				},
				shim: {
				'bg': ['lodash']
				}
			})
		</script>

		 <script>
			require(['bg'], function (bg) {
                bg.createBackground({bgElId: 'baseCanvas', seed: 4 ,colors :[{r: 19, g: 200, b: 140, a: 0.9}, {r: 49, g: 227, b: 176, a: 0.9} ]});
			})
		</script> -->



   </body>
</html> 