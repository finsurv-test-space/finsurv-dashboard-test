package za.co.synthesis.dashboard;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by nishaal on 11/25/16.
 */

@RestController
public class Rest {

    public final String USER_AGENT = "Mozilla/5.0";
    public String defaultProxyURL = "http://localhost:8080/tXstreamServer/spring-rest/";
    public String proxyUrl;

    public Rest(){
        this.proxyUrl =  ServerConfig.getProxyUrl("DashboardService",defaultProxyURL);
        System.out.println("Constructor proxyUrl is : " + proxyUrl  );
    }

    @RequestMapping(method = RequestMethod.GET, value = "/doGet/{controller}/{method}")
    private String sendGet(@PathVariable String controller, @PathVariable String method, HttpServletRequest req, HttpServletResponse resp) throws Exception {

        //  Create Get request dynamically to remote server
        String queryString = req.getQueryString();
        String pathInfo = req.getPathInfo();
        String url = proxyUrl + (pathInfo != null ? pathInfo : "" )+ (queryString != null ? "?" + queryString : "");
        System.out.println(url);

        URL obj = new URL(url + controller + "/" + method);

        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        System.out.println("This is the URL");
        System.out.println(obj.getPath().toString());

        // optional default is GET
        con.setRequestMethod("GET");

        //add request header
        con.setRequestProperty("User-Agent", USER_AGENT);

        int responseCode = con.getResponseCode();
        System.out.println("\nSending 'GET' request to URL : " + obj.getPath().toString());
        System.out.println("Response Code : " + responseCode);

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();


        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        return response.toString();

    }

    @RequestMapping(method = RequestMethod.POST, value = "/doPost/{controller}/{method}")
    private String sendGet(@PathVariable String controller, @PathVariable String method, @RequestBody String query, HttpServletRequest req, HttpServletResponse res) throws Exception {

        String queryString = req.getQueryString();
        String pathInfo = req.getPathInfo();
        String url = proxyUrl + (pathInfo != null ? pathInfo : "" )+ (queryString != null ? "?" + queryString : "");
        System.out.println(url);

        URL obj = new URL(url + controller + "/" + method);

        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        System.out.println("This is the URL");
        System.out.println(obj.getPath().toString());

        //add reuqest header
        con.setRequestMethod("POST");
        con.setRequestProperty("User-Agent", USER_AGENT);
        con.setRequestProperty("Content-Type", "application/json; charset=utf-8");
//        con.setRequestProperty("api_key", req.getHeader("api_key"));
        String api_key = req.getHeader("api_key");
        if (api_key != null && api_key.length()>0) {
            con.setRequestProperty("api_key", api_key);
        }
        con.setRequestProperty("REMOTE_USER", req.getHeader("REMOTE_USER"));


        String urlParameters = query;

        // Send post request
        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(urlParameters);
        wr.flush();
        wr.close();

        int responseCode = con.getResponseCode();
        System.out.println("\nSending 'POST' request to URL : " + obj.getPath().toString());
        System.out.println("Post parameters : " + urlParameters);
        System.out.println("Response Code : " + responseCode);

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        return response.toString();

    }

}
