package za.co.synthesis.dashboard;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 * Created by nishaal on 2/1/17.
 */
public class ServerConfig {

    public static String getProxyUrl(String service, String defValue) {
        return getEnvPropertyValue("proxyUrl", service, defValue);
    }

    private static String getEnvPropertyValue(String key, String service, String defValue) {
        try {
            Context initCtx = new InitialContext();
            Context envCtx = (Context) initCtx.lookup("java:comp/env/");

            return (String) envCtx.lookup(service + "." + key);
        }
        catch (NamingException ne) {
            return defValue;
        }
    }

}
