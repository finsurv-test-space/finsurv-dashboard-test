package za.co.synthesis.dashboard;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.*;

import java.util.Map;

/**
 * Created by nishaal on 8/24/16.
 */

//Idea is to redirect to the index page on every refresh

//@Controller
//@RestController
@Configuration
//@EnableWebMvc
@ComponentScan

public class index extends WebMvcConfigurerAdapter {


    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/dashboard/**").setViewName("forward:/index.html");
    }


};


